\section{Relativistic Electrodynamics}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Magnetism as a Relativistic Phenomenon}
Classical electrodynamics is already consistent with special relativity:
Maxwell's equations and Lorentz force law can be applied in any inertial system.
What an observer interprets as an electrical process, another may regard as magnetic, but the actual particle motion they predict will be \textit{identical}.
\\

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-electric_magnetic-01}
    \caption{Electric vs. Magnetic.}%
    \label{fig:ch12:ElectricVSMagnetic}
\end{figure}
Suppose a string of positive and negative charges moving at the same speed, $v$, but in opposite direction (Fig.~\ref{fig:ch12:ElectricVSMagnetic}a).
We have then a net current to the right, of magnitude
\begin{equation}
    I = 2 \lambda v
\end{equation}
Meanwhile, a distance $s$ away there is a point charge $q$ traveling to the right at speed $u < v$.
Because two line charges cancel, there is no electric force on $q$ in the system $\mathcal S$.
\\

Let's examine the same situation from the point of view $\bar{\mathcal S}$, moving to the right with speed $u$ (Fig.~\ref{fig:ch12:ElectricVSMagnetic}b).
In this system, $q$ is at rest.
By Einstein velocity addition rule, the velocities of the line charges are
\begin{equation}
    v_\pm
    =
    \frac{v \mp u}{1 \mp vu/c^2}
\end{equation}
Because $v_- > v_+$, the Lorentz contraction of the spacing between the negative charges is more severe.
Therefore, in this frame, the density of the negative charges become larger
\begin{equation}
    \lambda_\pm
    =
    \pm (\gamma_\pm)
    \lambda_0
\end{equation}
resulting in the wire carrying a net negative charge.
Here,
\begin{equation}
    \gamma_\pm
    =
    \frac{1}{\sqrt{1 - v_\pm^2/c^2}}
\end{equation}
and $\lambda_0$ is the charge density in its own rest frame
\begin{equation}
    \lambda_0
    =
    \frac{1}{\gamma}
    \lambda
\end{equation}
where
\begin{equation}
    \gamma = \frac{1}{\sqrt{1 - v^2/c^2}}
\end{equation}
Working out $\gamma_\pm$,
\begin{equation}
    \begin{aligned}
        \gamma_\pm
        &=
        \frac{1}{\sqrt{1 - \frac{1}{c^2} (v \mp u)^2 (1 \mp vu/c^2)^{-2}}}
        \\
        &=
        \frac{c^2 \mp uv}{\sqrt{(c^2 \mp uv)^2 - c^2 (v \mp u)^2}}
        \\
        &=
        \frac{c^2 \mp uv}{\sqrt{(c^2 - v^2) (c^2 - u^2)}}
        \\
        &=
        \gamma \frac{1 \mp uv/c^2}{\sqrt{1 - u^2/c^2}}
    \end{aligned}
\end{equation}
the net line charge in $\bar{\mathcal S}$ then is
\begin{equation}
    \lambda_{\rm tot}
    =
    \lambda_+ + \lambda_-
    =
    \lambda_0 (\gamma_+ - \gamma_-)
    =
    \frac{-2 \lambda uv}{c^2 \sqrt{1 - u^2/c^2}}
\end{equation}
Now, a line charge set up an electric field
\begin{equation}
    E = \frac{\lambda_{\rm tot}}{2\pi \epsilon_0 s}
\end{equation}
so there is an electric force on $q$ in $\bar{\mathcal S}$
\begin{equation}
    \bar F
    =
    qE
    =
    -\frac{\lambda v}{\pi\epsilon_0 c^2 s}
    \frac{qu}{\sqrt{1 - u^2/c^2}}
\end{equation}
If there is a force on $q$ in $\bar{\mathcal S}$, there must be one in $\mathcal S$.
Since $q$ is at rest in $\bar{\mathcal S}$, and $\bar F$ is perpendicular to $u$,
the force in $\mathcal S$ is given by Eq.~\ref{eq:ch12:ForceTransformWithParticleAtRest}
\begin{equation}
    F
    =
    \sqrt{1 - u^2/c^2}
    \bar F
    =
    -\frac{\lambda v}{\pi\epsilon_0 c^2}
    \frac{qu}{s}
    =
    -qu
    \left( \frac{\mu_0 I}{2\pi s} \right)
\end{equation}
The term in the parenthesis is the magnetic field of a long straight wire.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{How the Fields Transform}
\textbf{Q.}
General transformation rules for electromagnetic fields:
Given the fields in $\mathcal S$, what are the fields in $\bar{\mathcal S}$?

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-parallel_capacitor-01}
    \caption{Parallel capacitor moving parallel to the plates.}%
    \label{fig:ch12:ParallelCapacitor:1}
\end{figure}
Consider the simplest possible electric field (Fig.~\ref{fig:ch12:ParallelCapacitor:1}).
The capacitor is at rest in $\mathcal S_0$ and carries surface charges $\pm\sigma_0$.
Then, the electric field is
\begin{equation}
    \mathbf E_0
    =
    \frac{\sigma_0}{\epsilon_0}
    \uvec y
\end{equation}
What if we examine this same capacitor from system $\mathcal S$, moving to the right at speed $v_0$?
\\

The field still takes the form
\begin{equation}
    \mathbf E
    =
    \frac{\sigma}{\epsilon_0}
    \uvec y
\end{equation}
The total charge on each plate is invariant, the width $(w)$ is unchanged, but the length $(l)$ is Lorentz-contracted by a factor
\begin{equation}
    \gamma_0
    =
    \frac{1}{\sqrt{1 - v_0^2/c^2}}
\end{equation}
So, the charge per unit area is increased by the same factor
\begin{equation}
    \sigma = \gamma_0 \sigma_0
\end{equation}
Accordingly,
\begin{equation}
    \mathbf E^\perp
    =
    \gamma_0
    \mathbf E_0^\perp
    \label{eq:ch12:CapacitorFieldTransformationRule:Perp}
\end{equation}
The subscript $\perp$ is to make it clear that this rule pertains to components of $\mathbf E$ that are perpendicular to the direction of motion of $\mathcal S$.
\\

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-parallel_capacitor-02}
    \caption{Parallel capacitor moving perpendicular to the plates.}%
    \label{fig:ch12:ParallelCapacitor:2}
\end{figure}
To get the rule for the parallel component,
line up the capacitor plates with the $yz$ plane (Fig.~\ref{fig:ch12:ParallelCapacitor:2}).
This time, the plate separation $(d)$ is Lorentz-contracted.
Since the field does not depend on $d$, it follows
\begin{equation}
    E^\| = E_0^\|
    \label{eq:ch12:CapacitorFieldTransformationRule:Para}
\end{equation}

\framed{%
    \example{12.14}%
    \textbf{Electric field of a point charge in uniform motion}.
    A point charge $q$ is at rest at the origin in system $\mathcal S_0$.
    What is the electric field of this same charge in system $\mathcal S$,
    which moves to the right at speed $v_0$ relative to $\mathcal S_0$ (Fig.~\ref{fig:ch12:Example12_14}a)?
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-example_12_14-01}
    \caption{Example 12.14.}%
    \label{fig:ch12:Example12_14}
\end{figure}
In $\mathcal S_0$, the field is
\begin{equation}
    \mathbf E_0
    =
    \frac{1}{4\pi\epsilon_0}
    \frac{q}{r_0^2} \uvec r_0
\end{equation}
Its Cartesian components are
\begin{equation}
    \begin{aligned}
        E_{x0}
        &=
        \frac{1}{4\pi\epsilon_0}
        \frac{q x_0}{r_0^3}
        \\
        E_{y0}
        &=
        \frac{1}{4\pi\epsilon_0}
        \frac{q y_0}{r_0^3}
        \\
        E_{z0}
        &=
        \frac{1}{4\pi\epsilon_0}
        \frac{q z_0}{r_0^3}
    \end{aligned}
\end{equation}
From the transformation rules (Eqs.~\ref{eq:ch12:CapacitorFieldTransformationRule:Perp} and~\ref{eq:ch12:CapacitorFieldTransformationRule:Para}), we have
\begin{equation}
    \begin{aligned}
        E_{x}
        =
        \phantom{\gamma_0}
        E_{x0}
        &=
        \frac{1}{4\pi\epsilon_0}
        \frac{q x_0}{r_0^3}
        \\
        E_{y}
        =
        \gamma_0 E_{y0}
        &=
        \frac{\gamma_0}{4\pi\epsilon_0}
        \frac{q y_0}{r_0^3}
        \\
        E_{z}
        =
        \gamma_0 E_{z0}
        &=
        \frac{\gamma_0}{4\pi\epsilon_0}
        \frac{q z_0}{r_0^3}
    \end{aligned}
\end{equation}
where
\begin{equation}
    \gamma_0
    =
    \frac{1}{\sqrt{1 - v_0^2/c^2}}
\end{equation}

We need to express coordinates in $\mathcal S$
\begin{equation}
    \begin{array}{rcccl}
        x_0 &=& \gamma_0 (x + v_0 t) &=& \gamma_0 R_x \\
        y_0 &=&           y          &=&          R_y \\
        z_0 &=&           z          &=&          R_z \\
    \end{array}
\end{equation}
where $\mathbf R$ is the vector from $q$ to a point $P$ in system $\mathcal S$ (Fig.~\ref{fig:ch12:Example12_14}b).
Since $R_x^2 = R^2 \cos^2\theta$ and $R_y^2 + R_z^2 = R^2 \sin^2\theta$,
\begin{equation}
    \begin{aligned}
        \mathbf E
        &=
        \frac{1}{4\pi\epsilon_0}
        \frac{\gamma_0 q \mathbf R}{(\gamma_0^2 R^2 \cos^2\theta + R^2 \sin^2\theta)^{3/2}}
        \\
        &=
        \frac{1}{4\pi\epsilon_0}
        \frac{q (1 - v_0^2/c^2)}{(1 - (v_0^2/c^2) \sin^2\theta)^{3/2}}
        \frac{\uvec R}{R^2}
    \end{aligned}
    \label{eq:ch12:ElectricFieldOfPointChargeConstantSpeed}
\end{equation}
This is then the field of a charge in uniform motion,
the same result in Chapter 10 using the retarded potentials (Eq.~\ref{eq:ch10:ElectricFieldOfPointChargeConstantSpeed}).
\\

This derivation sheds some light on the fact that the field points away from the instantaneous (as opposed to the retarded) position of the charge:
$E_x$ gets a factor of $\gamma_0$ from the Lorentz transformation of coordinates $(x_0 \rightarrow x)$;
$E_y$ and $E_z$ pick theirs from the transformation of the field.
It's the balance of these two $\gamma_0$'s that leaves $\mathbf E$ parallel to $\mathbf R$.

\separator%

To derive the general rule, we must start out in a system with both electric and magnetic fields.

Consider a parallel capacitor in the $xz$ plane (Fig.~\ref{fig:ch12:ParallelCapacitor:1}).
In system $\mathcal S$, in addition to the electric field
\begin{equation}
    E_y = \frac{\sigma}{\epsilon_0}
\end{equation}
there is a magnetic field due to the surface current
\begin{equation}
    \mathbf K_\pm
    =
    \mp \sigma v_0 \uvec x
\end{equation}
By Amp\`ere's law,
\begin{equation}
    B_z = -\mu_0 \sigma v_0
\end{equation}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-parallel_capacitor-03}
    \caption{Three inertial systems.}%
    \label{fig:ch12:ParallelCapacitor:3}
\end{figure}
In a third system $\bar{\mathcal S}$, traveling to the right with speed $v$ relative to $\mathcal S$ ($\bar v$ relative to $\mathcal S_0$; Fig.~\ref{fig:ch12:ParallelCapacitor:3}),
the field would be
\begin{equation}
    \bar E_y
    =
    \frac{\bar\sigma}{\epsilon_0}
    ,\quad
    \bar B_z
    =
    -\mu_0 \bar\sigma \bar v
\end{equation}
where
\begin{equation}
    \bar v
    =
    \frac{v + v_0}{1 + vv_0/c^2}
    ,\quad
    \bar \gamma
    =
    \frac{1}{\sqrt{1 - \bar v^2/c^2}}
\end{equation}
and
\begin{equation}
    \bar\sigma
    =
    \bar\gamma \sigma_0
\end{equation}
Since $\sigma_0 = \sigma/\gamma_0$,
\begin{equation}
    \bar E_y
    =
    \left( \frac{\bar\gamma}{\gamma_0} \right)
    \frac{\sigma}{\epsilon_0}
    ,\quad
    \bar B_z
    =
    -\left( \frac{\bar\gamma}{\gamma_0} \right)
    \mu_0 \sigma \bar v
\end{equation}
we can show that
\begin{equation}
    \frac{\bar\gamma}{\gamma_0}
    =
    \frac{\sqrt{1 - v_0^2/c^2}}{\sqrt{1 - \bar v^2/c^2}}
    =
    \frac{1 + vv_0/c^2}{\sqrt{1 - v^2/c^2}}
    =
    \gamma \left( 1 + \frac{vv_0}{c^2} \right)
\end{equation}
where
\begin{equation}
    \gamma
    =
    \frac{1}{\sqrt{1 - v^2/c^2}}
\end{equation}
Thus, writing $\bar E_y$ in terms of the components of $\mathbf E$ and $\mathbf B$ in $\mathcal S$,
\begin{equation}
    \bar E_y
    =
    \gamma
    \left( 1 + \frac{vv_0}{c^2} \right)
    \frac{\sigma}{\epsilon_0}
    =
    \gamma
    \left( E_y - \frac{v}{c^2 \epsilon_0 \mu_0} B_z \right)
\end{equation}
whereas $\bar B_z$ reads
\begin{equation}
    \bar B_z
    =
    -\gamma
    \left( 1 + \frac{vv_0}{c^2} \right)
    \mu_0 \sigma
    \left( \frac{v + v_0}{1 + vv_0/c^2} \right)
    =
    \gamma
    \left( B_z - \mu_0 \epsilon_0 v E_y \right)
\end{equation}
Or, since $\mu_0 \epsilon_0 = 1/c^2$,
\begin{equation}
    \begin{aligned}
        \bar E_y
        &=
        \gamma
        \left( E_y - v B_z \right)
        \\
        \bar B_z
        &=
        \gamma
        \left( B_z - \frac{v}{c^2} E_y \right)
    \end{aligned}
\end{equation}
This tells us how $E_y$ and $B_z$ transform.
\\

To do $E_z$ and $B_y$, we simply align the same capacitor parallel to the $xy$ plane.
The fields in $\mathcal S$ are then
\begin{equation}
    E_z = \frac{\sigma}{\epsilon_0}
    ,\quad
    B_y = \mu_0 \sigma v_0
\end{equation}
And following the same argument,
\begin{equation}
    \begin{aligned}
        \bar E_z
        &=
        \gamma
        \left( E_z + v B_y \right)
        \\
        \bar B_y
        &=
        \gamma
        \left( B_y + \frac{v}{c^2} E_z \right)
    \end{aligned}
\end{equation}
As for the $x$ components, we have the transformation rule for the electric field
\begin{equation}
    \bar E_x
    =
    E_x
\end{equation}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-traveling_solenoid-01}
    \caption{Solenoid moving in the $x$ direction.}%
    \label{fig:ch12:TravelingSolenoid}
\end{figure}
For the magnetic field, consider a long solenoid aligned parallel to the $x$ axis and at rest in $\mathcal S$ (Fig.~\ref{fig:ch12:TravelingSolenoid}).
The magnetic field within the coil is
\begin{equation}
    B_x
    =
    \mu_0 n I
    =
    \mu_0 n \frac{dq}{dt}
\end{equation}
where $n$ is the number of turns per unit length, and $I$ is the current in the wire (i.e., charge per unit time, $dq/dt$).
\\

In system $\bar{\mathcal S}$, the length contracts, so $n$ increases
\begin{equation}
    \bar n = \gamma n
\end{equation}
On the other hand, due to time dilation, $d\bar t = \gamma dt$,
\begin{equation}
    \bar I
    =
    \frac{dq}{d\bar t}
    =
    \frac{1}{\gamma}
    \frac{dq}{dt}
    =
    \frac{1}{\gamma} I
\end{equation}
The two factors of $\gamma$ cancels, and we conclude
\begin{equation}
    \bar B_x
    =
    B_x
\end{equation}
Here is then the complete set of transformation rules
\begin{equation}
    \boxed{
        \begin{array}{lll}
            \displaystyle
            \bar E_x = E_x,
            &
            \displaystyle
            \bar E_y = \gamma \left( E_y - v B_z \right),
            &
            \displaystyle
            \bar E_z = \gamma \left( E_z + v B_y \right)
            \\\\
            \displaystyle
            \bar B_x = B_x,
            &
            \displaystyle
            \bar B_y = \gamma \left( B_y + \frac{v}{c^2} E_z \right),
            &
            \displaystyle
            \bar B_z = \gamma \left( B_z - \frac{v}{c^2} E_y \right)
        \end{array}
    }
    \label{eq:ch12:FieldsTransformationRules}
\end{equation}
\\

\textbf{Two special cases}:
\begin{enumerate}
    \item If $\mathbf B = 0$ in $\mathcal S$, then
        \begin{equation}
            \bar{\mathbf B}
            =
            \gamma \frac{v}{c^2}
            \left( E_z \uvec y - E_y \uvec z \right)
            =
            \frac{v}{c^2}
            \left( \bar E_z \uvec y - \bar E_y \uvec z \right)
        \end{equation}
        or, since $\mathbf v = v \uvec x$,
        \begin{equation}
            \bar{\mathbf B}
            =
            -\frac{1}{c^2}
            (\mathbf v \times \bar{\mathbf E})
        \end{equation}

    \item If $\mathbf E = 0$ in $\mathcal S$, then
        \begin{equation}
            \bar{\mathbf E}
            =
            -\gamma v \left( B_z \uvec y - B_y \uvec z \right)
            =
            -v (\bar B_z \uvec y - \bar B_y \uvec z)
        \end{equation}
        or, since $\mathbf v = v\uvec x$,
        \begin{equation}
            \bar{\mathbf E}
            =
            \mathbf v \times \bar{\mathbf B}
        \end{equation}
\end{enumerate}

\framed{%
    \example{12.15}%
    \textbf{Magnetic field of a point charge in uniform motion}.
    Find the magnetic field of a point charge $q$ moving at constant velocity $\mathbf v$.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-example_12_15-01}
    \caption{Example 12.15.}%
    \label{fig:ch12:Example12_15}
\end{figure}
In the particle's rest frame the magnetic field is zero (everywhere), so in a system moving with velocity $-\mathbf v$,
\begin{equation}
    \mathbf B
    =
    \frac{1}{c^2}
    (\mathbf v \times \mathbf E)
\end{equation}
where the electric field is given by Eq.~\ref{eq:ch12:ElectricFieldOfPointChargeConstantSpeed}
\begin{equation}
    \mathbf E
    =
    \frac{1}{4\pi\epsilon_0}
    \frac{q (1 - v^2/c^2)}{(1 - (v^2/c^2) \sin^2\theta)^{3/2}}
    \frac{\uvec R}{R^2}
\end{equation}
With the help of the diagram in Fig.~\ref{fig:ch12:Example12_15}, we can work out for the magnetic field
\begin{equation}
    \mathbf B
    =
    \frac{\mu_0}{4\pi}
    \frac{qv (1 - v^2/c^2) \sin\theta}{(1 - (v^2/c^2) \sin^2\theta)^{3/2}}
    \frac{\uvec\phi}{R^2}
\end{equation}
In the nonrelativistic limit $(v^2 \ll c^2)$,
\begin{equation}
    \mathbf B
    \approx
    \frac{\mu_0}{4\pi}
    \frac{q\mathbf v \times \uvec R}{R^2}
\end{equation}
which is exactly what you would get by na\"ive application of the Biot-Savart law to a point charge.

\homework{12.43, 12.45, 12.48}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Field Tensor}
$\mathbf E$ and $\mathbf B$ in Eq.~\ref{eq:ch12:FieldsTransformationRules} do not transform like the spatial parts of the two 4-vectors.
\\
It turns out both form an \textbf{antisymmetric, second-rank tensor}.
\\

Recall the transformation rule of a 4-vector
\begin{equation}
    \bar a^\mu
    =
    {\Lambda^\mu}_\nu
    a^\nu
\end{equation}
if $\bar{\mathcal S}$ is moving with respect to $\mathcal S$ with speed $v$ in the $x$ direction,
where
\begin{equation}
    {\Lambda^\mu}_\nu
    =
    \left(
        \begin{matrix}
            \gamma & -\gamma \beta & 0 & 0 \\
            -\gamma \beta & \gamma & 0 & 0 \\
            0 & 0 & 1 & 0 \\
            0 & 0 & 0 & 1 \\
        \end{matrix}
    \right)
\end{equation}

Similarly, a second-rank tensor is an object with two indices, which transforms like
\begin{equation}
    \bar t^{\mu\nu}
    =
    {\Lambda^\mu}_\lambda
    {\Lambda^\nu}_\sigma
    t^{\lambda\sigma}
\end{equation}
A symmetric tensor has the property
\begin{equation}
    t^{\mu\nu}
    =
    t^{\nu\mu}
\end{equation}
so that there are 10 distinct components.
\\
Similarly, an antisymmetric tensor obeys
\begin{equation}
    t^{\mu\nu}
    =
    -t^{\nu\mu}
\end{equation}
Such an object has 6 distinct elements
\begin{equation}
    t^{\mu\nu}
    =
    \left(
        \begin{matrix}
            0 & t^{01} & t^{02} & t^{03} \\
            -t^{01} & 0 & t^{12} & t^{13} \\
            -t^{02} & -t^{12} & 0 & t^{23} \\
            -t^{03} & -t^{13} & -t^{23} & 0 \\
        \end{matrix}
    \right)
\end{equation}

Let's see how the transformation rule works for an antisymmetric tensor.
For the element $\bar t^{01}$,
\begin{equation}
    \bar t^{01}
    =
    {\Lambda^0}_\lambda
    {\Lambda^1}_\sigma
    t^{\lambda\sigma}
\end{equation}
But, since ${\Lambda^0}_\lambda = 0$ unless $\lambda = 0$ or $1$,
and ${\Lambda^1}_\sigma = 0$ unless $\sigma = 0$ or $1$, it follows
\begin{equation}
    \begin{aligned}
        \bar t^{01}
        &=
        {\Lambda^0}_0
        \left( {\Lambda^1}_0\cancelto{\color{blue}0}{t^{00}} + {\Lambda^1}_1 t^{01} \right)
        \\
        &+
        {\Lambda^0}_1
        \left( {\Lambda^1}_0\cancelto{\color{blue}-t^{01}}{t^{10}} + {\Lambda^1}_1 \cancelto{\color{blue}0}{t^{11}} \right)
        \\
        &=
        \left( {\Lambda^0}_0 {\Lambda^1}_1 - {\Lambda^0}_1 {\Lambda^1}_0 \right) t^{01}
        \\
        &=
        \underbrace{\left( \gamma^2 - (\gamma\beta)^2 \right)}_{\color{blue} = 1}
        t^{01}
        \\
        &=
        t^{01}
    \end{aligned}
    \label{eq:ch12:TensorTransformationRules:1}
\end{equation}
Working out other components as well, the complete set of transformation rules is
\begin{equation}
    \begin{array}{lll}
        \displaystyle
        \bar t^{01} = t^{01},
        &
        \displaystyle
        \bar t^{02} = \gamma \left( t^{02} - \beta t^{12} \right),
        &
        \displaystyle
        \bar t^{03} = \gamma \left( t^{03} + \beta t^{31} \right)
        \\\\
        \displaystyle
        \bar t^{23} = t^{23},
        &
        \displaystyle
        \bar t^{31} = \gamma \left( t^{31} + \beta t^{03} \right),
        &
        \displaystyle
        \bar t^{12} = \gamma \left( t^{12} - \beta t^{02} \right)
    \end{array}
    \label{eq:ch12:FieldsTransformationRules:1}
\end{equation}
Comparing with Eq.~\ref{eq:ch12:FieldsTransformationRules}
\begin{equation}
    \begin{array}{lll}
        \displaystyle
        \frac{\bar E_x}{\color{blue}c}
        =
        \frac{E_x}{\color{blue}c},
        &
        \displaystyle
        \frac{\bar E_y}{\color{blue}c}
        =
        \gamma \left( \frac{E_y}{\color{blue}c} - \frac{v}{\color{blue}c} B_z \right),
        &
        \displaystyle
        \frac{\bar E_z}{\color{blue}c}
        =
        \gamma \left( \frac{E_z}{\color{blue}c} + \frac{v}{\color{blue}c} B_y \right)
        \\\\
        \displaystyle
        \bar B_x = B_x,
        &
        \displaystyle
        \bar B_y = \gamma \left( B_y + \frac{v}{c} \frac{E_z}{c} \right),
        &
        \displaystyle
        \bar B_z = \gamma \left( B_z - \frac{v}{c} \frac{E_y}{c} \right)
    \end{array}
\end{equation}
we can construct the \textbf{field tensor} $F^{\mu\nu}$
\begin{equation}
    \begin{array}{lll}
        \displaystyle
        F^{01}
        \equiv
        \frac{E_x}{c},
        &
        \displaystyle
        F^{02}
        \equiv
        \frac{E_y}{c}
        &
        \displaystyle
        F^{03}
        \equiv
        \frac{E_z}{c}
        \\\\
        \displaystyle
        F^{23}
        \equiv
        B_x,
        &
        \displaystyle
        F^{31}
        \equiv
        B_y
        &
        \displaystyle
        F^{12}
        \equiv
        B_z
    \end{array}
\end{equation}
or
\begin{equation}
    F^{\mu\nu}
    =
    \left(
        \begin{matrix}
            0 & E_x/c & E_y/c & E_z/c \\
            -E_x/c & 0 & B_z & -B_y \\
            -E_y/c & -B_z & 0 & B_x \\
            -E_z/c & B_y & -B_x & 0 \\
        \end{matrix}
    \right)
\end{equation}

One can also construct a \textbf{dual tensor} $G^{\mu\nu}$, by comparing the first line of Eq.~\ref{eq:ch12:TensorTransformationRules:1} to the second line of Eq.~\ref{eq:ch12:FieldsTransformationRules:1},
and the second line of Eq.~\ref{eq:ch12:TensorTransformationRules:1} to the first line of Eq.~\ref{eq:ch12:FieldsTransformationRules:1}
\begin{equation}
    G^{\mu\nu}
    =
    \left(
        \begin{matrix}
            0 & B_x & B_y & B_z \\
            -B_x & 0 & -E_z/c & E_y/c \\
            -B_y & E_z/c & 0 & -E_x/c \\
            -B_z & -E_y/c & E_x/c & 0 \\
        \end{matrix}
    \right)
\end{equation}
$G^{\mu\nu}$ can be obtained directly from $F^{\mu\nu}$ by substitution:
$\mathbf E/c \rightarrow \mathbf B$ and $\mathbf B \rightarrow -\mathbf E/c$.
Notice that this operation leaves Eq.~\ref{eq:ch12:FieldsTransformationRules} unchanged.

\homework{12.51, 12.52}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Electrodynamics in Tensor Notation}
It's time to reformulate the laws of electrodynamics (i.e., Maxwell's equations and Lorentz force law) in tensor language.
\\

To begin with, we must determine how the sources of the fields, $\rho$ and $\mathbf J$, transform.
By definition,
\begin{equation}
    \rho = \frac{Q}{V}
    \quad\text{and}\quad
    \mathbf J = \rho\mathbf u
\end{equation}
In the rest frame of charge, the proper charge density $\rho_0$ is
\begin{equation}
    \rho_0 = \frac{Q}{V_0}
\end{equation}
Because only one dimension is Lorentz-contracted,
\begin{equation}
    V = \sqrt{1 - u^2/c^2} V_0
\end{equation}
so
\begin{equation}
    {\color{blue}c}
    \rho
    =\rho_0
    \frac{\color{blue}c}{\sqrt{1 - u^2/c^2}}
    ,\quad
    \mathbf J
    =
    \rho_0
    \frac{\mathbf u}{\sqrt{1 - u^2/c^2}}
\end{equation}

Evidently, by comparing with 4-velocity, $\eta^\mu = (\gamma c, \gamma \mathbf u)$, and knowing that $\rho_0$ is invariant, charge density and current density go together to make a 4-vector
\begin{equation}
    J^\mu = \rho_0 \eta^\mu
\end{equation}
whose components are
\begin{equation}
    J^\mu = (c\rho, J_x, J_y, J_z)
\end{equation}
which we call the \textbf{current density 4-vector}.
\\

The continuity equation
\begin{equation}
    \nabla \cdot \mathbf J
    =
    -\frac{\partial\rho}{\partial t}
\end{equation}
takes a nice compact form when written in terms of $J^\mu$
\begin{equation}
    \nabla \cdot \mathbf J
    =
    \frac{\partial J_x}{\partial x}
    +
    \frac{\partial J_y}{\partial y}
    +
    \frac{\partial J_z}{\partial z}
    =
    \sum_{i = 1}^3 \frac{\partial J^i}{\partial x^i}
\end{equation}
while
\begin{equation}
    \frac{\partial \rho}{\partial t}
    =
    \frac{1}{c}
    \frac{\partial J^0}{\partial t}
    =
    \frac{\partial J^0}{\partial x^0}
\end{equation}
Hence, the continuity equation becomes
\begin{equation}
    \frac{\partial J^\mu}{\partial x^\mu}
    =
    0
\end{equation}
denoting the \textbf{four-dimensional divergence}.
\\

Maxwell's equations can be written as
\begin{equation}
    \frac{\partial F^{\mu\nu}}{\partial x^\nu}
    =
    \mu_0 J^\mu
    ,\quad
    \frac{\partial G^{\mu\nu}}{\partial x^\nu}
    =
    0
\end{equation}
If $\mu = 0$,
\begin{equation}
    \begin{aligned}
        \frac{\partial F^{0\nu}}{\partial x^\nu}
        &=
        \cancelto{\color{blue}0}{\frac{\partial F^{00}}{\partial x^0}}
        +
        \frac{\partial F^{01}}{\partial x^1}
        +
        \frac{\partial F^{02}}{\partial x^2}
        +
        \frac{\partial F^{03}}{\partial x^3}
        \\
        &=
        \frac{1}{c}
        \left(
            \frac{\partial E_x}{\partial x}
            +
            \frac{\partial E_y}{\partial y}
            +
            \frac{\partial E_z}{\partial z}
        \right)
        \\
        &=
        \frac{1}{c} (\nabla \cdot \mathbf E)
        \\
        &=
        \mu_0 J^0
        =
        \mu_0 c \rho
    \end{aligned}
\end{equation}
which is Gauss's law.
\\
Similarly, if $\mu = 1$,
\begin{equation}
    \begin{aligned}
        \frac{\partial F^{1\nu}}{\partial x^\nu}
        &=
        \frac{\partial F^{10}}{\partial x^0}
        +
        \cancelto{\color{blue}0}{\frac{\partial F^{11}}{\partial x^1}}
        +
        \frac{\partial F^{12}}{\partial x^2}
        +
        \frac{\partial F^{13}}{\partial x^3}
        \\
        &=
        -\frac{1}{c^2}
        \frac{\partial E_x}{\partial t}
        +
        \frac{\partial B_z}{\partial y}
        -
        \frac{\partial B_y}{\partial z}
        \\
        &=
        (\nabla \times \mathbf B)_x
        -
        \frac{1}{c^2} \frac{\partial E_x}{\partial t}
        \\
        &=
        \mu_0 J^1
        =
        \mu_0 J_x
    \end{aligned}
\end{equation}
Combining this with the corresponding results for $\mu = 2$ and $3$ gives
Amp\`ere-Maxwell equation.
\\

Meanwhile, the second equation with $\mu = 0$ becomes
\begin{equation}
    \begin{aligned}
        \frac{\partial G^{0\nu}}{\partial x^\nu}
        &=
        \cancelto{\color{blue}0}{\frac{\partial G^{00}}{\partial x^0}}
        +
        \frac{\partial G^{01}}{\partial x^1}
        +
        \frac{\partial G^{02}}{\partial x^2}
        +
        \frac{\partial G^{03}}{\partial x^3}
        \\
        &=
        \frac{\partial B_x}{\partial x}
        +
        \frac{\partial B_y}{\partial y}
        +
        \frac{\partial B_z}{\partial z}
        \\
        &=
        \nabla \cdot \mathbf B
        \\
        &=
        0
    \end{aligned}
\end{equation}
If $\mu = 1$,
\begin{equation}
    \begin{aligned}
        \frac{\partial G^{1\nu}}{\partial x^\nu}
        &=
        \frac{\partial G^{10}}{\partial x^0}
        +
        \cancelto{\color{blue}0}{\frac{\partial G^{11}}{\partial x^1}}
        +
        \frac{\partial G^{12}}{\partial x^2}
        +
        \frac{\partial G^{13}}{\partial x^3}
        \\
        &=
        -\frac{1}{c}
        \frac{\partial B_x}{\partial t}
        -\frac{1}{c}
        \frac{\partial E_z}{\partial y}
        +\frac{1}{c}
        \frac{\partial E_y}{\partial z}
        \\
        &=
        -\frac{1}{c} \frac{\partial B_x}{\partial t}
        -(\nabla \times \mathbf E)_x
        \\
        &=
        0
    \end{aligned}
\end{equation}
Combining this with the corresponding results for $\mu = 2$ and $3$ yields Faraday's law.
\\

In terms of $F^{\mu\nu}$ and $\eta^\mu$, the Minkowski force on a charge $q$ is given by
\begin{equation}
    K^\mu
    =
    q \eta_\nu F^{\mu\nu}
    \label{eq:ch12:RelativisticLorentzForceLaw}
\end{equation}
where
\begin{equation}
    \eta_\nu
    =
    (-\eta^0, \eta^1, \eta^2, \eta^3)
\end{equation}
If $\mu = 0$,
\begin{equation}
    K^0
    =
    q \eta_\nu F^{0\nu}
    =
    \frac{q}{c} (\eta_j E_j)
    =
    \frac{q}{c} \frac{1}{\sqrt{1 - u^2/c^2}} (\mathbf u \cdot \mathbf E)
\end{equation}
Since $K^0 = dE/d\tau = \gamma dE/dt$, it follows
\begin{equation}
    \frac{dE}{dt} = q (\mathbf u \cdot \mathbf E)
\end{equation}
which is the power delivered to $q$ by the electromagnetic fields.
\\
If $\mu = 1$,
\begin{equation}
    \begin{aligned}
        K^1
        &=
        q \eta_\nu F^{1\nu}
        \\
        &=
        q ( -\eta^0 F^{10} +\eta^1 \cancelto{\color{blue}0}{F^{11}} +\eta^2 F^{12} +\eta^3 F^{13})
        \\
        &=
        q
        \left(
            \frac{-c}{\sqrt{1 - u^2/c^2}}
            \left( \frac{-E_x}{c} \right)
            +
            \frac{u_y}{\sqrt{1 - u^2/c^2}}
            (B_z)
            +
            \frac{u_z}{\sqrt{1 - u^2/c^2}}
            (-B_y)
        \right)
        \\
        &=
        \frac{q}{\sqrt{1 - u^2/c^2}}
        \left( E_x + (\mathbf u \times \mathbf B)_x \right)
    \end{aligned}
\end{equation}
Combining this with the corresponding results for $\mu = 2$ and $3$ yields
\begin{equation}
    \mathbf K
    =
    \frac{q}{\sqrt{1 - u^2/c^2}}
    (\mathbf E + \mathbf u \times \mathbf B)
\end{equation}
In terms of the ``ordinary'' force,
\begin{equation}
    \mathbf F = q (\mathbf E + \mathbf u \times \mathbf B)
\end{equation}
Therefore, Eq.~\ref{eq:ch12:RelativisticLorentzForceLaw}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Relativistic Potentials}
Recall that the electric and magnetic fields can be expressed in terms of a scalar potential $V$ and vector potential $\mathbf A$
\begin{equation}
    \mathbf E
    =
    -\nabla V
    -\frac{\partial \mathbf A}{\partial t}
    ,\quad
    \mathbf B
    =
    \nabla \times \mathbf A
    \label{eq:ch12:EBbyPotentials}
\end{equation}
As guessed, $V$ and $\mathbf A$ together constitute a 4-vector
\begin{equation}
    A^\mu
    =
    (V/c, A_x, A_y, A_z)
\end{equation}
In terms of this \textbf{4-vector potential}, the field tensor can be written as
\begin{equation}
    F^{\mu\nu}
    =
    \frac{\partial A^\nu}{\partial x_\mu}
    -
    \frac{\partial A^\mu}{\partial x_\nu}
    \label{eq:ch12:FieldTensorByPotentials}
\end{equation}
(Observe that the differentiation is with respect to the \textit{covariant} vectors whose temporal part differs by a minus sign: $x_0 = -x^0$.)
\\

To check that Eq.~\ref{eq:ch12:FieldTensorByPotentials} is equivalent to Eq.~\ref{eq:ch12:EBbyPotentials},
for $\mu = 0$ and $\nu = 1$, we get
\begin{equation}
    \begin{aligned}
        F^{01}
        &=
        \frac{\partial A^1}{\partial x_0}
        -
        \frac{\partial A^0}{\partial x_1}
        =
        -\frac{\partial A_x}{\partial(ct)}
        -\frac{1}{c} \frac{\partial V}{\partial x}
        \\
        &=
        \frac{1}{c}
        \left( -(\nabla V)_x - \frac{\partial A_x}{\partial t} \right)
        =
        \frac{E_x}{c}
    \end{aligned}
\end{equation}
Combining this with the corresponding results for $\nu = 2$ and $3$ gives
the first equation of Eq.~\ref{eq:ch12:EBbyPotentials}.
\\

For $\mu = 1$ and $\nu = 2$, we get
\begin{equation}
    \begin{aligned}
        F^{12}
        &=
        \frac{\partial A^2}{\partial x_1}
        -
        \frac{\partial A^1}{\partial x_2}
        =
        \frac{\partial A_y}{\partial x}
        -
        \frac{\partial A_x}{\partial y}
        \\
        &=
        (\nabla \times \mathbf A)_z
        =
        B_z
    \end{aligned}
\end{equation}
which, together with the corresponding results for $F^{23}$ and $F^{31}$, is the second equation in Eq.~\ref{eq:ch12:EBbyPotentials}.
\\

The potential formulation automatically takes care of the homogeneous Maxwell equation, $\partial G^{\mu\nu}/\partial x^\nu = 0$.
\\

The inhomogeneous equation, $\partial F^{\mu\nu}/\partial x^\nu = \mu_0 J^\mu$, becomes
\begin{equation}
    \frac{\partial}{\partial x_\mu}
    \left( \frac{\partial A^\nu}{\partial x^\nu} \right)
    -
    \frac{\partial}{\partial x_\nu}
    \left( \frac{\partial A^\mu}{\partial x^\nu} \right)
    =
    \mu_0 J^\mu
    \label{eq:ch12:InhomogeneousMaxwellEquationByPotentials}
\end{equation}
To bring this to a familiar form, we take advantage of the \textbf{gauge invariance}.
That is, we can adjust the potentials without changing $F^{\mu\nu}$.
In fact, it's clear from Eq.~\ref{eq:ch12:FieldTensorByPotentials} that you could add to $A^\mu$ the gradient of any scalar function $\lambda$
\begin{equation}
    A^\mu
    \quad\rightarrow\quad
    {A^{\mu}}'
    =
    A^\mu + \frac{\partial\lambda}{\partial x_\mu}
\end{equation}
without changing $F^{\mu\nu}$.
\\

In particular, the Lorenz gauge condition (Eq.~\ref{eq:ch10:LorenzGauge})
\begin{equation}
    \nabla \cdot \mathbf A
    =
    -\frac{1}{c^2} \frac{\partial V}{\partial t}
\end{equation}
becomes in relativistic form
\begin{equation}
    \frac{\partial A^\mu}{\partial x^\mu} = 0
\end{equation}
So, in Lorenz gauge, Eq.~\ref{eq:ch12:InhomogeneousMaxwellEquationByPotentials} reduces to
\begin{equation}
    \Box^2 A^\mu
    =
    -\mu_0 J^\mu
\end{equation}
where $\Box^2$ is the d'Alembertian
\begin{equation}
    \Box^2
    =
    \frac{\partial}{\partial x_\nu}
    \frac{\partial}{\partial x^\nu}
    =
    \nabla^2 - \frac{1}{c^2} \frac{\partial^2}{\partial t^2}
\end{equation}
