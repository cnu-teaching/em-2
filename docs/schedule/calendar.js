const ec = new EventCalendar(document.getElementById('ec'), {
    view: 'dayGridMonth',
    headerToolbar: {
        start: 'prev,next today',
        center: 'title',
        end: 'dayGridMonth,timeGridWeek,listWeek'
    },
    hiddenDays: [ 0, 1, 2, 3, 6 ],
    scrollTime: '09:00:00',
    events: createEvents(),
    height: '800px',
    dayMaxEvents: true,
    nowIndicator: true,
    selectable: true,
    editable: false,
    eventStartEditable: false,
    eventDurationEditable: false
});

function createEvents() {
    return [
        {start: "2024-09-05 14:00:00", end: "2024-09-05 15:00:00", title: "강의 소개", color: "#d3869b"},
        {start: "2024-09-12 14:00:00", end: "2024-09-12 15:00:00", title: "강의와 과제 제출", color: "#d3869b"},
        {start: "2024-09-19 14:00:00", end: "2024-09-19 15:00:00", title: "강의와 과제 제출", color: "#d3869b"},
        {start: "2024-09-26 14:00:00", end: "2024-09-26 15:00:00", title: "강의와 과제 제출", color: "#d3869b"},
        {start: "2024-10-10 14:00:00", end: "2024-10-10 15:00:00", title: "강의와 과제 제출", color: "#d3869b"},
        {start: "2024-10-17 14:00:00", end: "2024-10-17 15:00:00", title: "강의와 과제 제출", color: "#d3869b"},
        {start: "2024-10-24 14:00:00", end: "2024-10-24 15:00:00", title: "중간시험(과제 제출)", color: "#506bbd"},
        {start: "2024-10-31 14:00:00", end: "2024-10-31 15:00:00", title: "강의와 과제 제출", color: "#d3869b"},
        {start: "2024-11-07 14:00:00", end: "2024-11-07 15:00:00", title: "강의와 과제 제출", color: "#d3869b"},
        {start: "2024-11-14 14:00:00", end: "2024-11-14 15:00:00", title: "강의와 과제 제출", color: "#d3869b"},
        {start: "2024-11-21 14:00:00", end: "2024-11-21 15:00:00", title: "강의와 과제 제출", color: "#d3869b"},
        {start: "2024-11-28 14:00:00", end: "2024-11-28 15:00:00", title: "강의와 과제 제출", color: "#d3869b"},
        {start: "2024-12-05 14:00:00", end: "2024-12-05 15:00:00", title: "강의와 과제 제출", color: "#d3869b"},
        {start: "2024-12-12 14:00:00", end: "2024-12-12 15:00:00", title: "기말시험", color: "#d3869b"},
        //{start: "2024-12-19 14:00:00", end: "2024-12-19 15:00:00", title: "기말시험 기간", color: "#d3869b"},
        //
        {start: "2024-09-06 13:00:00", end: "2024-09-06 15:00:00", title: "강의", color: "#d3869b"},
        {start: "2024-09-13 13:00:00", end: "2024-09-13 15:00:00", title: "강의", color: "#d3869b"},
        {start: "2024-09-20 13:20:00", end: "2024-09-20 15:00:00", title: "강의", color: "#d3869b"},
        {start: "2024-09-27 13:00:00", end: "2024-09-27 15:00:00", title: "강의", color: "#d3869b"},
        {start: "2024-10-04 13:00:00", end: "2024-10-04 15:00:00", title: "휴강", color: "#cc241d"},
        {start: "2024-10-11 13:00:00", end: "2024-10-11 15:00:00", title: "강의", color: "#d3869b"},
        {start: "2024-10-18 13:00:00", end: "2024-10-18 15:00:00", title: "강의", color: "#d3869b"},
        {start: "2024-10-25 13:00:00", end: "2024-10-25 15:00:00", title: "휴강(중간시험)", color: "#aad386"},
        {start: "2024-11-01 13:00:00", end: "2024-11-01 15:00:00", title: "강의", color: "#d3869b"},
        {start: "2024-11-08 13:00:00", end: "2024-11-08 15:00:00", title: "강의", color: "#d3869b"},
        {start: "2024-11-15 13:00:00", end: "2024-11-15 15:00:00", title: "강의", color: "#d3869b"},
        {start: "2024-11-22 13:00:00", end: "2024-11-22 15:00:00", title: "강의", color: "#d3869b"},
        {start: "2024-11-29 13:00:00", end: "2024-11-29 15:00:00", title: "강의", color: "#d3869b"},
        {start: "2024-12-06 13:00:00", end: "2024-12-06 15:00:00", title: "강의", color: "#d3869b"},
        //{start: "2024-12-20 13:00:00", end: "2024-12-20 15:00:00", title: "기말시험 기간", color: "#d3869b"},
        //
        {start: "2024-10-03", end: "2024-10-03", allDay: true, color: "#cc241d", title: "휴교 (개천절)"},
    ];
};
