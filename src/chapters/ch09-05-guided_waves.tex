\section{Guided Waves}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Wave Guides}
\begin{figure}[ht]
    \centering
    \includegraphics[width=15pc]{assets/ch09-wave_guides-01}
    \caption{Wave guide.}%
    \label{fig:ch09:WaveGuide}
\end{figure}
\framed{%
    \textbf{Wave guide}:
    Electromagnetic waves confined to the interior of a hollow pipe (Fig. \ref{fig:ch09:WaveGuide}).
}
We will assume that the wave guide is a perfect conductor, resulting in $\mathbf E = 0$ and $\mathbf B = 0$ inside material.

Therefore, the boundary conditions at the inner surface are
\begin{equation}
    \begin{array}{cl}
        \text{(i)}
        & \displaystyle
        \mathbf E^{\|} = 0
        \\\\
        \text{(ii)}
        & \displaystyle
        B^{\perp} = 0
    \end{array}
    \label{eq:ch09:BoundaryConditions:WaveGuide}
\end{equation}
Free charges and currents will be introduced on the surface in such a way as to enforce these constraints.
\\

This supports monochromatic waves that propagate down the tube:
\begin{equation}
    \begin{array}{cl}
        \text{(i)}
        & \displaystyle
        \tilde{\mathbf E}(x, y, z, t)
        =
        \tilde{\mathbf E}_{0}(x, y)
        e^{i(kz - \omega t)}
        \\\\
        \text{(ii)}
        & \displaystyle
        \tilde{\mathbf B}(x, y, z, t)
        =
        \tilde{\mathbf B}_{0}(x, y)
        e^{i(kz - \omega t)}
    \end{array}
    \label{eq:ch09:PlaneWave:WaveGuide}
\end{equation}
which must satisfy Maxwell's equations in the interior of the wave guide:
\begin{equation}
    \begin{array}{cllcll}
        \mathrm{(i)}
        &
        \displaystyle \nabla \cdot \mathbf E = 0
        &&
        \mathrm{(iii)}
        &
        \displaystyle \nabla \times \mathbf E = -\frac{\partial\mathbf B}{\partial t}
        \\\\
        \mathrm{(ii)}
        &
        \displaystyle \nabla \cdot \mathbf B = 0
        &&
        \mathrm{(iv)}
        &
        \displaystyle \nabla \times \mathbf B = \frac{1}{c^2} \frac{\partial\mathbf E}{\partial t}
    \end{array}
\end{equation}
The problem is then to find $\tilde{\mathbf E}_0$ and $\tilde{\mathbf B}_0$.
\\

\textit{Confined waves are not (in general) transverse};
in order to fit the boundary conditions, we shall have to include the longitudinal components (i.e., $E_z$ and $B_z$):
\begin{equation}
    \tilde{\mathbf E}_0(x, y)
    =
    E_x\uvec x
    +
    E_y\uvec y
    +
    E_z\uvec z
    ,\quad
    \tilde{\mathbf B}_0(x, y)
    =
    B_x\uvec x
    +
    B_y\uvec y
    +
    B_z\uvec z
\end{equation}
Putting them into (iii) and (iv),
\begin{equation}
    \begin{array}{cllcll}
        \mathrm{(i)}
        & \displaystyle
        \frac{\partial E_y}{\partial x}
        -
        \frac{\partial E_x}{\partial y}
        =
        i\omega B_z
        &&
        \mathrm{(iv)}
        & \displaystyle
        \frac{\partial B_y}{\partial x}
        -
        \frac{\partial B_x}{\partial y}
        =
        -\frac{i\omega}{c^2} E_z
        \\\\
        \mathrm{(ii)}
        & \displaystyle
        \frac{\partial E_z}{\partial y}
        -
        ik E_y
        =
        i\omega B_x
        &&
        \mathrm{(v)}
        & \displaystyle
        \frac{\partial B_z}{\partial y}
        -
        ik B_y
        =
        -\frac{i\omega}{c^2} E_x
        \\\\
        \mathrm{(iii)}
        & \displaystyle
        ik E_x
        -
        \frac{\partial E_z}{\partial x}
        =
        i\omega B_y
        &&
        \mathrm{(vi)}
        & \displaystyle
        ik B_x
        -
        \frac{\partial B_z}{\partial x}
        =
        -\frac{i\omega}{c^2} E_y
    \end{array}
    \label{eq:ch09:WaveGuide:FaradayAmpere}
\end{equation}
Equations (ii), (iii), (v), and (vi) can be combined to solve for $E_x$, $E_y$, $B_x$, and $B_y$:
\begin{equation}
    \begin{array}{cl}
        \text{(i)}
        & \displaystyle
        E_x
        =
        \frac{i}{(\omega/c)^2 - k^2}
        \left(
            k\frac{\partial E_z}{\partial x}
            +
            \omega\frac{\partial B_z}{\partial y}
        \right)
        \\\\
        \text{(ii)}
        & \displaystyle
        E_y
        =
        \frac{i}{(\omega/c)^2 - k^2}
        \left(
            k\frac{\partial E_z}{\partial y}
            -
            \omega\frac{\partial B_z}{\partial x}
        \right)
        \\\\
        \text{(iii)}
        & \displaystyle
        B_x
        =
        \frac{i}{(\omega/c)^2 - k^2}
        \left(
            k\frac{\partial B_z}{\partial x}
            -
            \frac{\omega}{c^2} \frac{\partial E_z}{\partial y}
        \right)
        \\\\
        \text{(iv)}
        & \displaystyle
        B_y
        =
        \frac{i}{(\omega/c)^2 - k^2}
        \left(
            k\frac{\partial B_z}{\partial y}
            +
            \frac{\omega}{c^2} \frac{\partial E_z}{\partial x}
        \right)
    \end{array}
    \label{eq:ch09:WaveGuide:ExEyBxBy}
\end{equation}
So, once we know $E_z$ and $B_z$, all other components can be trivially obtained.
\\

Inserting Eq. \ref{eq:ch09:WaveGuide:ExEyBxBy} into (i) and (iv) of Eq. \ref{eq:ch09:WaveGuide:FaradayAmpere} yields uncoupled equations for $E_z$ and $B_z$:
\begin{equation}
    \begin{array}{cl}
        \text{(i)}
        & \displaystyle
        \left(
            \frac{\partial^2}{\partial x^2}
            +
            \frac{\partial^2}{\partial y^2}
            +
            \left( \frac{\omega}{c} \right)^2
            -
            k^2
        \right)
        E_z = 0
        \\\\
        \text{(ii)}
        & \displaystyle
        \left(
            \frac{\partial^2}{\partial x^2}
            +
            \frac{\partial^2}{\partial y^2}
            +
            \left( \frac{\omega}{c} \right)^2
            -
            k^2
        \right)
        B_z = 0
    \end{array}
    \label{eq:ch09:WaveGuide:EzBz}
\end{equation}
So, the wave guide admits
\begin{itemize}
    \item TE (transverse electric) waves when $E_z = 0$;
    \item TM (transverse magnetic) waves when $B_z = 0$; and
    \item TEM waves when both $E_z$ and $B_z$ are zero.

        (TEM waves cannot occur in a \textit{hollow} wave guide.)
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{TE Waves in a Rectangular Wave Guide}
\begin{figure}[ht]
    \centering
    \includegraphics[width=15pc]{assets/ch09-rectangular_wave_guide-01}
    \caption{Rectangular Wave guide.}%
    \label{fig:ch09:WaveGuide:Rectangular}
\end{figure}
Suppose we have a wave guide of rectangular shape, with height $a$ and width $b$, and we are interested in \textit{TE waves} (that is, $E_z = 0$).

The problem is to solve Eq. \ref{eq:ch09:WaveGuide:EzBz}ii,
\[
    \left(
        \frac{\partial^2}{\partial x^2}
        +
        \frac{\partial^2}{\partial y^2}
        +
        \left( \frac{\omega}{c} \right)^2
        -
        k^2
    \right)
    B_z = 0
\]
subject to the boundary condition Eq. \ref{eq:ch09:BoundaryConditions:WaveGuide}ii
\[
    B^{\perp} = 0%
    .
\]
To apply separation of variable, let
\begin{equation}
    B_z(x, y) = X(x) Y(y)
\end{equation}
Substituting it into Eq. \ref{eq:ch09:WaveGuide:EzBz}ii,
\begin{equation}
    Y \frac{d^2 X}{dx^2}
    +
    X \frac{d^2 Y}{dy^2}
    +
    \left[ \left( \frac{\omega}{c} \right)^2 - k^2 \right]
    XY
    =
    0
    \label{eq:ch09:TERectWaveGuide:1}
\end{equation}
Divide by $XY$, and the $x$- and $y$-dependent terms must equal to constants:
\begin{equation}
    \text{(i)}\ %
    \frac{1}{X} \frac{d^2 X}{dx^2}
    =
    -k_x^2
    \qquad
    \text{(ii)}\ %
    \frac{1}{Y} \frac{d^2 Y}{dy^2}
    =
    -k_y^2
    \label{eq:ch09:TERectWaveGuide:2}
\end{equation}
Substituting into Eq. \ref{eq:ch09:TERectWaveGuide:1} yields
\begin{equation}
    -k_x^2 -k_y^2
    +
    \left( \frac{\omega}{c} \right)^2
    -
    k^2 = 0
    \label{eq:ch09:TERectWaveGuide:3}
\end{equation}
The general solution to Eq. \ref{eq:ch09:TERectWaveGuide:2}i is
\begin{equation}
    X(x)
    =
    A \sin(k_x x)
    +
    B \cos(k_x x)
\end{equation}
Since the boundary condition requires $B_x = 0$ at $x = 0$ and $x = a$, according to Eq. \ref{eq:ch09:WaveGuide:ExEyBxBy}iii it follows
\begin{equation}
    \frac{\partial B_z}{\partial x}
    =
    0
    \quad\rightarrow\quad
    \frac{dX}{dx} = 0
\end{equation}
at $x = 0$ and $x = a$.
So, $A = 0$, and
\begin{equation}
    k_x = \frac{m\pi}{a}
    \quad
    (m = 0, 1, 2, \cdots)
\end{equation}
Likewise, for the $y$ component,
\begin{equation}
    k_y = \frac{n\pi}{b}
    \quad
    (n = 0, 1, 2, \cdots)
\end{equation}

So, we conclude that
\begin{equation}
    B_z(x, y)
    =
    B_0
    \cos\left( \frac{m\pi x}{a} \right)
    \cos\left( \frac{n\pi y}{b} \right)
\end{equation}
This solution is called TE$_{mn}$ mode.
(At least one index must be nonzero, because the smallest perpendicular wave length must be larger than the longest wave guide dimension.)
\\

From Eq. \ref{eq:ch09:TERectWaveGuide:3}, the wave number $k$ is
\begin{equation}
    k
    =
    \sqrt{
        \left( \frac{\omega}{c} \right)^2
        -
        \pi^2
        \left[
            \left( \frac{m}{a} \right)^2
            +
            \left( \frac{n}{b} \right)^2
        \right]
    }
    \label{eq:ch09:TERectWaveGuide:4}
\end{equation}
If
\begin{equation}
    \omega
    <
    c\pi
    \sqrt{
        \left( \frac{m}{a} \right)^2
        +
        \left( \frac{n}{b} \right)^2
    }
    \equiv
    \omega_{mn}
\end{equation}
then $k$ of Eq. \ref{eq:ch09:TERectWaveGuide:4} becomes imaginary, leading to \textit{wave attenuation}.
For this reason, $\omega_{mn}$ is called the \textbf{cutoff frequency}.
\\

The longest frequency for a given wave guide occurs for the mode TE$_{10}$:
\begin{equation}
    \omega_{10} = \frac{c\pi}{a}
\end{equation}
Frequencies less than $\omega_{10}$ will not propagate at all.
\\

One can write Eq. \ref{eq:ch09:TERectWaveGuide:4} in terms of the cutoff frequency:
\begin{equation}
    k = \frac{1}{c} \sqrt{\omega^2 - \omega_{mn}^2}
\end{equation}
The wave guide velocity is
\begin{equation}
    v \equiv \frac{\omega}{k}
    =
    \frac{c}{\sqrt{1 - (\omega_{mn}/\omega)^2}}
    > c
    \label{eq:ch09:WaveGuide:Velocity}
\end{equation}
exceeding the speed of light.
However, the energy carried by the wave travels at the group velocity:
\begin{equation}
    v_g \equiv \frac{d\omega}{dk}
    =
    \frac{1}{dk/d\omega}
    =
    c \sqrt{1 - \left( \frac{\omega_{mn}}{\omega} \right)^2}
    < c
\end{equation}
\\

\subsubsection{Visualization:}
\begin{figure}[ht]
    \centering
    \includegraphics[width=15pc]{assets/ch09-wave_guide_visualization-01}
    \caption{Wave guide TE mode visualization.}%
    \label{fig:ch09:WaveGuide:Visualization}
\end{figure}
Consider an ordinary plane wave ($\omega = k' c$), traveling at an angle $\theta$ to the $z$ axis, reflecting perfectly off each each conducting surface (Fig. \ref{fig:ch09:WaveGuide:Visualization}).
\\

In the $x$ and $y$ directions, the multiply reflected waves interfere to form standing wave patterns, of wave length $\lambda_x = 2a/m$ and $\lambda_y = 2b/n$ (hence wave number $k_x = 2\pi/\lambda_x = m\pi/a$ and $k_y = 2\pi/\lambda_y = m\pi/b$), respectively.
Meanwhile, in the $z$ direction there remains a traveling wave with wave number $k_z = k$.
\\

The propagation vector for the ``original'' plane wave is
\begin{equation}
    \mathbf k'
    =
    \frac{\pi m}{a} \uvec x
    +
    \frac{\pi n}{b} \uvec y
    +
    k \uvec z
\end{equation}
and the frequency is
\begin{equation}
    \omega = c |\mathbf k'|
    =
    c \sqrt{k^2 + \pi^2 \left[ (m/a)^2 + (n/b)^2 \right]}
    =
    \sqrt{(ck)^2 + (\omega_{mn})^2}
    \label{eq:ch09:WaveGuide:ckprime}
\end{equation}
\\

Only certain angles will lead to one of the allowed standing wave patterns:
\begin{equation}
    \cos\theta
    =
    \frac{k}{|\mathbf k'|}
    =
    \sqrt{1 - \left( \frac{\omega_{mn}}{\omega} \right)^2}
\end{equation}
where we used Eqs.~\ref{eq:ch09:WaveGuide:Velocity} and~\ref{eq:ch09:WaveGuide:ckprime}.
\\
The plane wave travels at speed $c$, but because it is going at an angle $\theta$ to the $z$ axis, its net velocity down the wave guide is
\begin{equation}
    v_g = c\cos\theta
    =
    c \sqrt{1 - (\omega_{mn}/\omega)^2}
\end{equation}
\\

The wave velocity, on the other hand, is the speed of the wave fronts.
So, the wave velocity in the $z$ direction ($v = \omega/k$) is the intersection of the (slanted) wave front lines with the surface of the wave guide:
\begin{equation}
    v = \frac{\omega}{k}
    =
    \frac{\omega}{|\mathbf k'|} \frac{1}{\cos\theta}
    =
    \frac{c}{\cos\theta}
    =
    \frac{c}{\sqrt{1 - (\omega_{mn}/\omega)^2}}
\end{equation}

\homework{9.25}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Coaxial Transmission Line}
\begin{figure}[ht]
    \centering
    \includegraphics[width=17pc]{assets/ch09-coaxial_transmission_line-01}
    \caption{Coaxial transmission line.}%
    \label{fig:ch09:CoaxialTransmissionLine}
\end{figure}
A hollow wave guide cannot support TEM waves.
But coaxial transmission line, consisting of a long straight wire of radius $a$, surrounded by a cylindrical conducting sheath of radius $b$, does admit modes with $E_z = 0$ and $B_z = 0$.
In this case, Maxwell's equations (Eq. \ref{eq:ch09:WaveGuide:FaradayAmpere}) yield
\[
    \begin{array}{rcl}
        & \displaystyle
        k = \frac{\omega}{c}
        &
        \\\\
        \displaystyle
        c B_y = E_x
        &
        \text{and}
        & \displaystyle
        c B_x = -E_y
    \end{array}
\]
and
\begin{equation}
    \left.
        \begin{array}{rcr}
            \displaystyle
            \frac{\partial E_x}{\partial x}
            +
            \frac{\partial E_y}{\partial y}
            =
            0,
            &&
            \displaystyle
            \frac{\partial E_y}{\partial x}
            -
            \frac{\partial E_x}{\partial y}
            =
            0
            \\\\
            \displaystyle
            \frac{\partial B_x}{\partial x}
            +
            \frac{\partial B_y}{\partial y}
            =
            0,
            &&
            \displaystyle
            \frac{\partial B_y}{\partial x}
            -
            \frac{\partial B_x}{\partial y}
            =
            0
        \end{array}
    \right\}
\end{equation}
These are precisely the equations of \textit{electrostatics} and \textit{magnetostatics}, for empty space, in two dimensions.
\\

Considering an infinite line charge and an infinite straight current,
we can borrow the solution with cylindrical symmetry in previous chapters (noting $B_0/E_0 = 1/c$):
\begin{equation}
    \mathbf E_0(s, \phi)
    =
    \frac{A}{s} \uvec s
    ,\quad
    \mathbf B_0(s, \phi)
    =
    \frac{A}{c s} \uvec \phi
\end{equation}
for some constant $A$.

Substituting these into Eq. \ref{eq:ch09:PlaneWave:WaveGuide} and taking the real part:
\begin{equation}
    \left.
        \begin{aligned}
            \mathbf E(s, \phi, z, t)
            &=
            \frac{A \cos(kz - \omega t)}{s} \uvec s
            \\
            \mathbf B(s, \phi, z, t)
            &=
            \frac{A \cos(kz - \omega t)}{c s} \uvec \phi
        \end{aligned}
    \right\}
\end{equation}

\homework{9.32}
