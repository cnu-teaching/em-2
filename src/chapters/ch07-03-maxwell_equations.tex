\setcounter{section}{2}
\section{Maxwell's Equations}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setcounter{subsection}{4}
\subsection{Maxwell's Equations in Matter}
Maxwell's equations
\begin{center}
    \begin{tabular}[ht]{rclcl}
        (i)
        & &
        $\displaystyle \nabla \cdot \mathbf E = \frac{\rho}{\epsilon_0}$
        & &
        (Gauss's law)
        \\\\
        (ii)
        & &
        $\displaystyle \nabla \cdot \mathbf B = 0$
        & &
        (no name)
        \\\\
        (iii)
        & &
        $\displaystyle \nabla \times \mathbf E = -\frac{\partial\mathbf B}{\partial t}$
        & &
        (Faraday's law)
        \\\\
        (iv)
        & &
        $\displaystyle \nabla \times \mathbf B = \mu_0 \mathbf J + \mu_0 \epsilon_0 \frac{\partial\mathbf E}{\partial t}$
        & &
        (Ampere's law w/ Maxwell's correction)
    \end{tabular}
\end{center}
we have learned are \textbf{complete and correct}.
\\

However, when you are working with material that are subject to electric and magnetic polarization,
there will be accumulations of ``bound'' charge and current, over which you have no direct control.
\textbf{So, it would be nice to reformulate Maxwell's equations so as to make explicit reference only to the ``free'' charges and currents.}
\\

\noindent
\textbi{\underline{Static case:}}
\begin{itemize}
    \item An electric polarization $\mathbf P$ produces a bound charge density $\rho_b$:
        \begin{equation}
            \rho_b = -\nabla \cdot \mathbf P
        \end{equation}
    \item A magnetic polarization (or ``magnetization'') $\mathbf M$ results in a bound current $\mathbf J_b$:
        \begin{equation}
            \mathbf J_b = \nabla \times \mathbf M
        \end{equation}
\end{itemize}

\noindent
\textbi{\underline{Polarization current:}}
\framed{%
    In the \textit{non}static case, any \textit{change} in the electric polarization involves a flow of (bound) charge, i.e., \textbf{polarization current} $\mathbf J_p$, which must be included in the total current.
}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch07-polarization_current}
    \caption{An illustration of polarization current.}%
    \label{fig:ch07:PolarizationCurrent}
\end{figure}
\begin{itemize}
    \item Suppose a tiny section of polarized material (Figure~\ref{fig:ch07:PolarizationCurrent}).
    \item The polarization introduces a charge density $\sigma_b = P$ at one end and $-\sigma_b$ at the other (Eq.~\ref{eq:ch04:SurfaceBoundCharge}).
    \item If $P$ \textit{increases} a bit, $\sigma_b$ increases accordingly, giving rise to a net current
        \[
            dI = \frac{\partial\sigma_b}{\partial t} da_{\perp} = \frac{\partial P}{\partial t} da_{\perp}.
        \]
    \item From the definition of current density $J = dI/da_\perp$, it follows
        \begin{equation}
            \mathbf J_p = \frac{\partial\mathbf P}{\partial t}.
            \label{eq:ch07:PolarizationCurrent}
        \end{equation}
    \item This \textbf{polarization current} has nothing to do with the bound current $\mathbf J_b$; but \underline{is the result of the linear motion of charge when the electric polarization changes}.
    \item Eq.~(\ref{eq:ch07:PolarizationCurrent}) is consistent with the continuity equation:
        \[
            \nabla \cdot \mathbf J_p
            =
            \nabla \cdot \frac{\partial\mathbf P}{\partial t}
            =
            \frac{\partial}{\partial t}(\nabla \cdot \mathbf P)
            =
            -\frac{\partial\rho_p}{\partial t}.
        \]
\end{itemize}

\noindent
\textbi{\underline{Maxwell's equations in matter:}}\\
In view of all this,
the total charge density can be separated into two parts:
\begin{equation}
    \rho = \rho_f + \rho_b = \rho_f - \nabla \cdot \mathbf P,
\end{equation}
and the current density into \textit{three} parts:
\begin{equation}
    \mathbf J
    =
    \mathbf J_f + \mathbf J_b + \mathbf J_p
    =
    \mathbf J_f + \nabla \times \mathbf M + \frac{\partial\mathbf P}{\partial t}.
\end{equation}
Gauss's law (i) can now be written as
\[
    \nabla \cdot \mathbf E
    =
    \frac{1}{\epsilon_0}
    \left( \rho_f - \nabla \cdot \mathbf P \right),
\]
or
\begin{equation}
    \nabla \cdot \mathbf D = \rho_f,
\end{equation}
where, as in the static case,
\begin{equation}
    \mathbf D \equiv \epsilon_0\mathbf E + \mathbf P.
\end{equation}
Amp\`ere's law (with Maxwell's correction; iv) becomes
\[
    \begin{aligned}
        \nabla \times \mathbf B
        &=
        \mu_0 \left( \mathbf J_f + \nabla \times \mathbf M + \frac{\partial \mathbf P}{\partial t} \right)
        +
        \mu_0\epsilon_0\frac{\partial\mathbf E}{\partial t},
        \\
        &=
        \mu_0 \left( \mathbf J_f + \nabla \times \mathbf M \right)
        +
        \mu_0 \left( \epsilon_0\frac{\partial\mathbf E}{\partial t} + \frac{\partial \mathbf P}{\partial t} \right),
    \end{aligned}
\]
or
\begin{equation}
    \nabla \times \mathbf H
    =
    \mathbf J_f + \frac{\partial\mathbf D}{\partial t},
\end{equation}
where, as in the static case,
\begin{equation}
    \mathbf H \equiv \frac{1}{\mu_0}\mathbf B - \mathbf M.
\end{equation}
Faraday's law (iii) and $\nabla \cdot \mathbf B = 0$ (ii) are not affected by the separation of charge and current.

In terms of \textit{free} charges and currents, then, Maxwell's equations read
\begin{equation}
    \boxed{%
        \begin{array}{cllcll}
            \mathrm{(i)} & \displaystyle \nabla \cdot \mathbf D = \rho_f,
                         &&
            \mathrm{(iii)} & \displaystyle \nabla \times \mathbf E = -\frac{\partial\mathbf B}{\partial t},
            \\\\
            \mathrm{(ii)} & \displaystyle \nabla \cdot \mathbf B = 0,
                         &&
            \mathrm{(iv)} & \displaystyle \nabla \times \mathbf H = \mathbf J_f + \frac{\partial\mathbf D}{\partial t}.
        \end{array}
    }
    \label{eq:ch07:MaxwellEqs:Matter}
\end{equation}
Note that Eq.~(\ref{eq:ch07:MaxwellEqs:Matter}) are in \textit{no way} more ``general'' than the Maxwell's equations we have learned.
All the information about bound charges and currents are contained in $\mathbf D$ and $\mathbf H$, which depend on the nature of the material; for linear media
\begin{equation}
    \mathbf P = \epsilon_0 \chi_e \mathbf E,
    \quad\text{and}\quad
    \mathbf M = \chi_m \mathbf H,
\end{equation}
so
\begin{equation}
    \mathbf D = \epsilon\mathbf E,
    \quad\text{and}\quad
    \mathbf H = \frac{1}{\mu}\mathbf B,
\end{equation}
where $\epsilon \equiv \epsilon_0(1 + \chi_e)$ and $\mu \equiv \mu_0(1 + \chi_m)$.
\\

Incidently, the \textbf{displacement current} is given by the electric ``displacement''
\begin{equation}
    \mathbf J_d \equiv \frac{\partial\mathbf D}{\partial t}.
\end{equation}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Boundary Conditions}
In general, the fields $\mathbf E$, $\mathbf B$, $\mathbf D$, and $\mathbf H$ will be discontinuous
\begin{itemize}
    \item at a boundary between two different media; or
    \item at a surface that carries a charge density $\sigma$ or a current density $\mathbf K$.
\end{itemize}
The explicit form of these discontinuities can be deduced from the integral form of Maxwell's equations (\ref{eq:ch07:MaxwellEqs:Matter})
\[
    \begin{aligned}
        & \left.
            \begin{array}{cl}
                \mathrm{(i)}
                &
                \displaystyle
                \oint_{\mathcal S} \mathbf D \cdot d\mathbf a = Q_{f_{\rm enc}}
                \\\\
                \mathrm{(ii)}
                &
                \displaystyle
                \oint_{\mathcal S} \mathbf B \cdot d\mathbf a = 0
            \end{array}
        \right\}
        \quad\text{over any closed surface $\mathcal S$}
        \\\\
        & \left.
            \begin{array}{cl}
                \mathrm{(iii)}
                &
                \displaystyle
                \oint_{\mathcal P} \mathbf E \cdot d\mathbf l = -\frac{d}{dt} \int_{\mathcal S} \mathbf B \cdot d\mathbf a
                \\\\
                \mathrm{(iv)}
                &
                \displaystyle
                \oint_{\mathcal P} \mathbf H \cdot d\mathbf l = I_{f_{\rm enc}} + \frac{d}{dt} \int_{\mathcal S} \mathbf D \cdot d\mathbf a
            \end{array}
        \right\}
        \quad
        \begin{array}{l}
            \text{for any surface $\mathcal S$} \\
            \text{bounded by the} \\
            \text{closed loop $\mathcal P$}
        \end{array}
    \end{aligned}
\]
where the positive direction for $\mathbf a$ is \textit{from ``below'' to ``above''}.
Applying the same trick, we get
\begin{equation}
    \boxed{%
        \begin{array}{cllcll}
            \mathrm{(i)} & \displaystyle D^{\perp}_{\rm above} - D^{\perp}_{\rm below} = \sigma_f,
                         &&
            \mathrm{(iii)} & \displaystyle \mathbf E^{\|}_{\rm above} - \mathbf E^{\|}_{\rm below} = \mathbf 0,
            \\\\
            \mathrm{(ii)} & \displaystyle B^{\perp}_{\rm above} - B^{\perp}_{\rm below} = 0,
                         &&
            \mathrm{(iv)} & \displaystyle \mathbf H^{\|}_{\rm above} - \mathbf H^{\|}_{\rm below} = \mathbf K_f \times \uvec n.
        \end{array}
    }
    \label{eq:ch07:BoundaryConditions:Matter}
\end{equation}
In the case of \textit{linear} media, they can be expressed in terms of $\mathbf E$ and $\mathbf B$ alone:
\begin{equation}
    \begin{array}{cllcll}
        \mathrm{(i)} & \displaystyle \epsilon_{\rm above}E^{\perp}_{\rm above} - \epsilon_{\rm below}E^{\perp}_{\rm below} = \sigma_f,
                     &&
        \mathrm{(iii)} & \displaystyle \mathbf E^{\|}_{\rm above} - \mathbf E^{\|}_{\rm below} = \mathbf 0,
        \\\\
        \mathrm{(ii)} & \displaystyle B^{\perp}_{\rm above} - B^{\perp}_{\rm below} = 0,
                      &&
        \mathrm{(iv)} & \displaystyle \frac{1}{\mu_{\rm above}}\mathbf B^{\|}_{\rm above} - \frac{1}{\mu_{\rm below}}\mathbf B^{\|}_{\rm below} = \mathbf K_f \times \uvec n.
    \end{array}
    \label{eq:ch07:BoundaryConditions:LinearMatter}
\end{equation}
If there is \textit{no} free charge or free current at the interface, then
\begin{equation}
    \begin{array}{cllcll}
        \mathrm{(i)} & \displaystyle \epsilon_{\rm above}E^{\perp}_{\rm above} - \epsilon_{\rm below}E^{\perp}_{\rm below} = 0,
                     &&
        \mathrm{(iii)} & \displaystyle \mathbf E^{\|}_{\rm above} - \mathbf E^{\|}_{\rm below} = \mathbf 0,
        \\\\
        \mathrm{(ii)} & \displaystyle B^{\perp}_{\rm above} - B^{\perp}_{\rm below} = 0,
                      &&
        \mathrm{(iv)} & \displaystyle \frac{1}{\mu_{\rm above}}\mathbf B^{\|}_{\rm above} - \frac{1}{\mu_{\rm below}}\mathbf B^{\|}_{\rm below} = \mathbf 0.
    \end{array}
    \label{eq:ch07:BoundaryConditions:NoFreeCharge}
\end{equation}
