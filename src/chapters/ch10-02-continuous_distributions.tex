\section{Continuous Distributions}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Retarded Potential}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch10-static_solution-01}
    \caption{Configuration.}%
\end{figure}
In static case, Eq.~(\ref{eq:ch10:PoissonEquation:4D}) reduces to four Poisson's equations
\[
    \nabla^2 V = -\frac{1}{\epsilon_0} \rho
    ,\quad
    \nabla^2\mathbf A = -\mu_0 \mathbf J
\]
with the familiar solutions
\begin{equation}
    V(\mathbf r)
    =
    \frac{1}{4\pi\epsilon_0} \int \frac{\rho(\mathbf r')}{\eta} d\tau'
    ,\quad
    \mathbf A(\mathbf r)
    =
    \frac{\mu_0}{4\pi} \int \frac{\mathbf J(\mathbf r')}{\eta} d\tau'
\end{equation}

In non-static case, we have to take into account the \textit{travel time} of the electromagnetic ``news''.
\begin{itemize}
    \item Travel time of source configuration charge change:
        \begin{equation}
            \Delta t = \frac{\eta}{c}
        \end{equation}
        This is the time it takes for electromagnetic news to get to the field point, $\mathbf r$.

    \item Therefore, we need to calculate potentials using sources at
        \begin{equation}
            t_r \equiv t - \Delta t
            = t - \frac{\eta}{c}
            \label{eq:ch10:RetardedTime}
        \end{equation}
        (called the \textbf{retarded time}), not at current time $t$.
\end{itemize}
Hence, the natural generation of our static solutions for \textit{nonstatic} sources is
\begin{equation}
    \boxed{%
        V(\mathbf r, t)
        =
        \frac{1}{4\pi\epsilon_0} \int \frac{\rho(\mathbf r', t_r)}{\eta} d\tau'
        ,\quad
        \mathbf A(\mathbf r, t)
        =
        \frac{\mu_0}{4\pi} \int \frac{\mathbf J(\mathbf r', t_r)}{\eta} d\tau'
    }
    \label{eq:ch10:RetardedPotentials}
\end{equation}
called \textbf{retarded potentials}.
Keep in mind that the retarded time $t_r$ is a function of $(t, \mathbf r, \mathbf r')$.

\framed{%
    \textbf Q. Are the retarded potentials the correct solution to the inhomogeneous wave equations?
}
\textbf{Proof.}
Obviously, all four components $(V, A_x, A_y, A_z)$ of Eq.~(\ref{eq:ch10:RetardedPotentials}) have the same structure, so we will prove Eq.~(\ref{eq:ch10:PoissonEquation:4D}) only for $V$.
\\

Taking the gradient of $V$ in Eq.~(\ref{eq:ch10:RetardedPotentials}),
\begin{equation}
    \nabla V
    =
    \frac{1}{4\pi\epsilon_0} \nabla \int \frac{\rho(\mathbf r', t_r)}{\eta} d\tau'
    =
    \frac{1}{4\pi\epsilon_0} \int
    \left( \underbrace{(\nabla \rho)}_{\color{blue}(1)}\frac{1}{\eta} + \rho \nabla \left( \frac{1}{\eta} \right) \right) d\tau'
\end{equation}
Note that $\nabla$ is applied to a function of $\mathbf r$ of which $t_r$ is also a function.
Evaluating (1),
\begin{equation}
        \nabla \rho(\mathbf r', t_r(\mathbf r))
        =
        \frac{\partial\rho}{\partial t_r} \nabla t_r
        =
        \frac{\partial \rho}{\partial t} \left( -\frac{\nabla\eta}{c} \right)
        =
        -\frac{1}{c} \dot\rho \uspvec
\end{equation}
where we made use of $\partial/\partial t_r = \partial/\partial t$ since $\mathbf r$ and $\mathbf r'$ are independent variables, and $\dot \rho \equiv \partial\rho/\partial t$.
Substituting this and making use of $\nabla \eta^{-1} = -\uspvec/\eta^2$, we get
\begin{equation}
    \nabla V
    =
    \frac{1}{4\pi\epsilon_0} \int
    \left( -\frac{\dot \rho}{c}\frac{\uspvec}{\eta} - \rho\frac{\uspvec}{\eta^2} \right)
    d\tau'
    \label{eq:ch10:GradientOfRetardedPotential}
\end{equation}
Taking the divergence on $\nabla V$,
\[
    \begin{aligned}
        \nabla^2 V
        &=
        \nabla \cdot \nabla V
        \\
        &=
        \frac{1}{4\pi\epsilon_0} \int -\frac{1}{c}
        \left( \frac{\uspvec}{\eta} \cdot \nabla \dot\rho + \dot\rho \nabla \cdot \left( \frac{\uspvec}{\eta} \right) \right)
        d\tau'
        \\
        &\quad
        -
        \frac{1}{4\pi\epsilon_0} \int
        \left( \frac{\uspvec}{\eta^2} \cdot \nabla \rho + \rho \nabla \cdot \left( \frac{\uspvec}{\eta^2} \right) \right)
        d\tau'
    \end{aligned}
\]
\begin{enumerate}
    \item [(i)]
        Since $\dot\rho = \dot\rho(\mathbf r', t_r)$, by the same reasoning,
        \[
            \nabla \dot\rho = -\frac{1}{c} \ddot\rho \uspvec
        \]

    \item [(ii)]
        \[
            \nabla \cdot \left( \frac{\uspvec}{\eta^2} \right) = 4\pi\delta^3(\spvec) = 4\pi\delta^3(\mathbf r - \mathbf r')
        \]

    \item [(iii)]
        \[
            \begin{aligned}
                \nabla \cdot \left( \frac{\uspvec}{\eta} \right)
                &=
                \nabla \cdot \left( \frac{\spvec}{\eta^2} \right)
                =
                \frac{1}{\eta^2} \underbrace{(\nabla \cdot \spvec)}_{\color{blue}=3}
                +
                \spvec \cdot \left( \nabla \frac{1}{\eta^2} \right)
                \\
                &=
                \frac{3}{\eta^2} + \spvec \cdot \left( -2 \frac{1}{\eta^3} \nabla \eta \right)
                =
                \frac{3}{\eta^2} - 2\spvec \cdot \frac{\uspvec}{\eta^3}
                \\
                &=
                \frac{1}{\eta^2}
            \end{aligned}
        \]
\end{enumerate}
Substituting them, it follows
\[
    \begin{aligned}
        \nabla^2 V
        &=
        \frac{1}{4\pi\epsilon_0} \int
        \left( \frac{1}{c^2} \frac{\ddot \rho}{\eta} - 4\pi\rho\left( \mathbf r', t - \frac{|\mathbf r - \mathbf r'|}{c} \right) \right) \delta^3(\mathbf r - \mathbf r')
        d\tau'
        \\
        &=
        \frac{1}{c^2} \frac{\partial^2}{\partial t^2}
        \underbrace{\left( \frac{1}{4\pi\epsilon_0} \int \frac{\rho(\mathbf r', t_r)}{\eta} d\tau' \right)}_{\color{blue}=V(\mathbf r, t)}
        -
        \frac{\rho(\mathbf r, t)}{\epsilon_0}
    \end{aligned}
\]

The same derivation applies to $A_x$, $A_y$, and $A_z$.

\framed{%
    \example{10.2}%
    An infinite straight wire carries the current
    \[
        I(t)
        =
        \left\{
            \begin{array}{rcl}
                0, & & \text{for}\, t \le 0
                \\
                I_0, & & \text{for}\, t > 0
            \end{array}
        \right.
    \]
    That is, a constant current $I_0$ is turned on abruptly at $t = 0$.
    Find the resulting electric and magnetic fields.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch10-example_2-01}
    \caption{Example 10.2 Infinite straight wire.}%
\end{figure}
\textbf{(1) Potentials:}\\
Since the wire is neutral,
\[
    \rho = 0 \quad\rightarrow\quad V = 0
\]
Meanwhile, the vector potential is
\[
    \mathbf A(s, t)
    =
    \frac{\mu_0}{4\pi} \int \frac{I(t_r)}{\eta} d\mathbf l
    =
    \uvec z \frac{\mu_0}{4\pi} \int_{-\infty}^\infty \frac{I(t_r)}{\sqrt{s^2 + z^2}} dz
\]
The retarded time is
\[
    t_r = t - \frac{\sqrt{s^2 + z^2}}{c}
\]
For $t \le s/c$, we have
\[
    \mathbf A = 0
    \qquad (t \le s/c)
\]
It is only when $t > s/c$ that the constant current $I_0$ starts to contribute to $\mathbf A$ at $P$.
The maximum extent $z_{\max}$ of the current segment that can contribute to $\mathbf A$ at the field point is
\[
    t_r = 0 = t - \frac{\sqrt{s^2 + z_{\max}^2}}{c}
    \quad\rightarrow\quad
    ct = \sqrt{s^2 + z_{\max}^2}
    \quad\rightarrow\quad
    z_{\max} = \pm\sqrt{{(ct)}^2 - s^2}
\]
Therefore, the potential is
\[
    \begin{aligned}
        \mathbf A
        &=
        \uvec z \frac{\mu_0 I_0}{4\pi} \cdot 2
        \int_0^{\sqrt{(ct)^2 + s^2}} \frac{1}{\sqrt{s^2 + z^2}} dz
        \\
        &=
        \uvec z \frac{\mu_0 I_0}{2\pi}
        \left. \log\left( \sqrt{s^2 + z^2} + z \right) \right|_0^{\sqrt{(ct)^2 - s^2}}
        \\
        &=
        \uvec z \frac{\mu_0 I_0}{2\pi}
        \log\left( \frac{ct + \sqrt{(ct)^2 - s^2}}{s} \right)
        \qquad(t \ge s/c)
    \end{aligned}
\]

\textbf{(2) Fields.}\\
\[
    \left\{
        \begin{aligned}
            \mathbf E(s, t)
            &=
            -\frac{\partial\mathbf A}{\partial t}
            =
            -\frac{\mu_0 I_0 c}{2\pi \sqrt{(ct)^2 - s^2}} \uvec z
            \\
            \mathbf B(s, t)
            &=
            \nabla \times \mathbf A
            =
            -\frac{\partial A_z}{\partial s} \uvec\phi
            =
            \frac{\mu_0 I_0}{2\pi s} \frac{ct}{\sqrt{(ct)^2 - s^2}} \uvec\phi
        \end{aligned}
    \right.
\]

As $t \rightarrow \infty$,
\[
    \mathbf E = 0
    \quad\text{and}\quad
    \mathbf B = \frac{\mu_0 I_0}{2\pi s} \uvec\phi
\]

\homework{10.11, 10.12}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Jefimenko's Equations}
Given the retarded potentials,
\begin{equation}
    V(\mathbf r, t)
    =
    \frac{1}{4\pi\epsilon_0} \int \frac{\rho(\mathbf r', t_r)}{\eta} d\tau'
    ,\quad
    \mathbf A(\mathbf r, t)
    =
    \frac{\mu_0}{4\pi} \int \frac{\mathbf J(\mathbf r', t_r)}{\eta} d\tau'
\end{equation}
we can now determine the fields:
\begin{equation}
    \mathbf E = -\nabla V - \frac{\partial\mathbf A}{\partial t}
    ,\quad
    \mathbf B = \nabla \times \mathbf A
\end{equation}

To calculate $\mathbf E$, we already know $\nabla V$ from Eq.~(\ref{eq:ch10:GradientOfRetardedPotential}):
\[
    \nabla V
    =
    \frac{1}{4\pi\epsilon_0} \int
    \left( -\frac{\dot \rho}{c}\frac{\uspvec}{\eta} - \rho\frac{\uspvec}{\eta^2} \right)
    d\tau'
\]
The time derivative of $\mathbf A$ is
\begin{equation}
    \frac{\partial\mathbf A}{\partial t}
    =
    \frac{\mu_0}{4\pi} \int \frac{\dot{\mathbf J}(\mathbf r', t_r)}{\eta} d\tau'
\end{equation}
Putting them together and using $\mu_0\epsilon_0 = 1/c^2$, we get
\begin{equation}
    \boxed{%
        \mathbf E(\mathbf r, t)
        =
        \frac{1}{4\pi\epsilon_0} \int \left(
            \frac{\rho(\mathbf r', t_r)}{\eta^2} \uspvec
            +
            \frac{\dot\rho(\mathbf r', t_r)}{c\eta} \uspvec
            -
            \frac{\dot{\mathbf J}(\mathbf r', t_r)}{c^2\eta}
        \right) d\tau'
    }
\end{equation}
This is the time-dependent generalization of Coulomb's law.
\\

As for $\mathbf B$, expand the curl of $\mathbf A$:
\[
    \begin{aligned}
        \nabla \times \mathbf A
        &=
        \frac{\mu_0}{4\pi} \nabla \times
        \int \frac{\mathbf J(\mathbf r', t_r)}{\eta} d\tau'
        \\
        &=
        \frac{\mu_0}{4\pi} \int \left(
            \frac{1}{\eta} (\nabla \times \mathbf J)
            -
            \mathbf J \times \nabla \left( \frac{1}{\eta} \right)
        \right) d\tau'
    \end{aligned}
\]
Similar to the derivation of $\nabla \rho$,
\[
    \nabla \times \mathbf J(\mathbf r', t_r)
    =
    -\frac{\nabla \eta}{c} \times \dot{\mathbf J}
    =
    \frac{1}{c} \dot{\mathbf J} \times \uspvec
\]
With this and making use of $\nabla\left( \frac{1}{\eta} \right) = -\frac{\uspvec}{\eta^2}$, we get
\begin{equation}
    \boxed{%
        \mathbf B(\mathbf r, t)
        =
        \frac{\mu_0}{4\pi} \int
        \left( \frac{\mathbf J(\mathbf r', t_r)}{\eta^2} + \frac{\dot{\mathbf J}(\mathbf r', t_r)}{c \eta} \right)
        \times \uspvec d\tau'
    }
\end{equation}
This is the time-dependent generalization of the Biot-Savart law.
\\

Expressions $\mathbf E(\mathbf r, t)$ and $\mathbf B(\mathbf r, t)$, known as \textbf{Jefimenko's equations}, are the causal solutions to Maxwell's equations.
This is obviously different from a na\"ive guess
\[
    \left\{
        \begin{aligned}
            \mathbf E &\ne \frac{1}{4\pi\epsilon_0} \int \frac{\rho(\mathbf r', t_r)}{\eta^2} \uspvec d\tau'
            \\
            \mathbf B &\ne \frac{\mu_0}{4\pi} \int \frac{\mathbf J(\mathbf r', t_r) \times \uspvec}{\eta^2} d\tau'
        \end{aligned}
    \right.
\]

\homework{10.13, 10.14}
