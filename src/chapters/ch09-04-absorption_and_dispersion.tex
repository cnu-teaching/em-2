\section{Absorption and Dispersion}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Electromagnetic Waves in Conductors}
EM wave propagation through vacuum and insulators (e.g., glass or pure water) (Sec. \ref{sec:ch09:03}):
\[
    \rho_f = 0,
    \quad
    \mathbf J_f = 0
\]
In case of conductors, \textit{free} charge exists, so
\[
    \mathbf J_f \ne 0
\]
According to Ohm's law,
\begin{equation}
    \mathbf J_f = \sigma \mathbf E
\end{equation}
Maxwell's equations for linear ($\epsilon = \epsilon_0(1 + \chi_e)$ and $\mu = \mu_0(1 + \chi_m)$), homogeneous ($\nabla\epsilon = 0$ and $\nabla \mu = 0$) media (Eq. \ref{eq:ch07:MaxwellEqs:Matter}):
\begin{equation}
    \begin{array}{cllcll}
        \mathrm{(i)} &
        \displaystyle \nabla \cdot \mathbf E = \frac{1}{\epsilon} \rho_f
                     &&
        \mathrm{(iii)} &
        \displaystyle \nabla \times \mathbf E = -\frac{\partial\mathbf B}{\partial t}
        \\\\
        \mathrm{(ii)} &
        \displaystyle \nabla \cdot \mathbf B = 0
                      &&
        \mathrm{(iv)} &
        \displaystyle \nabla \times \mathbf B = \mu\sigma \mathbf E + \mu\epsilon \frac{\partial\mathbf E}{\partial t}
    \end{array}
    %\label{eq:ch09:MaxwellEqs:LinearMedia}
\end{equation}
where $\sigma$ is the conductivity.
\\

Continuity equation for free charge reads
\begin{equation}
    \nabla \cdot \mathbf J_f = -\frac{\partial\rho_f}{\partial t}
\end{equation}
Applying Ohm's law and Gauss's law (i),
\begin{equation}
    \frac{\partial\rho_f}{\partial t}
    =
    -\sigma (\nabla \cdot \mathbf E)
    =
    -\frac{\sigma}{\epsilon} \rho_f
\end{equation}
whose solution is
\begin{equation}
    \rho_f(t)
    =
    e^{-(\sigma/\epsilon)t}
    \rho_f(0)
\end{equation}
Thus, any initial free charge $\rho_f(0)$ dissipates in a characteristic time $\tau \equiv \epsilon/\sigma$.

Recall that if you put some free charge on a (perfect) conductor, it will flow out to the edges.
\framed{%
    $\therefore$ The time constant $\tau$ is a measure of how good a conductor is:
    \begin{itemize}
        \item Perfect conductor: $\sigma = \infty\ \rightarrow\ \tau = 0$ (instant dissipation of free charge)

        \item ``good'' conductor: $\tau \ll T$ ($T$ is other relevant time scales in the system)

        \item ``poor'' conductor: $\tau \gg T$ (very slow dissipation of free charge)
    \end{itemize}
}

\separator%

After $t \gg \tau$, $\rho_f \approx 0$.
Then, we have
\begin{equation}
    \begin{array}{cllcll}
        \mathrm{(i)} &
        \displaystyle \nabla \cdot \mathbf E = 0
                     &&
        \mathrm{(iii)} &
        \displaystyle \nabla \times \mathbf E = -\frac{\partial\mathbf B}{\partial t}
        \\\\
        \mathrm{(ii)} &
        \displaystyle \nabla \cdot \mathbf B = 0
                      &&
        \mathrm{(iv)} &
        \displaystyle \nabla \times \mathbf B = \mu\epsilon \frac{\partial\mathbf E}{\partial t}
        +
        {\color{blue}\mu\sigma \mathbf E}
    \end{array}
    \label{eq:ch09:MaxwellEqs:LinearConductor}
\end{equation}
The last term in (iv) is added from the equations of \textit{non}conducting media (Eq. \ref{eq:ch09:MaxwellEqs:LinearInsulator}).
\\

Applying the curl to (iii) and (iv), we get modified wave equations for $\mathbf E$ and $\mathbf B$:
\begin{equation}
    \nabla^2 \mathbf E
    =
    \mu\epsilon \frac{\partial^2 \mathbf E}{\partial t^2}
    +
    \mu\sigma \frac{\partial \mathbf E}{\partial t}
    ,\quad
    \nabla^2 \mathbf B
    =
    \mu\epsilon \frac{\partial^2 \mathbf B}{\partial t^2}
    +
    \mu\sigma \frac{\partial \mathbf B}{\partial t}
    \label{eq:ch09:WaveEqs:LinearConductor}
\end{equation}
These equations admit plane-wave solutions (assuming propagation in the $z$ direction):
\begin{equation}
    \tilde{\mathbf E}(z, t)
    =
    \tilde{\mathbf E}_{0}
    e^{i(\tilde k z - \omega t)}
    ,\quad
    \tilde{\mathbf B}(z, t)
    =
    \tilde{\mathbf B}_{0}
    e^{i(\tilde k z - \omega t)}
    \label{eq:ch09:PlaneWave:LinearConductor}
\end{equation}
Substituting them into the wave Eqs. \ref{eq:ch09:WaveEqs:LinearConductor}, we obtain
\begin{equation}
    \tilde k^2
    =
    \mu\epsilon \omega^2
    +
    i \mu\sigma \omega
\end{equation}
Therefore, the ``wave number'' $\tilde k$ becomes complex.

Let $\tilde k = k + i \kappa$. Then,
\begin{equation}
    k
    =
    \omega \sqrt{\frac{\epsilon\mu}{2}}
    \left[
        \sqrt{1 + \left( \frac{\sigma}{\epsilon \omega} \right)^2}
        +
        1
    \right]^{1/2}
    ,\quad
    \kappa
    =
    \omega \sqrt{\frac{\epsilon\mu}{2}}
    \left[
        \sqrt{1 + \left( \frac{\sigma}{\epsilon \omega} \right)^2}
        -
        1
    \right]^{1/2}
\end{equation}
Put $\tilde k$ into Eq. \ref{eq:ch09:PlaneWave:LinearConductor}, we obtain
\begin{equation}
    \tilde{\mathbf E}(z, t)
    =
    \underbrace{\color{blue}\tilde{\mathbf E}_0 e^{-\kappa z}}_{(\dagger)}
    \cdot
    e^{i(kz - \omega t)}
    ,\quad
    \tilde{\mathbf B}(z, t)
    =
    \underbrace{\color{blue}\tilde{\mathbf B}_0 e^{-\kappa z}}_{(\dagger)}
    \cdot
    e^{i(kz - \omega t)}
\end{equation}
Therefore, the imaginary part of $\tilde k$ results in an attenuation of the wave (i.e., decreasing amplitude $(\dagger)$ with increasing $z$).
\\

\textbf{Skin depth} is defined as
the distance it takes to reduce the amplitude by a factor of $e^{-1}\,(\approx 1/3)$:
\begin{equation}
    d \equiv \frac{1}{\kappa}
\end{equation}
It is a measure of how far the wave penetrates into the conductor.

Meanwhile, from the real part of $\tilde k$, we determine
\begin{equation}
    \lambda = \frac{2\pi}{k}
    ,\quad
    v = \frac{\omega}{k}
    ,\quad
    n = \frac{c k}{\omega}
\end{equation}

Maxwell's equations (i) and (ii) state that the fields are transverse:
\[
    \tilde E_z = 0
    ,\quad
    \tilde B_z = 0
\]
So, choosing $\uvec x$ along the polarization of $\mathbf E$,
\begin{equation}
    \tilde{\mathbf E}(z, t)
    =
    \tilde E_0 e^{-\kappa z}
    e^{i(kz - \omega t)} \uvec x
    \label{eq:ch09:WaveEq:Ezt}
\end{equation}
Then, Faraday's law gives
\begin{equation}
    \tilde{\mathbf B}(z, t)
    =
    \underbrace{\frac{\tilde k}{\omega} \tilde E_0}_{\color{blue}\tilde{\mathbf B}_0}
    e^{-\kappa z}
    e^{i(kz - \omega t)} \uvec y
    \label{eq:ch09:WaveEq:Bzt}
\end{equation}
As is the case for any complex number, $\tilde k$ can be expressed in terms of its modulus $K$ and phase $\phi$:
\begin{equation}
    \tilde k
    =
    K e^{i \phi}
\end{equation}
where
\begin{equation}
    K = |\tilde k|
    = \sqrt{k^2 + \kappa^2}
    =
    \omega \sqrt{\epsilon\mu \sqrt{1 + \left( \frac{\sigma}{\epsilon \omega} \right)^2}}
\end{equation}
and
\begin{equation}
    \phi = \tan^{-1}\left( \frac{\kappa}{k} \right)
\end{equation}

Likewise, the complex amplitudes are
\begin{equation}
    \tilde E_0
    =
    E_0 e^{i\delta_E}
    ,\quad
    \tilde B_0
    =
    B_0 e^{i\delta_B}
\end{equation}
and since $\tilde{\mathbf B}_0 = (\tilde k/\omega) \tilde{\mathbf E}_0$ (Eq. \ref{eq:ch09:WaveEq:Bzt}),
\begin{equation}
    B_0 e^{i\delta_B}
    =
    \frac{K e^{i\phi}}{\omega}
    E_0 e^{i\delta_E}
    \quad\rightarrow\quad
    \left( B_0 \right)
    \left( e^{i\delta_B} \right)
    =
    \left( \frac{K}{\omega} E_0 \right)
    \left( e^{i(\delta_E + \phi)} \right)
\end{equation}
Evidently, $\mathbf E$ and $\mathbf B$ are no longer in phase:
\begin{equation}
    \delta_B - \delta_E = \phi
\end{equation}
It says that $\mathbf B$ lags behind $\mathbf E$ by this amount.

The (real) amplitudes of $\mathbf E$ and $\mathbf B$ are related by
\begin{equation}
    \frac{B_0}{E_0}
    =
    \frac{K}{\omega}
    =
    \sqrt{\epsilon \mu \sqrt{1 + \left( \frac{\sigma}{\epsilon \omega} \right)^2}}
\end{equation}

\begin{figure}[ht]
    \centering
    \includegraphics[width=20pc]{assets/ch09-wave_field_inside_conductor-01}
    \caption{Wave fields inside a conductor.}%
    \label{fig:ch09:WaveFieldInConductor}
\end{figure}
Collecting the results together and taking the real part,
\begin{equation}
    \begin{aligned}
        \mathbf E(z, t)
        &=
        E_0 e^{-\kappa z}
        \cos(kz - \omega t + \delta_E)
        \uvec x
        \\
        \mathbf B(z, t)
        &=
        \frac{K}{\omega} E_0 e^{-\kappa z}
        \cos(kz - \omega t + \delta_E + \phi) \uvec y
    \end{aligned}
\end{equation}
The wave fields are shown in Fig. \ref{fig:ch09:WaveFieldInConductor}.

\homework{9.19, 9.20, 9.21}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Reflection at a Conducting Surface}
Unlike the nonconducting media in Sec. \ref{sec:ch09:03},
now we have to use the more general boundary conditions (Eq. \ref{eq:ch09:BoundaryConditions:LinearMatter}):
\begin{equation}
    \begin{array}{cllcll}
        \mathrm{(i)} & \displaystyle \epsilon_1 E^{\perp}_1 - \epsilon_2 E^{\perp}_2 = \sigma_f
                     &&
        \mathrm{(iii)} & \displaystyle \mathbf E^{\|}_1 - \mathbf E^{\|}_2 = \mathbf 0
        \\\\
        \mathrm{(ii)} & \displaystyle B^{\perp}_1 - B^{\perp}_2 = 0
                      &&
        \mathrm{(iv)} & \displaystyle \frac{1}{\mu_1}\mathbf B^{\|}_1 - \frac{1}{\mu_2}\mathbf B^{\|}_2
        =
        \underbrace{\color{blue}\mathbf K_f}_{= 0} \times \uvec n
    \end{array}
    \label{eq:ch09:BoundaryConditions:LinearMatter}
\end{equation}
\[
    \left\{
        \begin{aligned}
            \sigma_f:\ &\text{free surface charge}
            \\
            \mathbf K_f:\ &\text{free surface current}
            \\
            \uvec n:\ &\text{normal to the surface, pointing}
            \\
                      &\text{from medium (2) into medium (1)}
        \end{aligned}
    \right.
\]
For ohmic conductors ($\mathbf J_f = \sigma \mathbf E$) there can be no free surface current,
since this would require an infinite electric field at the boundary.

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch09-reflection_transmission-01}
    \caption{Normal incidence of EM waves into a conductor.}%
    \label{fig:ch09:NormalIncidence:Conductor}
\end{figure}
Suppose the $xy$ plane forms the boundary between a nonconducting medium (1) and a conductor (2).
A monochromatic plane wave, traveling the in $z$ direction and polarized in the $x$ direction, as in Fig. \ref{fig:ch09:NormalIncidence:Conductor}.
\begin{itemize}
    \item[] Incident wave:
        \begin{equation}
            \tilde{\mathbf E}_{I}(z, t)
            =
            \tilde E_{0_I}
            e^{i(k_1 z - \omega t)}
            \uvec x
            ,\quad
            \tilde{\mathbf B}_{I}(z, t)
            =
            \frac{1}{v_1}
            \tilde E_{0_I}
            e^{i(k_1 z - \omega t)}
            \uvec y
        \end{equation}

    \item[] Reflected wave:
        \begin{equation}
            \tilde{\mathbf E}_{R}(z, t)
            =
            \tilde E_{0_R}
            e^{i(-k_1 z - \omega t)}
            \uvec x
            ,\quad
            \tilde{\mathbf B}_{R}(z, t)
            =
            -
            \frac{1}{v_1}
            \tilde E_{0_R}
            e^{i(-k_1 z - \omega t)}
            \uvec y
        \end{equation}

    \item[] Transmitted wave:
        \begin{equation}
            \tilde{\mathbf E}_{T}(z, t)
            =
            \tilde E_{0_T}
            e^{i(\tilde k_2 z - \omega t)}
            \uvec x
            ,\quad
            \tilde{\mathbf B}_{T}(z, t)
            =
            \frac{\tilde k_2}{\omega}
            \tilde E_{0_T}
            e^{i(\tilde k_2 z - \omega t)}
            \uvec y
        \end{equation}
        which is attenuated as it penetrates into the conductor.
\end{itemize}

At $z = 0$, $\tilde{\mathbf E}_{I} + \tilde{\mathbf E}_{R}$ must join $\tilde{\mathbf E}_T$,
according to the boundary conditions (Eq. \ref{eq:ch09:BoundaryConditions:LinearMatter}).
\begin{enumerate}
    \item[(1)] $E^{\perp} = 0$ on both sides $\rightarrow$ $\sigma_f = 0$ by (i)

    \item[(2)] $B^{\perp} = 0$ on both sides $\rightarrow$ automatically satisfies (i)

    \item[(3)] (iii) gives
        \begin{equation}
            \tilde E_{0_I} + \tilde E_{0_R} = \tilde E_{0_T}
        \end{equation}

    \item[(4)] (iv) with $\mathbf K_f = 0$ gives
        \begin{equation}
            \frac{1}{\mu_1 v_1}
            (\tilde E_{0_I} - \tilde E_{0_R})
            -
            \frac{\tilde k_2}{\mu_2 \omega}
            \tilde E_{0_T}
            =
            0
            \quad\rightarrow\quad
            \tilde E_{0_I}
            -
            \tilde E_{0_I}
            =
            \tilde\beta \tilde E_{0_T}
        \end{equation}
        where
        \begin{equation}
            \tilde\beta
            =
            \frac{\mu_1 v_1}{\mu_2 \omega} \tilde k_2
            \qquad\text{(complex number)}
        \end{equation}
\end{enumerate}
Solving for $\tilde E_{0_R}$ and $\tilde E_{0_T}$, it follows
\begin{equation}
    \tilde E_{0_R}
    =
    \left( \frac{1 - \tilde\beta}{1 + \tilde\beta} \right)
    \tilde E_{0_I}
    ,\quad
    \tilde E_{0_T}
    =
    \left( \frac{2}{1 + \tilde\beta} \right) \tilde E_{0_I}
\end{equation}

For a perfect conductor ($\sigma = 0$), $k_2 = \kappa_2 = \infty$, so $\tilde\beta \rightarrow \infty$.
So,
\begin{equation}
    \tilde E_{0_R} = -\tilde E_{0_I}
    ,\quad
    \tilde E_{0_T} = 0
\end{equation}
This is total reflection with a $180^\circ$ phase shift, e.g., mirror (silver coating).

\homework{9.22}
