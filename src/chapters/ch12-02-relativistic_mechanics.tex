\section{Relativistic Mechanics}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Proper Time and Proper Velocity}
\textbf{Proper time}:
The time, $\tau$, associated with a moving object.
\\
By time dilation, the interval of proper time, $d\tau$, is related to the interval of coordinate time, $dt$:
\begin{equation}
    d\tau
    =
    \sqrt{1 - u^2/c^2}
    dt
\end{equation}
Proper time is \textit{invariant}, whereas ``ordinary'' time depends on the particular reference frame.
\\

By velocity, we mean the displacement divided by the ``ordinary'' time
\begin{equation}
    \mathbf u
    =
    \frac{d\mathbf l}{dt}
\end{equation}
which we call the \textbf{ordinary velocity}.
\\
We may be more interested in the distance covered per unit proper time
\begin{equation}
    \vec\eta
    \equiv
    \frac{d\mathbf l}{d\tau}
\end{equation}
which we call \textbf{proper velocity}.
\\
The two are related
\begin{equation}
    \vec\eta
    =
    \frac{d\mathbf l}{d\tau}
    =
    \frac{1}{\sqrt{1 - u^2/c^2}}
    \frac{d\mathbf l}{dt}
    =
    \gamma \mathbf u
\end{equation}
The proper velocity $\vec\eta$ is the spatial part of a 4-vector
\begin{equation}
    \eta^\mu
    =
    \frac{dx^\mu}{d\tau}
\end{equation}
whose zeroth component is
\begin{equation}
    \eta^0
    =
    \frac{dx^0}{d\tau}
    =
    c\frac{dt}{d\tau}
    =
    \gamma c
\end{equation}
Advantage of proper velocity is that it transforms simply between inertial systems:
\begin{equation}
    \begin{aligned}
        \bar\eta^0
        &=
        \gamma (\eta^0 - \beta \eta^1)
        \\
        \bar\eta^1
        &=
        \gamma (\eta^1 - \beta \eta^0)
        \\
        \bar\eta^2
        &=
        \eta^2
        \\
        \bar\eta^3
        &=
        \eta^3
    \end{aligned}
\end{equation}
Or more generally,
\begin{equation}
    \bar\eta^\mu
    =
    {\Lambda^\mu}_\nu
    \eta^\nu
\end{equation}
$\eta^\mu$ is called \textbf{proper velocity 4-vector} or simply \textbf{4-velocity}.

\homework{12.25, 12.26}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Relativistic Energy and Momentum}
\textbf{Relativistic momentum}:
\begin{equation}
    \mathbf p
    \equiv
    m\vec\eta
    =
    m\gamma \mathbf u
    =
    \frac{m\mathbf u}{\sqrt{1 - u^2/c^2}}
\end{equation}
which is the spatial part of a 4-vector:
\begin{equation}
    p^\mu
    \equiv
    m \eta^\mu
\end{equation}
whose temporal component is
\begin{equation}
    p^0
    =
    m\eta^0
    =
    m\gamma c
    =
    \frac{mc}{\sqrt{1 - u^2/c^2}}
\end{equation}
Einstein identifies $p^0 c$ as relativistic energy:
\begin{equation}
    E
    \equiv
    m\gamma c^2
    =
    \frac{mc^2}{\sqrt{1 - u^2/c^2}}
\end{equation}
$p^\mu$ is called the \textbf{energy-momentum 4-vector} or \textbf{momentum 4-vector}.
\\

Relativistic energy is nonzero even when the object is stationary;
we call this the \textbf{rest energy}:
\begin{equation}
    E_{\rm rest}
    \equiv
    mc^2
\end{equation}
The kinetic energy is then the difference:
\begin{equation}
    E_{\rm kin}
    \equiv
    E - mc^2
    =
    mc^2 (\gamma - 1)
\end{equation}
In the nonrelativistic regime $(u \ll c)$,
\begin{equation}
    E_{\rm kin}
    =
    \frac{1}{2} mu^2
    +
    \frac{3}{8}
    \frac{mu^4}{c^2}
    +
    \cdots
\end{equation}

\framed{%
    In every closed system, the total relativistic energy and momentum are conserved.
}
\bigskip
Distinction between \textbf{invariant} and \textbf{conserved} quantities:
\[
    \left\{
        \begin{array}{rl}
            \mathbf{invariant}:
            &
            \text{Same value in all inertial systems.}
            \\\\
            \mathbf{conserved}:
            &
            \text{Same value before and after some process.}
        \end{array}
    \right.
\]
\begin{itemize}
    \item \textbf{Mass} is invariant but not conserved.

    \item \textbf{Energy} is conserved but not invariant.

    \item \textbf{Electric charge} is both conserved and invariant.
\end{itemize}
\bigskip

The scalar product of $p^\mu$ with itself is
\begin{equation}
    p^\mu p_\mu
    =
    -(p^0)^2
    +\underbrace{(\mathbf p \cdot \mathbf p)}_{\color{blue} p^2}
    =
    -m^2 c^2
\end{equation}
In terms of relativistic energy and momentum,
\begin{equation}
    \boxed{%
        E^2 - p^2 c^2
        =
        m^2 c^4
    }
\end{equation}
A massless particle can carry energy and momentum, provided it always travels at the speed of light
\begin{equation}
    E = pc
\end{equation}

\homework{12.30}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Relativistic Dynamics}
Newton's 2nd law:
\begin{equation}
    \mathbf F
    =
    \frac{d\mathbf p}{dt}
\end{equation}

\framed{%
    \example{12.10}%
    \textbf{Motion under a constant force}.
    A particle $m$ is subject to a constant force $F$.
    If it starts from rest at the origin, at time $t = 0$,
    find its position, $x$, as a function of time.
}
\solution%
\begin{equation}
    \frac{dp}{dt}
    =
    F
    \quad\rightarrow\quad
    p = Ft + \mathrm{const.}
\end{equation}
Since it starts from rest $(p = 0)$,
\begin{equation}
    p
    =
    \frac{mu}{\sqrt{1 - u^2/c^2}}
    =
    Ft
\end{equation}
Solving for $u$,
\begin{equation}
    u
    =
    \frac{(F/m) t}{\sqrt{1 + (Ft/mc)^2}}
\end{equation}
If $(F/m)t \ll c$, we get the classical answer
\begin{equation}
    u \approx \frac{F}{m} t
\end{equation}
Integrating it to get position,
\begin{equation}
    \begin{aligned}
        x(t)
        &=
        \frac{F}{m}
        \int_0^t \frac{t'}{\sqrt{1 + (Ft'/mc)^2}}
        dt'
        \\
        &=
        \frac{mc^2}{F}
        \left. \sqrt{1 + (Ft'/mc)^2} \right|_{0}^{t}
        \\
        &=
        \frac{mc^2}{F}
        \left( \sqrt{1 + (Ft/mc)^2} - 1 \right)
    \end{aligned}
\end{equation}
or
\begin{equation}
    \left( 1 + \frac{F}{mc^2} x \right)^2
    -
    \left( \frac{F}{mc^2} (ct) \right)^2
    =
    1
\end{equation}
The graph is a hyperbola whose focus is shifted in the $x$ axis by $-mc^2/F$.

\separator%

Work is still the line integral of the force
\begin{equation}
    W
    \equiv
    \int \mathbf F \cdot d\mathbf l
\end{equation}
\textbf{Work-energy theorem} (the net work on a particle equals the increase in kinetic energy):
\begin{equation}
    \begin{aligned}
        W
        &=
        \int \frac{d\mathbf p}{dt} \cdot d\mathbf l
        =
        \int \frac{d\mathbf p}{dt} \cdot \frac{d\mathbf l}{d\tau} d\tau
        \\
        &\qquad\qquad\qquad\leftarrow\quad {\color{blue}dt = \gamma d\tau}
        \\
        &=
        \int \frac{1}{\gamma} \frac{d\mathbf p}{d\tau} \cdot \vec\eta d\tau
        \\
        &\qquad\qquad\qquad\leftarrow\quad {\color{blue}\gamma = \sqrt{1 + \eta^2/c^2}}
        \\
        &=
        m \int \frac{1}{\sqrt{1 + \eta^2/c^2}} \frac{d\vec\eta}{d\tau} \cdot \vec\eta d\tau
        \\
        &=
        m \int \frac{1}{\sqrt{1 + \eta^2/c^2}} d\left(\frac{\eta^2}{2}\right)
        \\
        &=
        mc^2 \int \frac{1}{2} \frac{1}{\sqrt{1 + \eta^2/c^2}} d\left(1 + \eta^2/c^2\right)
        \\
        &=
        mc^2 \int \frac{1}{2\gamma} d(\gamma^2)
        \\
        &=
        mc^2 \int d\gamma
        \\
        &=
        E_{\rm final} - E_{\rm initial}
    \end{aligned}
\end{equation}

Like ordinary velocity,
because the derivative of momentum with respect to ``ordinary'' time,
its transformations become ugly.
However, if the particle is (instantaneously) at rest in $\mathcal S$,
so that $\mathbf u = 0$, then,
\begin{equation}
    \bar{\mathbf F}_\perp
    =
    \frac{1}{\gamma}
    \mathbf F_\perp
    ,\quad
    \bar F_\|
    =
    F_\|
    \label{eq:ch12:ForceTransformWithParticleAtRest}
\end{equation}
where $\|$ and $\perp$ are with respect to the motion of $\bar{\mathcal S}$.
\\

To avoid this, one can introduce ``proper'' force, called the \textbf{Minkowski force}
\begin{equation}
    K^\mu
    =
    \frac{dp^\mu}{d\tau}
\end{equation}
The spatial components of $K^\mu$ are related to the ``ordinary'' force by
\begin{equation}
    \mathbf K
    =
    \left( \frac{dt}{d\tau} \right)
    \frac{d\mathbf p}{dt}
    =
    \frac{1}{\sqrt{1 - u^2/c^2}}
    \mathbf F
    =
    \gamma \mathbf F
\end{equation}
while the zeroth component reads
\begin{equation}
    K^0
    =
    \frac{dp^0}{d\tau}
    =
    \frac{1}{c}
    \frac{dE}{d\tau}
\end{equation}
is the (proper) rate at which the energy of the particle increases---in other words, the (proper) power delivered to the particle.
\\

The Lorentz force can be written
\begin{equation}
    \mathbf F
    =
    q (\mathbf E + \mathbf u \times \mathbf B)
\end{equation}
or
\begin{equation}
    \mathbf K
    =
    q(\gamma \mathbf E + \vec\eta \times \mathbf B)
\end{equation}

\framed{%
    \example{12.11}%
    \textbf{Cyclotron motion}.
}
\solution%
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-cyclotron_motion-01}
    \caption{Cyclotron motion.}%
    \label{fig:ch12:CyclotronMotion}
\end{figure}
Lorentz force:
\[
    F = Q u B
\]
Newton's 2nd law:
\[
    F
    =
    \frac{dp}{dt}
    =
    p\frac{d\theta}{dt}
    =
    p \frac{u}{R}
\]
Thus,
\[
    QuB = p \frac{u}{R}
    \quad\rightarrow\quad
    p = QBR
\]

\separator%

In classical mechanics, the total momentum of interacting particles can be expressed as the total mass, $M$, times the velocity of the center of the mass
\begin{equation}
    \mathbf P
    =
    M \frac{d\mathbf R_m}{dt}
\end{equation}
where $\mathbf R_m = \frac{1}{M} \sum m_i \mathbf r_i$.
\\
In relativity, $\mathbf R_m$ is replaced by the \textbf{center-of-energy},
$\mathbf R_e = \frac{1}{E} \sum E_i \mathbf r_i$, and $M$ by $E/c^2$
\begin{equation}
    \mathbf P
    =
    \frac{E}{c^2}
    \frac{d\mathbf R_e}{dt}
\end{equation}
$\mathbf P$ now includes all forms of momentum, and $E$ all forms of energy.

\homework{12.39}
