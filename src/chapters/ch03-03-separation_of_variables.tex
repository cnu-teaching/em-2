\section{Separation of Variables}
In this section, we will directly solve Laplace's equation using the method of \textbf{separation of variables}.\\

This is applicable in circumstances where the potential ($V$), or the charge density ($\sigma$), is specified on the boundaries of some region.\\

\textbullet{} Basic strategy: Look for solutions that are products of functions each of which depends on only one of the coordinates.
For example,
\[
    \begin{array}{rclll}
        V(\mathbf r)
        &=&
        X(x) Y(y) Z(z)
        && \mathrm{Cartesian}
        \\
        &=&
        R(r) \Theta(\theta) \Phi(\phi)
        && \mathrm{Spherical}
        \\
        &=&
        S(s) \Phi(\phi) Z(z)
        && \mathrm{Cylindrical}
    \end{array}
\]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Cartesian Coordinates}

\framed{%
    \example{3.3}%
    Two infinite grounded metal plates lie parallel to the $xz$ plane.
    The left end, at $x = 0$, is closed off and maintained at specified potential $V_0(y)$.
    Find the potential inside.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch03-example_3-01}
    \caption{Example 3.3 Configuration.}%
    \label{fig:ch03:Example3}
\end{figure}
Since the configuration is independent of $z$, this is essentially a two-dimensional problem
\[
    \frac{\partial^2 V}{\partial x^2}
    +
    \frac{\partial^2 V}{\partial y^2}
    =
    0
\]
subject to the boundary conditions
\begin{enumerate}
    \item [(i)] $V = 0$ when $y = 0$
    \item [(ii)] $V = 0$ when $y = a$
    \item [(iii)] $V = V_0(y)$ when $x = 0$
    \item [(iv)] $V \rightarrow 0$ as $x \rightarrow \infty$
        \\
\end{enumerate}

The first step is to look for solutions of the form
\[
    V(x, y) = X(x) Y(y)
\]
Putting it into Laplace's equation and dividing it by $XY$:
\[
    Y\frac{d^2 X}{dx^2} + X\frac{d^2 Y}{dy^2} = 0
    \quad \rightarrow \quad
    \frac{1}{X} \frac{d^2 X}{dx^2}
    +
    \frac{1}{Y} \frac{d^2 Y}{dy^2}
    =
    0
\]
Here, we have an equation of the form $f(x) + g(y) = 0$ for all values of $x$ and $y$.
The only way this could be true is $f(x) = -g(x) = const$.

Let the constant be $k^2$, that is,
\[
    \frac{1}{X} \frac{d^2 X}{dx^2}
    =
    -\frac{1}{Y} \frac{d^2 Y}{dy^2}
    =
    k^2
\]
or
\begin{equation}
    \frac{d^2 X}{dx^2} = k^2 X
    \quad \text{and} \quad
    \frac{d^2 Y}{dy^2} = -k^2 Y
\end{equation}
By this process, we transformed \textit{one} partial differential equation of two variables into \text{two} ordinary equations.

The solutions are
\begin{equation}
    X(x) = A e^{kx} + B e^{-kx}
    \quad \text{and} \quad
    Y(y) = C \sin ky + D \cos ky
\end{equation}
The general form of our potential is then
\begin{equation}
    V(x, y)
    =
    X(x) Y(y)
    =
    (A e^{kx} + B e^{-kx})
    (C \sin ky + D \cos ky)
\end{equation}\\

Having solved Laplace's equation, the next step is to determine the constants by imposing the boundary conditions.
\begin{itemize}
    \item Condition (iv): \\
        We must have $A = 0$ so that the potential does not diverge at $x \rightarrow \infty$.
        Absorbing $B$ into $C$ and $D$, the potential reduces to
        \begin{equation}
            V(x, y) = e^{-kx} (C \sin ky + D \cos ky)
        \end{equation}

    \item Condition (i) demands $D = 0$, so
        \begin{equation}
            V(x, y) = C e^{-kx} \sin ky
        \end{equation}

    \item Condition (ii) yields
        \[
            \sin ka = 0
        \]
        from which it follows
        \begin{equation}
            k = \frac{n\pi}{a}
            ,\qquad
            n = 1, 2, 3, \dots
        \end{equation}
        So,
        \begin{equation}
            V_n(x, y) = C_n e^{-n\pi x/a} \sin\left( \frac{n\pi y}{a} \right)
        \end{equation}
\end{itemize}
This indicates that we have an infinite family of solution, one for each $n$.
Since Laplace's equation is linear, the most \textit{general solution} is the linear combination of them
\begin{equation}
    V(x, y) = \sum_{n = 1}^{\infty} C_n e^{-n\pi x/a} \sin\left( \frac{n\pi y}{a} \right)
    \label{eq:ch03:Example3:GeneralPotential}
\end{equation}
From condition (iii), we find the remaining constant
\begin{equation}
    V(0, y) = \sum_{n = 1}^\infty C_n \sin\left( \frac{n\pi y}{a} \right) = V_0(y)
\end{equation}
This is a Fourier sine series.
Recall that Dirichlet's theorem guarantees that any function (in this case, $V_0(y)$) can be expanded in such a series.

Using the identity
\[
    \int_0^a \sin\left( \frac{n\pi y}{a} \right) \sin\left( \frac{n' \pi y}{a} \right) dy
    =
    \left\{
        \begin{array}{lll}
            0 & & \mathrm{if}\ n \ne m \\
            \\
            \displaystyle \frac{a}{2} & & \mathrm{if}\ n = m \\
        \end{array}
    \right.
\]
it follows that
\[
    \sum_{n = 1}^\infty
    C_n \int_0^a \sin\left( \frac{n\pi y}{a} \right) \sin\left( \frac{n'\pi y}{a} \right) dy
    =
    \int_0^a V_0(y) \sin\left( \frac{n'\pi y}{a} \right) dy
\]
Hence,
\begin{equation}
    C_n = \frac{2}{a} \int_0^a V_0(y) \sin\left( \frac{n\pi y}{a} \right) dy
\end{equation}
This is as far as we can get without knowing $V_0(y)$.\\

As an example, if $V_0(y) = V_0$ (constant), then
\[
    C_n
    =
    \frac{2V_0}{a} \int_0^a \sin\left( \frac{n\pi y}{a} \right) dy
    =
    \frac{2 V_0}{n\pi} \left( 1 - \cos n\pi \right)
    =
    \left\{
        \begin{array}{lll}
            0 & & n \in\,\mathrm{even} \\
            \\
            \displaystyle \frac{4 V_0}{n\pi} & & n \in\,\mathrm{odd}
        \end{array}
    \right.
\]
Putting it into Eq.\ (\ref{eq:ch03:Example3:GeneralPotential}), it follows
\begin{equation}
    \begin{aligned}
        V(x, y)
        &=
        \frac{4 V_0}{\pi} \sum_{n = 1, 3, 5, \dots}
        \frac{1}{n} e^{-n\pi x/a} \sin\left( \frac{n\pi y}{a} \right)
        \\
        &=
        \frac{2 V_0}{\pi} \tan^{-1}
        \left( \frac{\sin(\pi y/a)}{\sinh(\pi x/a)} \right)
    \end{aligned}
\end{equation}\\

\separator%

\textbf{Two important properties of S.O.V}
\begin{enumerate}
    \item \textbf{Completeness:} A set of functions, $f_n(y)$, is said to be \textbf{complete} if any other function, $g(y)$, can be expressed as a linear combination of them:
        \[
            g(y) = \sum_n C_n f_n(y)
        \]
        For example, the functions, $\sin(n\pi y/a)$ are complete on \textit{the interval} $0 \le y \le a$.

    \item \textbf{Orthogonality:} A set of functions, $f_n(y)$, is \textbf{orthogonal} if
        \[
            \int_0^a f_n(y) f_m(y) dy = 0
            \qquad\mathrm{for}\ m \ne n
        \]
        For example, the functions, $\sin(n\pi y/a)$, are orthogonal on \textit{the interval} $0 \le y \le a$.
\end{enumerate}

\framed{%
    \example{3.4}%
    Two infinitely-long grounded metal plates, at $y = 0$ and $y = a$, are connected at $x = \pm b$ by metal strips maintained at a constant potential $V_0$.
    Find the potential inside.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch03-example_4-01}
    \caption{Example 3.4 Configuration.}%
    \label{fig:ch03:Example4}
\end{figure}
Again, the configuration is independent of $z$, which boils down to solving
\[
    \frac{\partial^2 V}{\partial x^2}
    +
    \frac{\partial^2 V}{\partial y^2}
    =
    0
\]
subject to the boundary conditions
\begin{enumerate}
    \item [(i)] $V = 0$ when $y = 0$
    \item [(ii)] $V = 0$ when $y = a$
    \item [(iii)] $V = V_0$ when $x = b$
    \item [(iv)] $V = V_0$ as $x = -b$
        \\
\end{enumerate}

After separation of variables, we may arrive at
\begin{equation}
    V(x, y)
    =
    (A \cosh kx + B \sinh kx)
    (C \sin ky + D \cos ky)
\end{equation}
Notice the hyperbolic cosine and sine instead of $e^{\pm kx}$ (the reason will be clear shortly).

Conditions (iii) and (iv) demand $B = 0$ because $V(+x) = V(-x)$ (i.e., even in $x$).
Thus, $B = 0$, and therefore
\begin{equation}
    V(x, y) = \cosh kx (C \sin ky + D \cos ky)
\end{equation}
As before, conditions (i) and (ii) require that $D = 0$ and $k = n\pi/a$, so the most general case will be
\begin{equation}
    V(x, y)
    =
    \sum_{n = 1}^{\infty}
    C_n \cosh\left( \frac{n\pi x}{a} \right) \sin\left( \frac{n\pi y}{a} \right)
\end{equation}
Finally, we pick the coefficients $C_n$ in such a way as to satisfy condition (iii)
\begin{equation}
    V(b, y)
    =
    \sum_{n = 1}^\infty
    C_n \cosh\left( \frac{n\pi b}{a} \right) \sin\left( \frac{n\pi y}{a} \right)
    =
    V_0
\end{equation}
From the orthogonality of $\sin(n\pi y/a)$, it follows
\begin{equation}
    C_n \cosh\left( \frac{n\pi b}{a} \right)
    =
    \left\{
        \begin{array}{ccl}
            0 & & n \in\,\mathrm{even} \\
            \\
            \displaystyle \frac{4 V_0}{n\pi} & & n \in\,\mathrm{odd} \\
        \end{array}
    \right.
\end{equation}
Therefore, the potential in this case is given by
\begin{equation}
    V(x, y)
    =
    \frac{4 V_0}{\pi} \sum_{n = 1, 3, 5, \dots}
    \frac{1}{n} \frac{\cosh(n\pi x/a)}{\cosh(n\pi b/a)}
    \sin\left( \frac{n\pi y}{a} \right)
\end{equation}

\homework{3.13, 3.14, 3.15}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Spherical Coordinates}
For round objects, spherical coordinates are more natural.
Laplace's equation in spherical coordinates reads
\begin{equation}
    \frac{1}{r^2} \frac{\partial}{\partial r} \left( r^2 \frac{\partial V}{\partial r} \right)
    +
    \frac{1}{r^2 \sin\theta} \frac{\partial}{\partial\theta} \left( \sin\theta \frac{\partial V}{\partial\theta} \right)
    +
    \frac{1}{r^2 \sin^2\theta} \frac{\partial^2 V}{\partial\phi^2}
    =
    0
\end{equation}
We will assume that the problem has \textit{azimuthal symmetry} (so that $\partial/\partial\phi = 0$), in which case Laplace's equation reduces
\begin{equation}
    \frac{\partial}{\partial r} \left( r^2 \frac{\partial V}{\partial r} \right)
    +
    \frac{1}{\sin\theta} \frac{\partial}{\partial\theta} \left( \sin\theta \frac{\partial V}{\partial\theta} \right)
    =
    0
    \label{eq:ch03:Laplace:Spherical}
\end{equation}
We look for solutions that are products
\begin{equation}
    V(r, \theta) = R(r) \Theta(\theta)
\end{equation}
Putting this into Eq.\ (\ref{eq:ch03:Laplace:Spherical}) and dividing it by $R \Theta$
\begin{equation}
    \frac{1}{R} \frac{d}{dr} \left( r^2 \frac{dR}{dr} \right)
    +
    \frac{1}{\Theta \sin\theta} \frac{d}{d\theta}\left( \sin\theta \frac{d\Theta}{d\theta} \right)
    =
    0
\end{equation}
which is of the form $f(r) + g(\theta) = 0$.
Choosing $l(l + 1)$ as the constant, it follows
\begin{equation}
    \frac{d}{dr} \left( r^2 \frac{dR}{dr} \right)
    =
    l(l + 1) R
    ,\quad
    \frac{d}{d\theta}\left( \sin\theta \frac{d\Theta}{d\theta} \right)
    =
    -l(l + 1) \sin\theta\, \Theta
\end{equation}

The radial equation has the general solution
\begin{equation}
    R(r) = A r^l + \frac{B}{r^{l + 1}}
\end{equation}
The solution of the angular equation is given by Legendre polynomials
\begin{equation}
    \Theta(\theta) = P_l(\cos\theta)
\end{equation}
where $P_l(x)$ is defined by the Rodrigues formula
\[
    P_l(x)
    =
    \frac{1}{2^l l!} \frac{d^l}{dx^l} (x^2 - 1)^l
\]
Note that the factor $1/(2^l l!)$ was chosen in order that $P_l(1) = 1$.
The first few Legendre polynomials are
\[
    \boxed{%
        \begin{aligned}
            P_0(x) &= 1 \\
            P_1(x) &= x \\
            P_2(x) &= \frac{1}{2} (3x^2 - 1) \\
            P_3(x) &= \frac{1}{2} (5x^3 - 3x) \\
            P_4(x) &= \frac{1}{8} (35x^4 - 30x^2 + 3) \\
            P_5(x) &= \frac{1}{8} (63x^5 - 70x^3 + 15x) \\
        \end{aligned}
    }
\]

In the case of azimuthal symmetry, the most general solution to Laplace's equation is
\begin{equation}
    \boxed{%
        V(r, \theta)
        =
        \sum_{l = 0}^\infty
        \left( A_l r^l + \frac{B_l}{r^{l + 1}} \right)
        P_l(\cos\theta)
    }
    \label{eq:ch03:LaplaceSolution:Spherical}
\end{equation}

\framed{%
    \example{3.6}%
    The potential $V_0(\theta)$ is specified on the surface of a hollow sphere, of radius $R$.
    Find the potential inside.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch03-example_6-01}
    \caption{Example 3.6 Configuration.}%
    \label{fig:ch03:Example6}
\end{figure}
Boundary conditions are
\begin{enumerate}
    \item [(i)] $V = \mathrm{finite}$ at $r = 0$
    \item [(ii)] $V = V_0(\theta)$ when $r = R$
        \\
\end{enumerate}

Condition (i) demands $B_l = 0$, so
\[
    V(r, \theta) = \sum_{l = 0}^\infty A_l r^l P_l(\cos\theta)
\]
At $r = R$, this must match $V_0(\theta)$:
\begin{equation}
    V(R, \theta)
    =
    \sum_{l = 0}^\infty A_l R^l P_l(\cos\theta)
    =
    V_0(\theta)
    \label{eq:ch03:Example6:PotentialAtR}
\end{equation}
From the orthogonality of $P_l(x)$, we find $A_l$:
\[
    \begin{aligned}
        \int_{-1}^{1} P_l(x) P_{l'}(x) dx
        &\underset{\color{blue}x \rightarrow \cos\theta}{=}
        \int_0^{\pi} P_l(\cos\theta) P_{l'}(\cos\theta) \sin\theta d\theta
        \\
        &\quad=
        \left\{
            \begin{array}{ccl}
                0 & & l' \ne l \\
                \\
                \displaystyle \frac{2}{2l + 1} & & l' = l
            \end{array}
        \right.
    \end{aligned}
\]
Multiplying Eq.\ (\ref{eq:ch03:Example6:PotentialAtR}) by $(P_{l'}(\cos\theta) \sin\theta)$ and integrating over $\theta$, we have
\[
    A_{l'} R^{l'} \frac{2}{2l' + 1}
    =
    \int_0^{\pi} V_0(\theta) P_{l'}(\cos\theta) \sin\theta d\theta
\]
or
\begin{equation}
    A_l = \frac{2l + 1}{2 R^l} \int_0^{\pi} V_0(\theta) P_l(\cos\theta) \sin\theta d\theta
\end{equation}

Suppose $V_0(\theta) = k \sin^2(\theta/2)$.
Using the half-angle formula,
\[
    V_0(\theta) = \frac{k}{2} (1 - \cos\theta) = \frac{k}{2} \left( P_0(\cos\theta) - P_1(\cos\theta) \right)
\]
Putting it into Eq.\ (\ref{eq:ch03:Example6:PotentialAtR})
\[
    \sum_{l = 0}^\infty A_l R^l P_l(\cos\theta)
    =
    \frac{k}{2} \left( P_0(\cos\theta) - P_1(\cos\theta) \right)
\]
and comparing both sides term by term, we read off immediately that
\[
    A_0 = \frac{k}{2}
    ,\quad
    A_1 = -\frac{k}{2 R}
\]
and all other $A_l$'s are zero.
Therefore,
\begin{equation}
    V(r, \theta)
    =
    \frac{k}{2} \left( r^0 P_0(\cos\theta) - \frac{r^1}{R} P_1(\cos\theta) \right)
    =
    \frac{k}{2} \left( 1 - \frac{r}{R} \cos\theta \right)
\end{equation}
Since $z = r\cos\theta$, we can immediately recognize
\begin{equation}
    V(z) = \frac{k}{2} - \frac{k}{2}\frac{z}{R}
\end{equation}
The electric field is then
\begin{equation}
    \mathbf E = - \nabla V = \frac{k}{2R} \uvec z
\end{equation}

\framed{%
    \example{3.7}%
    The potential is again specified on the surface of sphere of radius $R$, but this time we are asked to find the potential outside, assuming there is no charge there.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch03-example_7-01}
    \caption{Example 3.7 Configuration.}%
    \label{fig:ch03:Example7}
\end{figure}
In this case, $A_l = 0$ because $V \rightarrow 0$ as $r \rightarrow \infty$; thus
\begin{equation}
    V(r, \theta)
    =
    \sum_{l = 0}^\infty \frac{B_l}{r^{l + 1}} P_l(\cos\theta)
\end{equation}
At $r = R$, we require that
\begin{equation}
    V(R, \theta)
    =
    \sum_{l = 0}^\infty \frac{B_l}{R^{l + 1}} P_l(\cos\theta)
    =
    V_0(\theta)
    \label{eq:ch03:Example7:PotentialAtR}
\end{equation}
We find $B_l$'s by exploiting the orthogonality of $P_l(x)$:
\[
    B_l
    =
    \frac{2l + 1}{2} R^{l + 1}
    \int_0^{\pi} V_0(\theta) P_l(\cos\theta) \sin\theta d\theta
\]\\

Suppose that $V_0(\theta) = k \sin^2(k/2)$; then we put it into Eq.\ (\ref{eq:ch03:Example7:PotentialAtR})
\[
    \sum_{l = 0}^\infty \frac{B_l}{R^{l + 1}} P_l(\cos\theta)
    =
    \frac{k}{2} \left( P_0(\cos\theta) - P_1(\cos\theta) \right)
\]
and read off the coefficients
\[
    B_0 = R \frac{k}{2}
    ,\qquad
    B_1 = -R^2 \frac{k}{2}
\]
and all other $B_l$'s are zero.
Therefore,
\begin{equation}
    \begin{aligned}
        V(r, \theta)
        &=
        \frac{k}{2}
        \left(
            \frac{R}{r} P_0(\cos\theta)
            -
            \frac{R^2}{r^2} P_1(\cos\theta)
        \right)
        \\
        &=
        \frac{k}{2}
        \left(
            \underbrace{\frac{R}{r}}_{\color{blue}\mathrm{monopole}}
            -\quad
            \underbrace{\frac{R^2}{r^2} \cos\theta}_{\color{blue}\mathrm{dipole}}
        \right)
    \end{aligned}
\end{equation}
Note that the first term is the monopole contribution while the second is the dipole contribution.

\framed{%
    \example{3.8}%
    An \textit{uncharged} metal sphere of radius $R$ is placed in an otherwise uniform electric field $\mathbf E = E_0 \uvec z$.
    This external electric field will, in turn, induce charge on the surface of the metal sphere---\textit{positive} charge to the northern surface and \textit{negative} to the southern surface---which will distort the field in the neighborhood of the sphere.
    Find the potential outside.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch03-example_8-01}
    \caption{Example 3.8 Configuration.}%
    \label{fig:ch03:Example8}
\end{figure}
The sphere is equipotential; we may as well set it to zero:
\[
    \text{(i)}\quad V = 0\ \text{at}\ r = R
\]
From north-south symmetry, we also deduce
\[
    \text{(ii)}\quad V = 0\ \text{at}\ z = 0\ \text{(at the $xy$ plane)}
\]
Finally, at a distance far from the sphere, the field must approach $E_0 \uvec z$.
In terms of potential,
\[
    \text{(iii)}\quad V \rightarrow -E_0 z + C\ \text{when}\ r \rightarrow \infty
\]
By condition (ii), the constant $C$ must be zero.
\[
    \text{(iv)}\quad C = 0
\]

Condition (i) yields
\[
    A_l R^l + \frac{B_l}{R^{l + 1}} = 0
    \quad \rightarrow \quad
    B_l = -A_l R^{2l + 1}
\]
so
\begin{equation}
    V(r, \theta)
    =
    \sum_{l = 0}^\infty A_l \left( r^l - \frac{R^{2l + 1}}{r^{l + 1}} \right)
    P_l(\cos\theta)
\end{equation}
At $r \gg R$, condition (iii) demands
\[
    V(r \rightarrow \infty, \theta)
    =
    \sum_{l = 0}^\infty A_l r^l P_l(\cos\theta)
    =
    -E_0 r \cos\theta
    =
    -E_0 r P_1(\cos\theta)
\]
From the term-by-term comparison, we find that $A_1 = -E_0$ and all other $A_l$'s are zero.
Therefore, the potential outside is given by
\begin{equation}
    \begin{aligned}
        V(r, \theta)
        &=
        -E_0 \left( r - \frac{R^3}{r^2} \right) \cos\theta
        \\
        &=
        \underbrace{-E_0 z}_{\color{blue}(1)}
        +
        \underbrace{E_0 \frac{R^3}{r^2} \cos\theta}_{\color{blue}(2)}
    \end{aligned}
\end{equation}
The term (1) is due to the external field and the term (2) is the dipole contribution due to induced charge.

The induced charge is
\begin{equation}
    \sigma(\theta)
    =
    -\epsilon_0 \left. \frac{\partial V}{\partial r} \right|_{r = R}
    =
    \left. \epsilon_0 E_0 \left( 1 + 2\frac{R^3}{r^3} \right) \cos\theta \right|_{r = R}
    =
    3 \epsilon_0 E_0 \cos\theta
\end{equation}
You can calculate the dipole moment of the induced charge and confirm the dipole contribution to the potential.

\framed{%
    \example{3.9}%
    A specified charge density $\sigma_0(\theta)$ is glued over the surface of a spherical shell of radius $R$.
    Find the potential inside and outside.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch03-example_9-01}
    \caption{Example 3.9 Configuration.}%
    \label{fig:ch03:Example9}
\end{figure}
You could find the potential by integrating
\[
    V = \frac{1}{4\pi\epsilon_0} \int \frac{\sigma_0}{\eta} da
\]
Here, let's use the method of separation of variables.\\

For the interior region,
\begin{equation}
    V(r, \theta)
    =
    \sum_{l = 0}^\infty A_l r^l P_l(\cos\theta)
    ,\qquad r \le R
\end{equation}
In the exterior region,
\begin{equation}
    V(r, \theta)
    =
    \sum_{l = 0}^\infty \frac{B_l}{r^{l + 1}} P_l(\cos\theta)
    ,\qquad r \ge R
\end{equation}
Since the potential must be continuous across surface charge, we require that
\[
    \sum_{l = 0}^\infty A_l R^{l} P_l(\cos\theta)
    =
    \sum_{l = 0}^\infty \frac{B_l}{R^{l + 1}} P_l(\cos\theta)
\]
from which
\begin{equation}
    B_l = A_l R^{2l + 1}
\end{equation}
Furthermore, the normal derivative of the potential at surface charge must be discontinuous by an amount proportional to the surface charge,
\[
    \left. \left( \frac{\partial V_{\rm out}}{\partial r} - \frac{\partial V_{\rm in}}{\partial r} \right) \right|_{r = R}
    =
    -\frac{\sigma_0}{\epsilon_0}
\]
Evaluating it, we get
\[
    -\sum_{l = 0}^\infty (l + 1) \frac{B_l}{R^{l + 2}} P_l(\cos\theta)
    -\sum_{l = 0}^\infty l A_l R^{l - 1} P_l(\cos\theta)
    =
    -\frac{\sigma_0}{\epsilon_0}
\]
Substituting $B_l$ in terms of $A_l$ we obtained earlier, it follows
\[
    \sum_{l = 0}^\infty (2l + 1) A_l R^{l - 1} P_l(\cos\theta) = \frac{\sigma_0(\theta)}{\epsilon_0}
\]
The coefficients $A_l$ can be determined using the orthogonality of $P_l(x)$:
\begin{equation}
    A_l = \frac{1}{\epsilon_0 R^{l - 1}} \int_0^{\pi} \sigma_0(\theta) P_l(\cos\theta) \sin\theta d\theta
\end{equation}

If $\sigma_0(\theta) = k \cos\theta = k P_1(\cos\theta)$, then all but $A_1$ are zero:
\begin{equation}
    A_1 = \frac{k}{2\epsilon_0} \int_0^{\pi} P_1^2(\cos\theta) \sin\theta d\theta = \frac{k}{3\epsilon_0}
\end{equation}
The potential is therefore
\begin{equation}
    V(r, \theta)
    =
    \left\{
        \begin{array}{ccl}
            \displaystyle \frac{k}{3\epsilon_0} r \cos\theta & & r \le R \\
            \\
            \displaystyle \frac{k R^3}{3\epsilon_0} \frac{1}{r^2} \cos\theta & & r \ge R \\
        \end{array}
    \right.
\end{equation}
Interestingly, the potential inside is
\begin{equation}
    V = \frac{k}{3\epsilon_0} z
\end{equation}
Thus, the field inside is
\[
    \mathbf E = -\frac{k}{3\epsilon_0} \uvec z
\]
If $\sigma_0(\theta)$ is the induced charge on a metal sphere in an external field given by $E_0 \uvec z$ with $E_0 = k/(3\epsilon_0)$, then the potential inside exactly cancels off the external field so as to make the field inside the metal zero (Ex. 3.8).

\homework{3.18, 3.19, 3.21, 3.24, 3.25, 3.26}
