\section{Magnetization}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Diamagnets, Paramagnets, Ferromagnets}
\begin{itemize}
    \item All ``magnetic'' phenomena are due to electric charges in motion
        \begin{enumerate}
            \item [(1)] Electron orbital motion
            \item [(2)] Electron spin
        \end{enumerate}
        We can treat them as \textit{magnetic dipoles}.

    \item When a magnetic field is applied, a net alignment of these magnetic dipoles occurs

        $\rightarrow$ The medium becomes magnetically polarized, or \textbf{magnetized}.

    \item Unlike electric polarization, which is almost always in the \textit{same direction} as $\mathbf E$, some materials acquire a magnetization \textit{parallel} to $\mathbf B$ (called \textbf{paramagnets}) and some \textit{opposite} to it (called \textbf{diamagnets}).

    \item A few substances (called \textbf{ferromagnets}) retain their magnetization even after the external field has been removed (e.g., iron).

    \item We will mostly focus on models of paramagnetism and diamagnetism.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Torques and Forces on Magnetic Dipoles}
\subsubsection{Uniform Field}
Let's calculate the torque on a rectangular current loop in a uniform $\mathbf B$.

\framed{%
    \textit{Remember} that there is no loss of generality of using a rectangular current loop here because any current loop can be built up from infinitesimal rectangles.
}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch06-torque_on_retangular_current_loop-01}
    \caption{Torque on a rectangular current loop.}%
    \label{fig:ch06:TorqueOnRetangularCurrentLoop}
\end{figure}
Place the current loop as shown in Figure~\ref{fig:ch06:TorqueOnRetangularCurrentLoop}a.
Recall that the force on a line current is
\[
    \mathbf F = \int \mathbf I \times \mathbf B dl
    = I \int d\mathbf l \times \mathbf B
\]
\begin{itemize}
    \item The forces on the two sloping sides (with length $a$) are equal and opposite, and thus cancel each other.

    \item The forces on the horizontal sides (with length $b$) are likewise equal and opposite.
\end{itemize}
Therefore, the net force on the loop is zero.

But, they \textit{do} generate a torque:
\[
    \mathbf N
    =
    \mathbf r_+ \times \mathbf F_+
    +
    \mathbf r_- \times \mathbf F_-
    =
    a F \sin\theta \uvec x
\]
Since $I$ and $\mathbf B$ are constant, the magnitude of the force on each end is
\[
    F = I b B
\]
Therefore,
\[
    \mathbf N
    =
    \underbrace{I a b}_{\color{blue} m}
    B \sin\theta \uvec x
    =
    m B \sin\theta \uvec x
\]
or
\begin{equation}
    \boxed{%
        \mathbf N = \mathbf m \times \mathbf B
    }
    \label{eq:ch06:TorqueOnMagneticDipole}
\end{equation}
where $m = I a b$ is the magnetic dipole moment of the loop.

The energy of the magnetic dipole in $\mathbf B$ is given by
\[
    U = \int m B \sin\theta d\theta = -mB \cos\theta = -\mathbf m \cdot \mathbf B
\]

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch06-source_of_paramagnetism-01}
    \caption{Paramagnetism.}%
\end{figure}
\begin{itemize}
    \item Eq.~(\ref{eq:ch06:TorqueOnMagneticDipole}) gives the torque on any localized current distribution, in the presence of a \textit{uniform} field.

    \item In a \textit{nonuniform} field, it is the exact torque (about the center) for a perfect dipole of infinitesimal size.

    \item The torque is in such a direction as to line the dipole up parallel to the field: \textbf{Paramagnetism}.

    \item The paramagnetism occurs due to \textit{electron spin}: If there are \textit{odd} numbers of electrons, the unpaired member is subject to the magnetic torque.
\end{itemize}

In a uniform field, the net force on a current loop is zero
\[
    \mathbf F = I \oint d\mathbf l \times \mathbf B = -I \mathbf B \times \oint d\mathbf l = 0
\]

\subsubsection{Nonuniform Field}
In a nonuniform field, there is non-zero net force.\\

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch06-force_on_magnetic_dipole_nonuniform-01}
    \caption{Force on a current loop in a nonuniform field.}%
\end{figure}
Suppose a circular wire ring of radius $R$, carrying a current $I$, is suspended above a short solenoid in the fringing region.
We assume that $R$ is sufficiently small (smaller than the diameter of the solenoid).

Writing $\nabla \cdot \mathbf B = 0$ out in cylindrical coordinates, assuming cylindrical symmetry (i.e., $\partial/\partial\phi = 0$),
\[
    \frac{1}{s} \frac{\partial}{\partial s} (s B_s) + \frac{\partial B_z}{\partial z} = 0
    \quad \rightarrow \quad
    \frac{\partial}{\partial s} (s B_s) = -s \frac{\partial B_z}{\partial z}
\]
where $|B_z| \gg |B_s|$ and the $z$ axis is parallel to the solenoid.

Integrating both sides and assuming that $\partial B/\partial z$ does not vary mush with respect to $s$, we have
\[
    B_s
    =
    -\frac{s}{2} \left. \left(\frac{\partial B_z}{\partial z}\right) \right|_{s = 0}
    =
    -\frac{s}{2} \frac{\partial B_z(0)}{\partial z}
\]
Therefore, the magnetic field vector can be approximated as
\[
    \mathbf B(s) \approx B_z(0) \uvec z - \frac{s}{2} \frac{\partial B_z(0)}{\partial z} \uvec s
\]
Substituting it into the magnetic force formula on the ring,
\[
    \begin{aligned}
        \mathbf F
        &=
        I \oint d\mathbf l \times \mathbf B(R)
        \\
        &\approx
        I \oint d\mathbf l \times \left( B_z(0) \uvec z - \frac{R}{2} \frac{\partial B_z(0)}{\partial z} \uvec s \right)
        \\
        &=
        I \cancelto{\color{blue}0}{\left( \oint d\mathbf l \right)} \times B_z(0) \uvec z
        +
        I \frac{\partial B_z(0)}{\partial z} \left( \frac{1}{2} \oint (R \uvec s) \times d\mathbf l \right)
    \end{aligned}
\]
The term in the parentheses is the area $a$ enclosed by the ring (Eq.~1.107 in the textbook); and thus
\[
    \mathbf F \approx \frac{\partial B_z}{\partial z} (I a \uvec z)
\]
which is evaluated at $s = 0$.
Using $\mathbf m = I a \uvec z$ and realising that $\mathbf m$ is constant, we can rearrange the expression
\[
    \mathbf F \approx \uvec z \frac{\partial}{\partial z} (\mathbf m \cdot \mathbf B)
\]
Generalizing it to a coordinate-independent form, we have
\begin{equation}
    \boxed{%
        \mathbf F = \nabla (\mathbf m \cdot \mathbf B)
    }
\end{equation}
Using the vector identity assuming uniform $\mathbf m$, one can write
\[
    \begin{aligned}
        \mathbf F
        &=
        \mathbf m \times (\nabla \times \mathbf B)
        +
        \mathbf B \times \cancelto{\color{red}0}{(\nabla \times \mathbf m)}
        +
        (\mathbf m \cdot \nabla) \mathbf B
        +
        \cancelto{\color{red}0}{(\mathbf B \cdot \nabla) \mathbf m}
        \\
        &=
        (\mathbf m \cdot \nabla) \mathbf B
        +
        \mathbf m \times \underbrace{(\nabla \times \mathbf B)}_{\color{blue}=\mu_0\mathbf J}
    \end{aligned}
\]
Since $\nabla \times \mathbf E = 0$ for the electrostatic limit, one can also write Eq.~(\ref{eq:ch04:NetForceOnPolarMoleculeInNonuniformField}) as $\mathbf F = \nabla (\mathbf p \cdot \mathbf E)$ as long as $\mathbf p$ is uniform.
This is not in general possible for the magnetic dipole case because $\mathbf J$ may not be zero.

\homework{6.1}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Effect of a Magnetic Field on Atomic Orbits}
\framed{%
    Not only the spin, but also orbital motion of electrons around the nucleus generate a magnetic dipole.
}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch06-magnetic_field_on_atomic_orbits-01}
    \caption{Atomic orbit.}%
\end{figure}
Let's assume that the orbit is a circle of radius $R$.
The current due to the electron's orbital motion is
\[
    I = \frac{-e}{T} = -\frac{ev}{2\pi R}
\]
where $T = 2\pi R/v$ is the orbital period.

\framed{%
    Technically, this orbital motion does not constitute a steady current, but the period is so short that it will look like a steady current.
}

\noindent
The corresponding dipole moment is
\begin{equation}
    \mathbf m = I \pi R^2 \uvec z = -\frac{1}{2} e v R \uvec z
\end{equation}
Like any other magnetic dipole, this one is subject to a torque ($\mathbf m \times \mathbf B$).

\begin{itemize}
    \item However, the orbital contribution to paramagnetism is small because it is a lot harder to tilt the entire orbit of the electron than it is the spin.

    \item A more significant effect on the orbital motion comes from the fact that the electron can speed up and slow down, depending on the orientation of $\mathbf B$.
\end{itemize}

Without $\mathbf B$, the centrifugal force is balanced by the electric force (assuming hydrogen atom)
\begin{equation}
    \frac{1}{4\pi\epsilon_0} \frac{e^2}{R^2} = m_e \frac{v^2}{R}
\end{equation}
In the presence of a magnetic field there is an additional force $-e (\mathbf v \times \mathbf B)$.
Let's say that $\mathbf B = B\uvec z$; then
\[
    \mathbf F = \mathbf F_e + \mathbf F_m
    = -\frac{1}{4\pi\epsilon_0} \frac{e^2}{R^2} \uvec r - e \bar v B \uvec r
\]
so
\begin{equation}
    \underbrace{\frac{1}{4\pi\epsilon_0} \frac{e^2}{R^2}}_{\color{blue} m_e v^2/R}
    +
    e \bar{v} B = m_e \frac{\bar v^2}{R}
\end{equation}
where $\bar v$ is the modified speed.
Rearranging it,
\[
    e \bar v B
    =
    \frac{m_e}{R} (\bar v^2 - v^2)
    =
    \frac{m_e}{R} (\bar v + v) (\bar v - v)
\]
or assuming the change $\Delta v = \bar v - v$ is small (and thus $\bar v + v \approx 2 \bar v$), it follows
\begin{equation}
    \Delta v = \frac{e R B}{2 m_e} \ {\color{blue} >\, 0}
\end{equation}
Thus, under these conditions, the new speed $\bar v$ is greater than $v$.
Therefore, when $\mathbf B$ is turned on, then, the electron speeds up.
(When $\mathbf B = -B\uvec z$, we have $\Delta v = -e R B/(2 m_e)$, which means that the electron slows down.)

A change in orbital speed means a change in the dipole moment
\begin{equation}
    \Delta \mathbf m
    =
    -\frac{1}{2} e (\Delta v) R \uvec z
    =
    {\color{blue}-}\frac{e^2 R^2}{4 m_e} \mathbf B
\end{equation}
which is opposite to the direction of $\mathbf B$.
\textit{The same is true even if the angular momentum of an electron is flipped.}

\begin{itemize}
    \item Ordinarily, the electron orbits are randomly oriented, and the orbital dipole moments cancel out.

    \item But in the presence of a magnetic field, each atom picks up a little \textit{extra} dipole moment that are all \textit{antiparallel} to $\mathbf B$, creating \textbf{diamagnetism}.

    \item This is typically much weaker than paramagnetism, and therefore observed mainly in atoms with even numbers of electrons where paramagnetism is usually absent.

    \item Despite the simplification of our model, experiments indeed show that diamagnetic materials induce dipole moments opposite to $\mathbf B$ applied.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Magnetization}
\begin{itemize}
    \item In the presence of a magnetic field, matter becomes magnetized.

    \item Two mechanisms we have discussed are: (1) paramagnetism and (2) diamagnetism.

    \item Regardless of the cause, we describe the state of magnetic polarization by
        \[
            \mathbf M = \text{magnetic dipole moment per unit volume}
        \]
        called the \textbf{magnetization}.
\end{itemize}
