\section{Linear Dielectrics}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Susceptibility, Permittivity, Dielectric Constant}
\begin{itemize}
    \item In general, calculating the field with a dielectric involved is not straightforward, because the Gauss's law
        \[
            \epsilon_0 \nabla \cdot \mathbf E
            =
            -\nabla \cdot \mathbf P + \rho_f
        \]
        is a nonlinear equation for the field because of the dependence of $\mathbf P$ on the field $\mathbf E$ itself.

    \item We know that the polarization of a dielectric ordinarily results from an electric field.

    \item For many substances, the polarization is \textit{proportional} to the (total) field $\mathbf E$, provided $\mathbf E$ is not too strong
        \begin{equation}
            \mathbf P = \epsilon_0 \chi_e \mathbf E
            \label{eq:ch04:LinearDielectric}
        \end{equation}

    \item The constant of proportionality $\chi_e$, which is \textit{dimensionless}, is called the \textbf{electric susceptibility} of the medium.
\end{itemize}

Materials that obey Eq.\ (\ref{eq:ch04:LinearDielectric}) is called \textbf{linear dielectrics}.

\framed{%
    Note that $\mathbf E$ in Eq.\ (\ref{eq:ch04:LinearDielectric}) is the total field:
    \begin{itemize}
        \item the field due to the free charge, \textit{plus}
        \item the field due to the polarization itself.
    \end{itemize}
}

In linear media, we have
\begin{equation}
    \mathbf D = \epsilon_0 \mathbf E + \mathbf P
    = \epsilon_0\mathbf E + \epsilon_0\chi_e\mathbf E
    = \epsilon_0 (1 + \chi_e) \mathbf E
\end{equation}
So, $\mathbf D$ is also proportional to $\mathbf E$:
\begin{equation}
    \mathbf D = \epsilon \mathbf E
    \qquad \text{for linear media}
\end{equation}
where
\begin{equation}
    \epsilon = \epsilon_0(1 + \chi_e)
\end{equation}
This new constant $\epsilon$ is called the \textbf{permittivity} of the material.
Note that $\epsilon$ becomes $\epsilon_0$ (the permittivity of free space), when $\chi_e = 0$.

Finally, the dimensionless quantity
\begin{equation}
    \epsilon_r = 1 + \chi_e = \frac{\epsilon}{\epsilon_0}
\end{equation}
is called the \textbf{relative permittivity}, or \textbf{dielectric constant}, of the material.\\

Note that the physics of linear dielectrics is all contained in Eq.\ (\ref{eq:ch04:LinearDielectric}) (and thus $\chi_e$); other constants do not convey anything new (other than notational convenience).

\framed{%
    \example{4.5}%
    A metal sphere of radius $a$ carries a charge $Q$.
    It is surrounded, out to radius $b$, by linear dielectric material of permittivity $\epsilon$.
    Find the potential at the center (relative to infinity).
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-example_5-01}
    \caption{Example 4.5 Potential of a linear spherical dielectric.}%
\end{figure}
For linear dielectric, Gauss's law reads
\[
    \nabla \cdot \mathbf D
    =
    \nabla \cdot (\epsilon \mathbf E)
    =
    \rho_f
\]
The electric field in the three regions is
\[
    \mathbf E
    =
    \left\{
        \begin{array}{ccc}
            \displaystyle \frac{1}{4\pi\epsilon_0} \frac{Q}{r^2} \uvec r & & r > b \\
            \\
            \displaystyle \frac{1}{4\pi\epsilon} \frac{Q}{r^2} \uvec r & & a < r < b \\
            \\
            0 & & r < a \\
        \end{array}
    \right.
\]
The potential at the center relative to infinity is
\[
    \begin{aligned}
        V = -\int_\infty^0 \mathbf E \cdot d\mathbf l
        &=
        -\int_a^0 \mathbf E \cdot d\mathbf l
        -\int_b^a \mathbf E \cdot d\mathbf l
        -\int_\infty^b \mathbf E \cdot d\mathbf l
        \\
        &=
        (0)
        +
        \frac{Q}{4\pi\epsilon} \left( \frac{1}{a} - \frac{1}{b} \right)
        +
        \frac{Q}{4\pi\epsilon_0} \frac{1}{a}
        \\
        &=
        \frac{Q}{4\pi}
        \left( \frac{1}{\epsilon_0 b} + \frac{1}{\epsilon a} - \frac{1}{\epsilon b} \right)
    \end{aligned}
\]

Note that it was not necessary to compute the polarization, or the bound charge, explicitly.
Nevertheless, the polarization is (using $\epsilon = \epsilon_0 + \epsilon_0 \chi_e$)
\[
    \mathbf P = \epsilon_0\chi_e \mathbf E
    = (\epsilon - \epsilon_0) \mathbf E
    = \frac{Q}{4\pi} \frac{\epsilon - \epsilon_0}{\epsilon} \frac{\uvec r}{r^2}
\]
The bound charge is
\[
    \rho_b = -\nabla \cdot \mathbf P = 0
\]
and
\[
    \sigma_b = \mathbf P \cdot \uvec n
    =
    \left\{
        \begin{array}{ccc}
            \displaystyle \frac{Q}{4\pi} \frac{\epsilon - \epsilon_0}{\epsilon} \frac{1}{b^2} & & r = b \\
            \\
            \displaystyle -\frac{Q}{4\pi} \frac{\epsilon - \epsilon_0}{\epsilon} \frac{1}{a^2} & & r = a \\
        \end{array}
    \right.
\]
Notice that $\sigma_b(r = a) < 0$; this is because $Q$ attracts opposite charge in all dielectric molecules.
It is \textit{this layer of negative charge that reduces the field within the dielectric} from $\frac{1}{4\pi\epsilon_0}\frac{Q}{r^2}\uvec r$ to $\frac{1}{4\pi\epsilon}\frac{Q}{r^2}\uvec r$, i.e., by $\frac{\epsilon_0}{\epsilon} = \frac{1}{\epsilon_r}$.
\\

On a conducting shell, the induced charge will cancel the field of $Q$ completely.
The dielectric is like an imperfect conductor so that the cancellation is only \textit{partial}.

\separator%

Even though the material is linear dielectric, $\nabla \times \mathbf D \ne 0$ in general, because $\chi_e$ can be inhomogeneous ($\nabla \chi_e \ne 0$).

On other other hand, if the space is entirely filled with a homogeneous linear dielectric ($\nabla \chi_e = 0$), then
\[
    \begin{aligned}
        \nabla \times \mathbf D
        &=
        \epsilon_0 (\underbrace{\nabla \times \mathbf E}_{\color{blue} = 0} + \nabla \times (\chi_e \mathbf E))
        \\
        &=
        \epsilon_0 (\mathbf E \times \nabla \chi_e + \chi_e \nabla \times \mathbf E)
        \\
        &=
        0
    \end{aligned}
\]
Therefore, in this special circumstance,
\[
    \nabla \cdot \mathbf D = \rho_f
    \quad \text{and} \quad
    \nabla \times \mathbf D = 0
\]
so $\mathbf D$ can be found from free charge just as though the dielectric were not there:
\[
    \mathbf D = \epsilon_0 \mathbf E_{\rm vac}
\]
where $\mathbf E_{\rm vac}$ is the field the same free charge distribution would produce in the absence of any dielectric.

Therefore, the total electric field within the dielectric material will be reduced by $\epsilon_r$ relative to $\mathbf E_{\rm vac}$:
\begin{equation}
    \mathbf E = \frac{\mathbf D}{\epsilon} = \frac{1}{\epsilon_r} \mathbf E_{\rm vac}
    \label{eq:ch04:FieldInHomogeneousDielectric}
\end{equation}
For example, if a free charge $q$ is embedded in a large dielectric, the field it produces satisfies
\[
    \nabla \cdot (\epsilon \mathbf E) = \epsilon (\nabla \cdot \mathbf E) = \rho_f
    ,\qquad
    \nabla \times (\epsilon\mathbf E) = \epsilon (\nabla \times \mathbf E) = 0
\]
so
\begin{equation}
    \mathbf E = \frac{1}{4\pi\epsilon} \frac{q}{r^2} \uvec r
\end{equation}
That is, the polarization of the medium partially shield the free charge, reducing its electric field within the dielectric (by $\epsilon_r$).\\

Actually, it is not necessary for the dielectric to fill all space; in regions where the field is zero, it does not matter whether the dielectric is present or not, since there is no polarization in any event.

\framed{%
    \example{4.6}%
    A parallel-plate capacitor is filled with insulating material of dielectric constant $\epsilon_r$.
    What effect does this have on its capacitance?
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-example_6-01}
    \caption{Example 4.6 Parallel-plate capacitor.}%
\end{figure}
Since the field is confined to the space between the plates, according to Eq.\ (\ref{eq:ch04:FieldInHomogeneousDielectric}) the dielectric will reduce the field, and hence the potential difference $V$, by a factor $\frac{1}{\epsilon_r}$:
\[
    V = \frac{V_{\rm vac}}{\epsilon_r}
\]
Accordingly, the capacitance $C = Q/V$ is increased by a factor $\epsilon_r$,
\[
    C = \frac{Q}{V} = \frac{Q}{V_{\rm vac}} \epsilon_r = \epsilon_r C_{\rm vac}
\]

\homework{4.18, 4.20, 4.21}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Boundary Value Problems with Linear Dielectrics}
\begin{itemize}
    \item In a (homogeneous isotropic) linear dielectric, the bound charge density $\rho_b$ is proportional to the free charge density $\rho_f$
        \begin{equation}
            \rho_b = -\nabla \cdot \mathbf P
            = -\nabla \cdot \left( \frac{\chi_e}{\epsilon_r} \mathbf D \right)
            = -\frac{\chi_e}{\epsilon_r} (\nabla \cdot \mathbf D)
            = -\frac{\chi_e}{1 + \chi_e} \rho_f
        \end{equation}

    \item In particular, unless free charge is actually embedded in the material, the total charge $\rho = 0$ (since $\rho_f = 0$), and any net charge must reside at the surface.

    \item Within such a dielectric, then, the potential obeys Laplace's equation.
\end{itemize}

We can rewrite the boundary conditions Eq.\ (\ref{eq:ch04:BoundaryCondition:DPerp}) in terms of $\mathbf E$
\begin{equation}
    \epsilon_{\rm above} E^{\perp}_{\rm above} - \epsilon_{\rm below} E^{\perp}_{\rm below} = \sigma_f
\end{equation}
or, in terms of potential
\begin{equation}
    \epsilon_{\rm above} \frac{\partial V_{\rm above}}{\partial n}
    -
    \epsilon_{\rm below} \frac{\partial V_{\rm below}}{\partial n}
    =
    -\sigma_f
\end{equation}
(Note that $\epsilon$ are evaluated at appropriate locations.)
The potential itself is of course continuous
\begin{equation}
    V_{\rm above} = V_{\rm below}
\end{equation}

\framed{%
    \example{4.7}%
    A sphere of homogeneous linear dielectric material is placed in an otherwise uniform electric field $\mathbf E_0$.
    Find the electric field inside.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch04-example_7-01}
    \caption{Example 4.7 Field of a sphere of homogeneous linear dielectric.}%
\end{figure}
This is reminiscent of Ex.~3.8, in which an uncharged conducting sphere was introduced into a uniform field.

Our problem is to solve Laplace's equation, subject to the boundary conditions
\begin{enumerate}
    \item [(i)] $V_{\rm in} = V_{\rm out}$ at $r = R$
    \item [(ii)] $\displaystyle \epsilon\frac{\partial V_{\rm in}}{\partial r} = \epsilon_0\frac{\partial V_{\rm out}}{\partial r}$ at $r = R$
    \item [(iii)] $V_{\rm out} \rightarrow -E_0 r\cos\theta$ for $r \gg R$
\end{enumerate}
In spherical coordinates, the general solution of Laplace's equation is (Eq.~\ref{eq:ch03:LaplaceSolution:Spherical})
\[
    V(r, \theta)
    =
    \sum_{l = 0}^\infty
    \left( A_l r^l + \frac{B_l}{r^{l + 1}} \right)
    P_l(\cos\theta)
\]
Inside the sphere, $V$ is finite at $r = 0$, so
\[
    V_{\rm in}(r, \theta) = \sum_{l = 0}^\infty A_l r^l P_l(\cos\theta)
\]
Outside the sphere, condition (iii) yields $A_1 = -E_0$ with all other $A_l$'s are zero.
So,
\[
    V_{\rm out}(r, \theta)
    =
    -E_0 r \cos\theta + \sum_{l = 0}^\infty \frac{B_l}{r^{l + 1}} P_l(\cos\theta)
\]

These potentials in the two regions should be connected at the interface.
Condition (i) requires
\[
    \sum_{l = 0}^\infty A_l R^l P_l(\cos\theta)
    =
    -E_0 R \cos\theta + \sum_{l = 0}^\infty \frac{B_l}{R^{l + 1}} P_l(\cos\theta)
\]
so,
\begin{equation}
    \left\{
        \begin{array}{lcc}
            \displaystyle A_l R^l = \frac{B_l}{R^{l + 1}} & & \mathrm{for}\ l \ne 1 \\
            \\
            \displaystyle A_1 R = -E_0 R + \frac{B_1}{R^2}
        \end{array}
    \right.
    \label{eq:ch04:Example7:1}
\end{equation}
Condition (ii) yields
\[
    \epsilon_r \sum_{l = 0}^\infty l A_l R^{l - 1} P_l(\cos\theta)
    =
    -E_0 \cos\theta - \sum_{l = 0}^\infty \frac{(l + 1) B_l}{R^{l + 2}} P_l(\cos\theta)
\]
so,
\begin{equation}
    \left\{
        \begin{array}{lcc}
            \displaystyle \epsilon_r l A_l R^{l - 1} = -(l + 1) \frac{B_l}{R^{l + 2}} & & \mathrm{for}\ l \ne 1 \\
            \\
            \displaystyle \epsilon_r A_1 = -E_0 - 2\frac{B_1}{R^2}
        \end{array}
    \right.
    \label{eq:ch04:Example7:2}
\end{equation}
Solving Eqs.~(\ref{eq:ch04:Example7:1}) and~(\ref{eq:ch04:Example7:2}) together, we get
\[
     \left\{
        \begin{array}{lcl}
            \displaystyle A_l = B_l = 0 & & \mathrm{for}\ l \ne 1 \\
            \\
            \displaystyle A_1 = -\frac{3}{\epsilon_r + 2} E_0 & & \displaystyle B_1 = \frac{\epsilon_r - 1}{\epsilon_r + 2} R^3 E_0
        \end{array}
    \right.
\]

Evidently, the potential inside is
\[
    V_{\rm in}(r, \theta)
    =
    -\frac{3 E_0}{\epsilon_r + 2} r \cos\theta
    =
    -\frac{3 E_0}{\epsilon_r + 2} z
\]
Hence, the field inside the sphere is \textit{uniform}
\[
    \mathbf E_{\rm in} = \frac{3}{\epsilon_r + 2} \mathbf E_0
\]
Since $\epsilon_r \ge 1$, the field in the linear dielectric is shielded.
For a perfect conductor, $\epsilon_r \rightarrow \infty$, and thus the field inside vanishes (the same result as in Ex. 3.8).
If there were no dielectric material, then $\epsilon_r = 1$ and the field inside becomes the same as the external field.

Outside the sphere,
\[
    V_{\rm out}(r, \theta)
    =
    -E_0 r \cos\theta
    +\frac{\epsilon_r - 1}{\epsilon_r + 2} E_0 R^3 \frac{\cos\theta}{r^2}
\]
The first term is due to the external field, and the second is the contribution of the polarized dielectric material (which is that of a perfect dipole).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Energy in Dielectric Systems}
\begin{itemize}
    \item Remember that it took work to charge up a capacitor
        \[
            W = \frac{1}{2} C V^2
        \]

    \item If the capacitor is filled with linear dielectric, its capacitance exceeds the vacuum value by a factor of the dielectric constant (Ex. 4.6)
        \[
            C = \epsilon_r C_{\rm vac}
        \]

    \item Evidently, the work necessary to charge a dielectric-filled capacitor is increased by the same factor.
        \[
            W = \epsilon_r W_{\rm vac}
        \]

    \item The reason is clear: You have to pump up more free charge, to achieve a given potential, because part of the field is cancelled by the bound charges.
\end{itemize}

In Chapter 2, the general formula for energy is
\[
    W = \int \frac{\epsilon_0}{2} E^2 d\tau
\]
The case with the dielectric-filled capacitor suggests that this should be changed to
\begin{equation}
    W
    =
    \underbrace{\epsilon_r \int \frac{\epsilon_0}{2} E^2 d\tau}_{\color{blue}(1)}
    =
    \frac{1}{2} \int (\mathbf E \cdot \mathbf D) d\tau
\end{equation}
in the presence of linear dielectrics.
(Technically, (1) is not correct if the material is inhomogeneous.
This is just to illustrate the point that the energy density is given by $\mathbf E \cdot \mathbf D$ which is exact.)
