\section{The Special Theory of Relativity}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Einstein's Postulates}
\begin{enumerate}
    \item \textbf{The principle of relativity}:
        The laws of physics apply in all inertial reference systems.

    \item \textbf{The universal speed of light}:
        The speed of light in vacuum is the same for all inertial observers,
        regardless of the motion of the source.
\end{enumerate}

\framed{%
    The special theory of relativity is not limited to any particular class of phenomena---rather, it is a description of the spacetime ``arena'' in which all physical phenomena take place.
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Geometry of Relativity}

\subsubsection{(i) The relativity of simultaneity}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-relativity_of_simultaneity-01}
    \caption{Relativity of simultaneity.}%
\end{figure}
\framed{%
    Two events that are simultaneous in on inertial system are not, in general, simultaneous in another.
}

\subsubsection{(ii) Time dilation}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-time_dilation-01}
    \caption{Time dilation.}%
\end{figure}
\framed{%
    Moving clocks run slow.
}
\begin{equation}
    \Delta\bar t
    =
    \sqrt{1 - v^2/c^2} \Delta t
    =
    \frac{\Delta t}{\gamma}
\end{equation}
where $\gamma$ is the relativistic factor
\[
    \gamma
    \equiv
    \frac{1}{\sqrt{1 - v^2/c^2}}
\]

\subsubsection{(iii) Lorentz contraction}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-lorentz_contraction-01}
    \caption{Lorentz contraction.}%
    \label{fig:ch12:LorentzContraction}
\end{figure}
\framed{%
    Moving objects are shortened.
}
\begin{enumerate}
    \item Round trip time on train (Fig.~\ref{fig:ch12:LorentzContraction}a):
        \begin{equation}
            \Delta\bar t
            =
            2 \frac{\Delta\bar x}{c}
        \end{equation}
        where $\Delta\bar x$ is the train length measured in the train rest frame.

    \item Round trip time measured on ground (Fig.~\ref{fig:ch12:LorentzContraction}b):
        \begin{equation}
            \begin{aligned}
                \Delta t_1
                &=
                \frac{\Delta x + v \Delta t_1}{c}
                \quad\rightarrow\quad
                \Delta t_1
                =
                \frac{\Delta x}{c - v}
                \\
                \Delta t_2
                &=
                \frac{\Delta x - v \Delta t_2}{c}
                \quad\rightarrow\quad
                \Delta t_2
                =
                \frac{\Delta x}{c + v}
                \\
                \therefore\,
                \Delta t
                &=
                \Delta t_1 + \Delta t_2
                =
                2\frac{\Delta x}{c}
                \frac{1}{1 - v^2/c^2}
                =
                2 \frac{\Delta x}{c} \gamma^2
            \end{aligned}
        \end{equation}

    \item Since $\Delta t = \gamma \Delta\bar t = 2(\Delta\bar x/c) \gamma$, it follows
        \begin{equation}
            \Delta\bar x
            =
            \gamma \Delta x
            =
            \frac{1}{\sqrt{1 - v^2/c^2}}
            \Delta x
        \end{equation}
\end{enumerate}

\textit{``A moving object is shortened only along the direction of its motion.''}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Lorentz Transformations}
\textbf{Event}:
Something that takes place at a specific location $(x, y, z)$, at precise time $(t)$.

\framed{%
    \textbi{Task}:
    We know the coordinate $(x, y, z, t)$ of a particular event $E$ in one inertial system $\mathcal S$,
    and we would like to calculate coordinates $(\bar x, \bar y, \bar z, \bar t)$ of that same event in some other inertial system $\bar{\mathcal S}$.
}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-lorentz_transformation-01}
    \caption{Lorentz transformations.}%
    \label{fig:ch12:LorentzTransformation}
\end{figure}
Suppose $\bar{\mathcal S}$ is sliding along the $x$ axis at velocity $v$ (Fig.~\ref{fig:ch12:LorentzTransformation}).
From the time dilation,
\begin{equation}
    \bar t
    =
    \frac{t}{\gamma}
    =
    \gamma t
    (1 - v^2/c^2)
    =
    \gamma \left( t - \frac{v}{c^2} vt \right)
    =
    \gamma \left( t - \frac{v}{c^2} x \right)
\end{equation}
Meanwhile, the location of $E$ in the $x$ axis is
\[
    x = d + vt
\]
But, $d$ is the Lorentz-contracted coordinate in $\bar{\mathcal S}$
\[
    d = \frac{1}{\gamma} \bar x
\]
So, substituting and rearranging it,
\begin{equation}
    \bar x
    =
    \gamma (x - vt)
\end{equation}
The Lorentz transformations then read
\begin{equation}
    \boxed{%
        \begin{array}{cl}
            \text{(i)}
            & \displaystyle
            \bar x = \gamma (x - vt)
            \\\\
            \text{(ii)}
            & \displaystyle
            \bar y = y
            \\\\
            \text{(iii)}
            & \displaystyle
            \bar z = z
            \\\\
            \text{(iv)}
            & \displaystyle
            \bar t = \gamma \left( t - \frac{v}{c^2} x \right)
        \end{array}
    }
    \quad\leftrightarrow\quad
    \boxed{%
        \begin{array}{rl}
            \text{(i')}
            & \displaystyle
            x = \gamma (\bar x + v\bar t)
            \\\\
            \text{(ii')}
            & \displaystyle
            y = \bar y
            \\\\
            \text{(iii')}
            & \displaystyle
            z = \bar z
            \\\\
            \text{(iv')}
            & \displaystyle
            t = \gamma \left( \bar t + \frac{v}{c^2} \bar x \right)
        \end{array}
    }
\end{equation}

\framed{%
    \example{12.6}%
    \textbf{Einstein's velocity addition rule}.
}
\solution%
Suppose a particle moves a distance $dx$ (in $\mathcal S$) in a time $dt$.
Its velocity $u$ is then
\[
    u = \frac{dx}{dt}
\]
In $\bar{\mathcal S}$, it has moved a distance
\[
    d\bar x
    =
    \gamma (dx - v dt)
\]
in a time
\[
    d\bar t
    =
    \gamma \left( dt - \frac{v}{c^2} dx \right)
\]
The velocity in $\bar{\mathcal S}$ is therefore
\begin{equation}
    \bar u
    =
    \frac{d\bar x}{d\bar t}
    =
    \frac{\gamma (dx - v dt)}{\gamma (dt - (v/c^2) dx)}
    =
    \frac{(dx/dt - v)}{1 - (v/c^2) (dx/dt)}
    =
    \frac{u - v}{1 - uv/c^2}
\end{equation}

\separator%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Structure of Spacetime}

\subsubsection{(i) Four vectors}
We can simplify notations by defining
\begin{equation}
    \beta = \frac{v}{c}
    ,\quad
    x^0 = ct
    ,\quad
    x^1 = x
    ,\quad
    x^2 = y
    ,\quad
    x^3 = z
\end{equation}
Then, the Lorentz transformations read
\begin{equation}
    \begin{aligned}
        \bar x^0
        &=
        \gamma (x^0 - \beta x^1)
        \\
        \bar x^1
        &=
        \gamma (x^1 - \beta x^0)
        \\
        \bar x^2
        &=
        x^2
        \\
        \bar x^3
        &=
        x^3
    \end{aligned}
\end{equation}
Or, in matrix form,
\begin{equation}
    \left(
        \begin{matrix}
            \bar x^0
            \\
            \bar x^1
            \\
            \bar x^2
            \\
            \bar x^3
        \end{matrix}
    \right)
    =
    \underbrace{%
        \left(
            \begin{matrix}
                \gamma & -\gamma\beta & 0 & 0 \\
                -\gamma\beta & \gamma & 0 & 0 \\
                0            &   0    & 1 & 0 \\
                0            &   0    & 0 & 1 \\
            \end{matrix}
        \right)
    }_{\color{blue}\Lambda}
    \left(
        \begin{matrix}
            x^0
            \\
            x^1
            \\
            x^2
            \\
            x^3
        \end{matrix}
    \right)
\end{equation}
Introducing $\Lambda$, the Lorentz transformation matrix, the transformations can be simplified to
\begin{equation}
    \boxed{%
        \bar x^\mu
        =
        \sum_{\nu = 0}^3
        {\Lambda^\mu}_\nu
        x^\nu
    }
\end{equation}
Lorentz transformation is a spacetime version of rotation:
\begin{equation}
    \Lambda
    =
    \left(
        \begin{matrix}
            \gamma & -\gamma\beta & 0 & 0 \\
            -\gamma\beta & \gamma & 0 & 0 \\
            0            &   0    & 1 & 0 \\
            0            &   0    & 0 & 1 \\
        \end{matrix}
    \right)
    =
    \left(
        \begin{matrix}
            \cosh\zeta  & -\sinh\zeta & 0 & 0 \\
            -\sinh\zeta &  \cosh\zeta & 0 & 0 \\
            0           &        0    & 1 & 0 \\
            0           &        0    & 0 & 1 \\
        \end{matrix}
    \right)
\end{equation}
where $\zeta$ is called the \textbf{rapidity}
\begin{equation}
    \beta = \tanh\zeta
    ,\quad
    \gamma = \cosh\zeta
    ,\quad
    \beta\gamma = \sinh\zeta
\end{equation}

Note that $(x^0, x^1, x^2, x^3)$ is a displacement 4-vector.
By extension, we now define a 4-vector as any set of four components that transform in the same manner:
\begin{equation}
    \bar a^\mu
    =
    \sum_{\nu = 0}^3
    {\Lambda^\mu}_\nu
    a^\nu
\end{equation}
For the particular case of a transformation along the $x$ axis
\begin{equation}
    \begin{aligned}
        \bar a^0
        &=
        \gamma (a^0 - \beta a^1)
        \\
        \bar a^1
        &=
        \gamma (a^1 - \beta a^0)
        \\
        \bar a^2
        &=
        a^2
        \\
        \bar a^3
        &=
        a^3
    \end{aligned}
\end{equation}

\textbf{Four-dimensional scalar product}:
\begin{equation}
    {\color{blue}-}
    \bar a^0 \bar b^0
    +
    \bar a^1 \bar b^1
    +
    \bar a^2 \bar b^2
    +
    \bar a^3 \bar b^3
    =
    {\color{blue}-}
    a^0 b^0
    +
    a^1 b^1
    +
    a^2 b^2
    +
    a^3 b^3
\end{equation}
This is \textit{invariant} under Lorentz transformations.
\\

We introduce \textit{contravariant} and \textit{covariant} vectors:
\framed{%
    \begin{equation}
        \begin{array}{rl}
            \text{contravariant vector}:
            &
            a^\mu
            \\\\
            \text{covariant vector}:
            &
            a_\mu
        \end{array}
    \end{equation}
}
They differ only in the sign of the zeroth component:
\begin{equation}
    (a_0, a_1, a_2, a_3)
    =
    (-a^0, a^1, a^2, a^3)
\end{equation}
That is, raising or lowering the temporal index costs a minus sign $(a_0 = -a^0)$;
raising or lowering a spatial index changes nothing $(a_j = a^j)$.
\\

Formally, one can use the metric tensor to raise or lower indices:
\[
    a_\mu
    =
    \sum_{\nu = 0}^3
    g_{\mu\nu} a^\nu
    ,\quad\text{where}\quad
    g_{\mu\nu}
    \equiv
    \left(
        \begin{matrix}
            -1 & 0 & 0 & 0 \\
            0  & 1 & 0 & 0 \\
            0  & 0 & 1 & 0 \\
            0  & 0 & 0 & 1 \\
        \end{matrix}
    \right)
\]
is the \textbf{Minkowski metric}.
\\

The scalar product can now be written as
\begin{equation}
    \sum_{\mu = 0}^3
    a^\mu b_\mu
    =
    a^\mu b_\mu
\end{equation}
where the summation is implied in the second form by \textbf{Einstein summation convention}.

\subsubsection{(ii) The invariant interval}
The scalar product of a 4-vector with itself
\begin{equation}
    a^\mu a_\mu
    =
    -(a^0)^2
    +(a^1)^2
    +(a^2)^2
    +(a^3)^2
\end{equation}
can be positive, negative, or zero:
\[
    \begin{array}{rl}
        \text{If}\ a^\mu a_\mu > 0,
        &
        a^\mu\ \text{is called \textbf{spacelike}}.
        \\\\
        \text{If}\ a^\mu a_\mu < 0,
        &
        a^\mu\ \text{is called \textbf{timelike}}.
        \\\\
        \text{If}\ a^\mu a_\mu = 0,
        &
        a^\mu\ \text{is called \textbf{lightlike}}.
    \end{array}
\]

Suppose event $A$ occurs at $(x_A^0, x_A^1, x_A^2, x_A^3)$ and event $B$ at $(x_B^0, x_B^1, x_B^2, x_B^3)$.
The difference,
\begin{equation}
    \Delta x^\mu
    \equiv
    x_A^\mu - x_B^\mu
\end{equation}
is the displacement 4-vector.

The scalar product of $\Delta x^\mu$ with itself is called the \textbf{invariant interval} between two events:
\begin{equation}
    \begin{aligned}
        I
        \equiv
        \Delta x^\mu \Delta x_\mu
        &=
        -(\Delta x^0)^2
        +(\Delta x^1)^2
        +(\Delta x^2)^2
        +(\Delta x^3)^2
        \\
        &=
        -(ct)^2 + d^2
    \end{aligned}
\end{equation}
where $t$ is the time difference between the two events and $d$ is their spatial separation.
\framed{%
    $t$ and $d$ are altered under Lorentz transformations, but the interval $I$ remains the same.
}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-invariant_interval-01}
    \caption{Invariant interval.}%
    \label{fig:ch12:InvariantInterval}
\end{figure}
\begin{itemize}
    \item If the displacement between two events is timelike $(I < 0)$, there exists an inertial system in which they occur at the same location (Fig.~\ref{fig:ch12:InvariantInterval}a).

    \item If the displacement between two events is spacelike $(I > 0)$, there exists a system in which the two events occur at the same time (Fig.~\ref{fig:ch12:InvariantInterval}b).

    \item And if the displacement is lightlike $(I = 0)$, then the two events could be connected by a light signal.
\end{itemize}

\homework{12.20, 12.21}

\subsubsection{(iii) Spacetime diagrams}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch12-minkowski_diagram-01}
    \caption{Minkowski diagrams.}%
    \label{fig:ch12:MinkowskiDiagram}
\end{figure}
\textbf{Minkowski diagrams}:
A particle at rest is represented by a vertical line;
a photon, traveling at the speed of light, is described by a $45^\circ$ line;
and a rocket going at some intermediate speed follows a line of slope $c/v = 1/\beta$.

\textbf{World line}:
The trajectory of a particle on a Minkowski diagram.
\\

The displacement between causally related events is always timelike, and their temporal ordering is the same for all inertial observers.
