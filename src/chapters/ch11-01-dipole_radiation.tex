\section{Dipole Radiation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{What Is Radiation?}
\framed{%
    When charges accelerate, their fields can transport energy \textit{irreversibly} (that is, the energy do not come back) out to infinity as a form of \textbf{radiation}.
}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch11-radiation_measure-01}
    \caption{Measuring radiation power out at a large distance.}%
\end{figure}
\begin{itemize}
    \item Suppose the source of radiation is localized near the origin.

    \item We want to calculate the energy it is radiating at time $t_0$.
        That is, we want to calculate the radiating power of the source, $P_{\rm rad}(t_0)$.

    \item We are measuring power out at a large distance, $r$ (for example, imagine measuring Sun's energy at Earth's orbit).
        The power passing through the surface of sphere of radius $r$ is
        \begin{equation}
            P(r, t)
            =
            \oint \mathbf S \cdot d\mathbf a
            =
            \frac{1}{\mu_0} \oint (\mathbf E \times \mathbf B) \cdot d\mathbf a
        \end{equation}

    \item Obviously, $P(r, t)$ is related to $P_{\rm rad}(t_0)$:
        \begin{equation}
            P_{\rm rad}(t_0) = \lim_{r \rightarrow \infty} P(r, \underbrace{t_0 + r/c}_{\color{blue}(\dagger)})
        \end{equation}
        taking into account the travel time $(\dagger)$ of the electromagnetic signal.

    \item The area of the sphere is $4\pi r^2$, so for radiation to occur the Poynting vector must decrease (at large $r$) no faster than $1/r^2$.

    \item \textit{Static} sources do not radiate: Electrostatic and magnetostatic fields fall off like $1/r^2$ (at best), which means that $S \sim 1/r^4$.

    \item But Jefimenko's equations show that the time-dependent fields (the acceleration fields) include terms that go like $1/r$, which are responsible for electromagnetic radiation.

    \item The study of radiation then involves picking out parts of $\mathbf E$ and $\mathbf B$ that go like $1/r$ at large distances from the source.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Electric Dipole Radiation}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch11-electric_dipole_radiation-01}
    \caption{Electric dipole configuration.}%
\end{figure}

\subsubsection{Potentials}
\begin{itemize}
    \item Picture two metal spheres a distance $d$ apart.

    \item In each sphere, there is variable charge $q(t)$
        \begin{equation}
            q(t) = q_0 \cos(\omega t)
        \end{equation}
        by moving charge through a thin wire connecting two spheres.

    \item Dipole moment
        \begin{equation}
            \mathbf p(t) = q(t) d \uvec z
            = \underbrace{q_0 d}_{\color{blue} p_0} \cos(\omega t) \uvec z
            =
            p_0 \cos(\omega t) \uvec z
        \end{equation}

    \item Retarded scalar potential at $(\mathbf r, t)$
        \begin{equation}
            \begin{aligned}
                V(\mathbf r, t)
                &=
                \frac{1}{4\pi\epsilon_0} \left(
                    \frac{+q(t_r^+)}{\eta_+}
                    +
                    \frac{-q(t_r^-)}{\eta_-}
                \right)
                \\
                &=
                \frac{q_0}{4\pi\epsilon_0} \left(
                    \frac{\cos\left( \omega(t - \eta_+/c) \right)}{\eta_+}
                    -
                    \frac{\cos\left( \omega(t - \eta_-/c) \right)}{\eta_-}
                \right)
            \end{aligned}
        \end{equation}
        where $t_r^\pm = t - \eta_\pm/c$.

    \item Law of cosine
        \begin{equation}
            \eta_\pm = \sqrt{r^2 \mp r d \cos\theta + (d/2)^2}
        \end{equation}

    \item \textbf{Approximation 1:} $d \ll r$ (source is far away), then
        \begin{equation}
            \text{(1)}\quad
            \eta_\pm \simeq r \left( 1 \mp \frac{d}{2r} \cos\theta \right)
            \qquad
            \because \sqrt{1 + \epsilon} \simeq 1 + \frac{\epsilon}{2} \quad\text{for}\ \epsilon \ll 1
        \end{equation}
        \begin{equation}
            \text{(2)}\quad
            \frac{1}{\eta_\pm} \simeq \frac{1}{r} \left( 1 \pm \frac{d}{2r} \cos\theta \right)
            \qquad
            \because \frac{1}{\sqrt{1 + \epsilon}} \simeq 1 - \frac{\epsilon}{2} \quad\text{for}\ \epsilon \ll 1
        \end{equation}
        \begin{equation}
            \begin{aligned}
                \text{(3)}\quad
                &\cos\left( \omega\left( t - \frac{\eta_\pm}{c} \right) \right)
                \simeq
                \cos\left( \omega\left( t - \frac{r}{c} \right) \pm \frac{\omega d}{2c} \cos\theta \right)
                \\
                &\ =
                \cos\left( \omega\left( t - \frac{r}{c} \right) \right) \cos\left( \frac{\omega d}{2c}\cos\theta \right)
                \mp
                \sin\left( \omega\left( t - \frac{r}{c} \right) \right) \sin\left( \frac{\omega d}{2c}\cos\theta \right)
            \end{aligned}
        \end{equation}

    \item \textbf{Approximation 2:} $d \ll c/\omega {\color{blue}\ = 1/k = \lambda/(2\pi)\ \rightarrow\ \therefore d \ll \lambda}$ (equivalent to $c \gg \omega d = v$)\\
        (This means that the wavelength is larger than the distance of the dipole.
        One way to think about it is that the movement of the charge ($v$) between two spheres cannot exceed $c$.)

        Then,
        \begin{equation}
            \cos\left( \frac{\omega d}{2c} \cos\theta \right) \simeq 1
            ,\quad
            \sin\left( \frac{\omega d}{2c} \cos\theta \right) \simeq \frac{\omega d}{2c} \cos\theta
        \end{equation}
        so
        \begin{equation}
            \cos\left( \omega\left( t - \frac{\eta_\pm}{c} \right) \right)
            \simeq
            \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
            \mp
            \frac{\omega d}{2c} \cos\theta \sin\left( \omega\left( t - \frac{r}{c} \right) \right)
        \end{equation}

    \item Substituting them into the retarded potential,
        \begin{equation}
            \begin{aligned}
                V(r, \theta, t)
                &\simeq
                \frac{q_0}{4\pi\epsilon_0}
                \left[
                    \frac{1}{r} \left( 1 + \frac{d}{2r}\cos\theta \right)
                    \left\{
                        \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
                        \phantom{\frac{\omega d}{2c}}
                    \right.
                \right.
                \\
                &\qquad\qquad\qquad\qquad\qquad\qquad -
                \left.
                    \frac{\omega d}{2c}\cos\theta\sin\left( \omega\left( t - \frac{r}{c} \right) \right)
                \right\}
                \\
                &\quad\quad\quad -
                \left.
                    \frac{1}{r} \left( 1 - \frac{d}{2r}\cos\theta \right)
                    \left\{
                        \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
                        \phantom{\frac{\omega d}{2c}}
                    \right.
                \right.
                \\
                &\qquad\qquad\qquad\qquad\qquad\qquad +
                \left.
                    \left.
                        \frac{\omega d}{2c}\cos\theta\sin\left( \omega\left( t - \frac{r}{c} \right) \right)
                    \right\}
                \right]
                \\
                &=
                \frac{q_0}{4\pi\epsilon_0}
                \left[
                    \frac{d}{r^2}\cos\theta\cos\left( \omega\left( t - \frac{r}{c} \right) \right)
                    -
                    \frac{1}{r}\frac{\omega d}{c}\cos\theta\sin\left( \omega\left( t - \frac{r}{c} \right) \right)
                \right]
                \\
                &=
                \frac{p_0\cos\theta}{4\pi\epsilon_0 r}
                \left[
                    -\frac{\omega}{c}\sin\left( \omega\left( t - \frac{r}{c} \right) \right)
                    +
                    \frac{1}{r}\cos\left( \omega\left( t - \frac{r}{c} \right) \right)
                \right]
            \end{aligned}
        \end{equation}
        In the static limit ($\omega \rightarrow 0$),
        \begin{equation}
            V = \frac{p_0 \cos\theta}{4\pi\epsilon_0 r^2}
        \end{equation}
        which is the potential by a perfect dipole.

    \item \textbf{Approximation 3:} $r \gg c/\omega {\color{blue}\ = 1/k = \lambda/(2\pi)}$
        (Distance so large that there are multiple wavelengths between the source and the field point.)
        \begin{equation}
            \begin{aligned}
                V(r, \theta, t)
                &\simeq
                -\frac{p_0\cos\theta}{4\pi\epsilon_0 r}
                \frac{\omega}{c} \left[
                    \sin\left( \omega\left( t - \frac{r}{c} \right) \right)
                    -
                    \cancelto{\color{blue}0}{\frac{c}{\omega r}}
                    \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
                \right]
                \\
                &\simeq
                -\frac{p_0 \omega}{4\pi\epsilon_0 c}
                \left( \frac{\cos\theta}{r} \right)
                \sin\left( \omega\left( t - \frac{r}{c} \right) \right)
            \end{aligned}
        \end{equation}
\end{itemize}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch11-electric_dipole_radiation-02}
    \caption{Electric dipole configuration.}%
\end{figure}
\begin{itemize}
    \item Current through the wire between two spheres
        \begin{equation}
            \mathbf I(t) = \frac{dq}{dt} \uvec z
            = -q_0 \omega \sin(\omega t) \uvec z
        \end{equation}

    \item Retarded vector potential
        \begin{equation}
            \mathbf A(\mathbf r, t)
            =
            \frac{\mu_0}{4\pi} \int_{-d/2}^{d/2}
            \frac{-q_0 \omega \sin\left( \omega\left( t - \frac{\eta}{c} \right) \right) \uvec z}{\eta}
            dz
        \end{equation}
        As before, we expand $\eta$ and $1/\eta$ and take the leading order term
        \begin{equation}
            \left\{
                \begin{aligned}
                    \frac{1}{\eta}
                    &=
                    \frac{1}{r} \left( 1 - \frac{z}{2r}\cos\theta \right)
                    \simeq
                    \frac{1}{r}
                    \\
                    \eta
                    &=
                    r \left( 1 + \frac{z}{2r}\cos\theta \right)
                    \simeq
                    r
                \end{aligned}
            \right.
        \end{equation}
        where we ignored the $z/r$ term on the ground of approximation 1.
        Substituting the, $\mathbf A$ simplifies to
        \begin{equation}
            \begin{aligned}
                \mathbf A(r, \theta, t)
                &\simeq
                \frac{\mu_0 q_0 \omega}{4\pi r}
                \int_{-d/2}^{d/2} \sin\left( \omega\left( t - \frac{r}{c} \right) \right) \uvec z
                dz
                \\
                &=
                -\frac{\mu_0 p_0 \omega}{4\pi r}
                \sin\left( \omega\left( t - \frac{r}{c} \right) \right) \uvec z
                \\
                &=
                -\frac{\mu_0 p_0 \omega}{4\pi r}
                \sin\left( \omega\left( t - \frac{r}{c} \right) \right)
                \left( \cos\theta\uvec r - \sin\theta\uvec\theta \right)
            \end{aligned}
        \end{equation}
\end{itemize}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch11-z_dir_in_polar_unit_vectors-01}
    \caption{Unit vector $\uvec z$ in terms of the unit vectors of polar coordinates.}%
\end{figure}

\subsubsection{Fields}
Using the potentials, we now calculate the fields
\[
    \mathbf E = -\nabla V - \frac{\partial\mathbf A}{\partial t}
    ,\quad
    \mathbf B = \nabla \times \mathbf A
\]
(1) $\nabla V$:
\begin{equation}
    \begin{aligned}
        \nabla V(r, \theta, t)
        &=
        \frac{\partial V}{\partial r}\uvec r
        +
        \frac{1}{r}\frac{\partial V}{\partial\theta} \uvec\theta
        \\
        &=
        -\frac{p_0 \omega}{4\pi\epsilon_0 c} \left[
            -\frac{\cos\theta}{r^2}\sin\left( \omega\left( t - \frac{r}{c} \right) \right)
            -\frac{\cos\theta}{r}\frac{\omega}{c}\cos\left( \omega\left( t - \frac{r}{c} \right) \right)
        \right] \uvec r
        \\
        &\quad
        -\frac{p_0\omega}{4\pi\epsilon_0 c} \left[
            -\frac{\sin\theta}{r^2}\sin\left( \omega\left( t - \frac{r}{c} \right) \right)
        \right] \uvec\theta
        \\
        &\simeq
        \frac{p_0 \omega^2}{4\pi\epsilon_0 c^2}
        \left( \frac{\cos\theta}{r} \right)
        \cos\left( \omega\left( t - \frac{r}{c} \right) \right) \uvec r
    \end{aligned}
\end{equation}
(2) $\partial\mathbf A/\partial t$:
\begin{equation}
    \begin{aligned}
        \frac{\partial\mathbf A}{\partial t}
        &=
        -\frac{\mu_0 p_0 \omega^2}{4\pi r}
        \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
        \left( \cos\theta\uvec r - \sin\theta\uvec\theta \right)
        \\
        &\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad
        {\color{blue}\leftarrow \mu_0\epsilon_0 = 1/c^2}
        \\
        &=
        -\frac{p_0 \omega^2}{4\pi\epsilon_0 c^2}
        \left[
            \left( \frac{\cos\theta}{r} \right) \cos\left( \omega\left( t - \frac{r}{c} \right) \right) \uvec r
            -
            \left( \frac{\sin\theta}{r} \right) \cos\left( \omega\left( t - \frac{r}{c} \right) \right) \uvec\theta
        \right]
    \end{aligned}
\end{equation}
Therefore, the electric field is
\[
    \mathbf E
    =
    -\frac{p_0 \omega^2}{4\pi\epsilon_0 c^2}
    \left( \frac{\sin\theta}{r} \right)
    \cos\left( \omega\left( t - \frac{r}{c} \right) \right) \uvec\theta
\]
The magnetic field is given by
\[
    \begin{aligned}
        \mathbf B
        &=
        \nabla \times \mathbf A
        \\
        &=
        \frac{1}{r} \left(
            \frac{\partial}{\partial r}\left( r A_\theta \right)
            -
            \frac{\partial A_r}{\partial\theta}
        \right) \uvec\phi
        \\
        &=
        -\frac{\mu_0 p_0 \omega}{4\pi r}
        \left[
            \frac{\omega}{c}\sin\theta\cos\left( \omega\left( t - \frac{r}{c} \right) \right)
            +
            \cancelto{\color{blue}\propto 1/r^2}{\frac{\sin\theta}{r}}\sin\left( \omega\left( t - \frac{r}{c} \right) \right)
        \right] \uvec\phi
        \\
        &\simeq
        -\frac{\mu_0 p_0 \omega^2}{4\pi c}
        \left( \frac{\sin\theta}{r} \right)
        \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
        \uvec\phi
    \end{aligned}
\]
where we ignored the $1/r^2$ term on the ground that this will not contribute to radiation.

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch11-spherical_wave_of_radiation-01}
    \caption{Radiation of electromagnetic fields.}%
\end{figure}
In summary, the radiating fields are
\begin{equation}
    \boxed{%
        \mathbf E
        =
        -\frac{p_0 \omega^2}{4\pi\epsilon_0 c^2}
        \left( \frac{\sin\theta}{r} \right)
        \cos\underbrace{\left( \omega\left( t - \frac{r}{c} \right) \right)}_{\color{blue}kr - \omega t}
        \uvec\theta
    }
\end{equation}
and
\begin{equation}
    \boxed{%
        \mathbf B
        =
        -\frac{\mu_0 p_0 \omega^2}{4\pi c}
        \left( \frac{\sin\theta}{r} \right)
        \cos\underbrace{\left( \omega\left( t - \frac{r}{c} \right) \right)}_{\color{blue}kr - \omega t}
        \uvec\phi
    }
\end{equation}
These equations describe a spherical wave of frequency $\omega$ traveling in the radial direction
\begin{equation}
    \uvec k = \uvec r = \uvec\theta \times \uvec\phi
\end{equation}
at the speed of light $c$.
These satisfy electromagnetic waves in free
\begin{equation}
    \frac{E}{B}
    =
    \left. \frac{1}{\epsilon_0 c^2} \middle/ \frac{\mu_0}{c} \right.
    =
    c
\end{equation}
At distances very far from the source, the wave front will become planar at which point the wave can be regarded as a plane wave.
The field strength goes like $1/r$.

\subsubsection{Radiation Power}
\begin{itemize}
    \item The energy radiated by an oscillating electric dipole
        \begin{equation}
            \mathbf S(\mathbf r, t)
            =
            \frac{1}{\mu_0}\left( \mathbf E \times \mathbf B \right)
            =
            \frac{\mu_0}{c} \left[
                \frac{p_0\omega^2}{4\pi}
                \left( \frac{\sin\theta}{r} \right)
                \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
            \right]^2 \uvec r
        \end{equation}

    \item Intensity
        \begin{equation}
            \langle \mathbf S \rangle
            =
            \frac{\mu_0 p_0^2 \omega^4}{32 \pi^2 c}
            \left( \frac{\sin\theta}{r} \right)^2
            \uvec r
        \end{equation}
        This tells us that there is no radiation along the $z$ axis (i.e., dipole axis; $\theta = 0$).

    \item Total radiation power
        \begin{equation}
            \langle P \rangle
            =
            \int \langle \mathbf S \rangle \cdot d\mathbf a
            =
            \frac{\mu_0 p_0^2 \omega^4}{32\pi^2 c}
            \underbrace{\int \frac{\sin^2\theta}{\cancel{r^2}} \cancel{r^2} \sin\theta d\theta d\phi}_{\color{blue}\leftarrow \int_0^\pi \sin^3\theta d\theta = 4/3}
            =
            \frac{\mu_0 p_0^2 \omega^4}{12\pi c}
        \end{equation}
\end{itemize}

\framed{%
    \example{11.1}%
    Blue sky.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch11-example_1-01}
    \caption{Example 11.1 Rayleigh scattering.}%
\end{figure}
\begin{itemize}
    \item Stronger absorption of blue light due to $\langle P \rangle \propto \omega^4$.
    \item The scattered light from blue sky is polarized in the direction perpendicular to the line of sight and to the Sun's ray.
\end{itemize}

\homework{11.2, 11.3, 11.4}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Magnetic Dipole Radiation}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch11-magnetic_dipole_configuration-01}
    \caption{Magnetic dipole configuration.}%
\end{figure}

\subsubsection{Potentials}
Suppose a current loop of radius $b$, with time-dependent current flow:
\begin{equation}
    I(t) = I_0 \cos(\omega t)
\end{equation}
This leads to an oscillating magnetic dipole
\begin{equation}
    \mathbf m = \pi b^2 I \uvec z = m_0 \cos(\omega t) \uvec z
\end{equation}
with $m_0 \equiv \pi b^2 I_0$.
The scalar potential is zero (no net charge):
\begin{equation}
    V = 0
\end{equation}
The retarded vector potential ($\mathbf A = \frac{\mu_0}{4\pi} \int \frac{I}{\eta} d\mathbf l'$)
\begin{equation}
    \begin{aligned}
        \mathbf A(\mathbf r, t)
        &=
        \frac{\mu_0}{4\pi} \int
        \frac{I_0 \cos\left( \omega\left( t - \frac{\eta}{c} \right) \right)}{\eta}
        d\mathbf l'
        \\
        &\qquad\qquad\qquad\qquad\qquad\qquad
        {\color{blue}\leftarrow d\mathbf l' = b d\phi' (-\sin\phi'\uvec x +\cos\phi'\uvec y)}
        \\
        &=
        \frac{\mu_0 I_0 b}{4\pi}
        \int_{-\pi}^\pi d\phi'
        \frac{\cos\left( \omega\left( t - \frac{\eta}{c} \right) \right)}{\eta}
        (-\sin\phi'\uvec x +\cos\phi'\uvec y)
    \end{aligned}
\end{equation}
Suppose $\mathbf r = x\uvec x + z\uvec z$.
Since $\mathbf b = b (\cos\phi'\uvec x + \sin\phi'\uvec y)$,
\[
    \eta^2 = r^2 + b^2 - 2 \mathbf r \cdot \mathbf b
    = r^2 + b^2 - 2 x b \cos\phi'
\]
Since $\eta$ is an even function of $\phi'$, only the $y$ component survives after integration:
\begin{equation}
    \mathbf A(\mathbf r, t)
    =
    \frac{\mu_0 I_0 b}{4\pi} \uvec y \int_{-\pi}^\pi
    \frac{\cos\left( \omega\left( t - \frac{\eta}{c} \right) \right)}{\eta}
    \cos\phi' d\phi'
\end{equation}
For an arbitrary position $\mathbf r$, $\uvec y$ can be replaced with $\uvec\phi$ (that is, the vector potential points toward in the azimuthal direction $\uvec\phi$).
\\

In spherical coordinates,
\[
    \eta = \sqrt{r^2 + b^2 - 2rb \sin\theta\cos\phi'}
\]
\begin{itemize}
    \item \textbf{Approximation 1:} $b \ll r$
        \[
            \begin{aligned}
                \eta
                &\simeq r \left( 1 - \frac{b}{r} \sin\theta\cos\phi' \right)
                \\
                \frac{1}{\eta}
                &\simeq
                \frac{1}{r} \left( 1 + \frac{b}{r} \sin\theta\cos\phi' \right)
            \end{aligned}
        \]
        Using this,
        \[
            \begin{aligned}
                \cos\left( \omega\left( t - \frac{\eta}{c} \right) \right)
                &\simeq
                \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
                \cos\left( \frac{\omega b}{c} \sin\theta\cos\phi' \right)
                \\
                &-
                \sin\left( \omega\left( t - \frac{r}{c} \right) \right)
                \sin\left( \frac{\omega b}{c} \sin\theta\cos\phi' \right)
            \end{aligned}
        \]

    \item \textbf{Approximation 2:} $b \ll \frac{c}{\omega}$
        \[
            \cos\left( \omega\left( t - \frac{\eta}{c} \right) \right)
            \simeq
            \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
            -
            \frac{\omega b}{c} \sin\theta\cos\phi' \sin\left( \omega\left( t - \frac{r}{c} \right) \right)
        \]
\end{itemize}
Collecting them together,
\begin{equation}
    \begin{aligned}
        \mathbf A(r, \theta, t)
        &\simeq
        \frac{\mu_0 I_0 b}{4\pi} \uvec\phi
        \int_{-\pi}^\pi \frac{1}{r} \left( 1 + \frac{b}{r}\sin\theta\cos\phi' \right)
        \left\{
            \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
            \phantom{\frac{\omega b}{c}}
        \right.
        \\
        &\qquad\qquad\qquad\qquad\qquad\qquad\qquad
        -
        \left.
            \frac{\omega b}{c} \sin\theta\cos\phi' \sin\left( \omega\left( t - \frac{r}{c} \right) \right)
        \right\}
        \cos\phi' d\phi'
        \\
        &=
        \frac{\mu_0 I_0 b}{4\pi r} \uvec\phi
        \int_{-\pi}^\pi d\phi' \cos\phi'
        \left\{
            \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
            \phantom{\frac{\omega b}{c}}
        \right.
        \\
        &\qquad\qquad\qquad\qquad\qquad\qquad
        +
        b\sin\theta\cos\phi'
        \left(
            \frac{1}{r}\cos\left( \omega\left( t - \frac{r}{c} \right) \right)
            -
            \frac{\omega}{c} \sin\left( \omega\left( t - \frac{r}{c} \right) \right)
        \right)
        \\
        &\qquad\qquad\qquad\qquad\qquad\qquad
        -
        \left.
            \cancelto{\color{blue}0 \because\;\mathcal O(r^{-2})}{\frac{b}{r}\frac{\omega b}{c}}
            \sin^2\theta \cos^2\phi'
            \sin\left( \omega\left( t - \frac{r}{c} \right) \right)
        \right\}
        \\
        &=
        \frac{\mu_0 I_0 b}{4\pi r} \uvec\phi
        \left\{
            \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
            \underbrace{\int_{-\pi}^\pi d\phi' \cos\phi'}_{\color{blue}=0}
            \right.
            \\
        &\qquad\qquad\qquad +
            \left.
            b\sin\theta \left(
                \frac{1}{r} \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
                -
                \frac{\omega}{c} \sin\left( \omega\left( t - \frac{r}{c} \right) \right)
            \right)
            \underbrace{\int_{-\pi}^\pi \cos^2\phi' d\phi'}_{\color{blue}=\pi}
        \right\}
    \end{aligned}
\end{equation}
After further arrangement,
\begin{equation}
    \begin{aligned}
        \mathbf A(r, \theta, t)
        &\simeq
        \frac{\mu_0 I_0 b}{4\pi r} \uvec\phi
        \cdot
        b\sin\theta
        \left(
            \frac{1}{r}\cos\left( \omega\left( t - \frac{r}{c} \right) \right)
            -
            \frac{\omega}{c}\sin\left( \omega\left( t - \frac{r}{c} \right) \right)
        \right)
        \cdot
        \pi
        \\
        &=
        \frac{\mu_0 m_0}{4\pi}
        \left( \frac{\sin\theta}{r} \right)
        \left(
            \frac{1}{r} \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
            -
            \frac{\omega}{c} \sin\left( \omega\left( t - \frac{r}{c} \right) \right)
        \right)
        \uvec\phi
    \end{aligned}
\end{equation}
In static case, $\omega \rightarrow 0$, we recover
\[
    \mathbf A = \frac{\mu_0}{4\pi} \frac{m_0 \sin\theta}{r^2} \uvec\phi
\]
\textbullet{} \textbf{Approximation 3:} $r \gg c/\omega$
\begin{equation}
    \boxed{%
        \mathbf A(r, \theta, t)
        =
        -\frac{\mu_0 m_0 \omega}{4\pi c}
        \left( \frac{\sin\theta}{r} \right)
        \sin\left( \omega\left( t - \frac{r}{c} \right) \right)
        \uvec\phi
    }
\end{equation}

\subsubsection{Fields}
Radiating electric field
\begin{equation}
    \boxed{%
        \mathbf E
        =
        -\frac{\partial\mathbf A}{\partial t}
        =
        \frac{\mu_0 m_0 \omega^2}{4\pi c}
        \left( \frac{\sin\theta}{r} \right)
        \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
        \uvec\phi
    }
\end{equation}
Magnetic field
\[
    \mathbf B
    =
    \nabla \times (A_\phi \uvec\phi)
    =
    \underbrace{
        \frac{\uvec r}{r\sin\theta}
        \frac{\partial}{\partial\theta} \left( \sin\theta A_\phi \right)
    }_{\color{blue}\mathcal O(r^{-2})}
    -
    \frac{\uvec\theta}{r} \frac{\partial}{\partial r} \left( r A_\phi \right)
\]
The first term won't contribute to radiation, and the second term
\begin{equation}
    \frac{1}{r} \frac{\partial}{\partial r} \left( r A_\phi \right)
    =
    -\frac{\mu_0 m_0 \omega}{4\pi c}
    \frac{\sin\theta}{r}
    \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
    \left( -\frac{\omega}{c} \right)
\end{equation}
Therefore, the radiating magnetic field is
\begin{equation}
    \boxed{%
        \mathbf B
        =
        -\frac{\mu_0 m_0 \omega^2}{4\pi c^2}
        \left( \frac{\sin\theta}{r} \right)
        \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
        \uvec\theta
    }
\end{equation}
Note
\[
    \frac{E}{B} = c
    ,\quad
    \uvec k = \uvec r = \uvec\phi \times (-\uvec\theta)
\]

\subsubsection{Radiation Power}
Energy flux
\begin{equation}
    \mathbf S(\mathbf r, t)
    =
    \frac{1}{\mu_0} (\mathbf E \times \mathbf B)
    =
    \frac{\mu_0}{c}
    \left(
        \frac{m_0 \omega^2}{4\pi c}
        \left( \frac{\sin\theta}{r} \right)
        \cos\left( \omega\left( t - \frac{r}{c} \right) \right)
    \right)^2
    \uvec r
\end{equation}
Intensity
\begin{equation}
    \langle \mathbf S \rangle
    =
    \frac{\mu_0 m_0^2 \omega^4}{32 \pi^2 c^3}
    \frac{\sin^2\theta}{r^2}
    \uvec r
\end{equation}
Total radiated power
\begin{equation}
    \langle P \rangle
    =
    \int \langle \mathbf S \rangle \cdot d\mathbf a
    =
    \frac{\mu_0 m_0^2 \omega^4}{12 \pi c^3}
\end{equation}

\subsubsection{Comparison With Electric Dipole Radiation}
The ratio of radiation power is
\begin{equation}
    \frac{P_{\rm magnetic}}{P_{\rm electric}}
    =
    \left( \frac{m_0}{p_0 c} \right)^2
\end{equation}
Making use of $p_0 = q_0 d$ and $m_0 = \pi b^2 I_0$, and since
\[
    I = \frac{dq}{dt}
    = \underbrace{q_0 \omega}_{\color{blue}I_0} \left( -\sin(\omega t) \right)
    = -I_0 \sin(\omega t)
\]
it follows
\begin{equation}
    \frac{P_{\rm magnetic}}{P_{\rm electric}}
    =
    \left( \frac{\omega b}{c} \right)^2
    {\color{blue}\ \ll 1}
\end{equation}
which should be very small by approximation 2.

Therefore, electric dipole radiation dominates \textit{for configurations with comparable dimensions}.

\homework{11.6}
