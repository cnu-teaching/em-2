# Electromagnetism 2 Lecture Note

[![pipeline status](https://gitlab.com/cnu-teaching/em-2/badges/master/pipeline.svg)](https://gitlab.com/cnu-teaching/em-2/-/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

The lecture note is generated using the LaTeX template [Science Textbook Template for LaTeX](https://github.com/ironmeld/science-textbook-template).

The lecture schedule calendar is powered by [Event Calendar](https://github.com/vkurko/calendar).
