\section{Charge and Energy}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Continuity Equation}
The first conservation equation we encountered is the continuity equation, which states (local) conservation of charge.

\framed{%
    If the charge in some region changes, then exactly that amount of charge must have passed in or out through the region.
}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch08-continuity_equation-01}
    \caption{Local charge conservation.}%
\end{figure}
Let $Q$ denote the total charge in a volume $\mathcal V$:
\[
    Q(t) = \int_{\mathcal V} \rho(\mathbf r, t) d\tau
\]
The time variation of the total charge in this volume must be compensated by the flow of charge across the boundary:
\[
    \frac{dQ}{dt} = \int_{\mathcal V} \frac{\partial\rho}{\partial t} d\tau = -\oint_{\mathcal S} \mathbf J \cdot d\mathbf a
\]
Note that $\mathbf J = \rho \mathbf v$ is none other than the charge flux.
Substituting $Q$ in the left side and making use of divergence theorem in the right side, it follows
\begin{equation}
    \boxed{%
        \frac{\partial\rho}{\partial t} = -\nabla \cdot \mathbf J
    }
\end{equation}

Recall that the charge conservation is baked into Maxwell's equation (through the Maxwell's correction term).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Poynting's Theorem}
\framed{%
    Poynting's theorem state (local) conservation of energy.
}

Recall that the work necessary to assemble a static charge distribution is
\[
    W_{\rm e} = \int \frac{\epsilon_0}{2} E^2 d\tau
\]
This is manifested as the energy stored in the electric field.\\

Likewise, the work required to get a current flowing (against the back emf) is
\[
    W_{\rm m} = \int \frac{1}{2\mu_0} B^2 d\tau
\]
This is manifested as the energy stored in the magnetic field.\\

These equations invite the total energy stored in electromagnetic fields per unit volume (energy density)
\begin{equation}
    \boxed{%
        u = \frac{1}{2} \left( \epsilon_0 E^2 + \frac{1}{\mu_0} B^2 \right)
    }
    \label{eq:ch08:ElectromagneticEnergyDensity}
\end{equation}
We will confirm this expression and develop the energy conservation law for electrodynamics.

\subsubsection{Work Done on Charge}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch08-energy_conservation-01}
    \caption{Work needed to redistribute charge.}%
    \label{fig:ch08:EnergyConservation}
\end{figure}
\begin{itemize}
    \item Suppose in Figure~\ref{fig:ch08:EnergyConservation}a there was some charge and current configuration at time $t$.
    \item By Maxwell's equation, this configuration produces fields, $\mathbf E$ and $\mathbf B$.
    \item In the next instant, $dt$, the charges move around a bit (Figure~\ref{fig:ch08:EnergyConservation}b).
    \item How much work, $dW$, is done by the electromagnetic forces acting on these charges, in the interval $dt$?
\end{itemize}

Consider the work done on a charge $q$ by fields:
\[
    dW = \mathbf F \cdot d\mathbf l
    =
    q (\mathbf E + \mathbf v \times \mathbf B) \cdot \mathbf v dt
    =
    q (\mathbf E \cdot \mathbf v) dt
\]
We can generalize it to a group of charges (by superposition).
Substituting $q\mathbf v \rightarrow \mathbf J d\tau$, it follows
\begin{equation}
    \boxed{%
        \frac{dW}{dt} = \int_{\mathcal V} (\mathbf E \cdot \mathbf J) d\tau
    }
    \label{eq:ch08:MechanicalPower}
\end{equation}
This is the rate at which work is done on all the charges in a volume $\mathcal V$ (i.e., power).

Therefore, $\mathbf E \cdot \mathbf J$ is the work done per \textit{unit time} per \textit{unit volume}, or power delivered per unit volume (Joule heating law).

\subsubsection{Poynting's Theorem}
Using Amp\`ere-Maxwell law,
\[
    \mathbf J = \frac{1}{\mu_0} \nabla \times \mathbf E - \epsilon_0 \frac{\partial\mathbf E}{\partial t}
\]
we can rewrite $\mathbf E \cdot \mathbf J$ as
\begin{equation}
    \mathbf E \cdot \mathbf J
    =
    \frac{1}{\mu_0} \underbrace{\mathbf E \cdot (\nabla \times \mathbf B)}_{\color{blue}(\dagger)}
    -
    \epsilon_0 \mathbf E \cdot \frac{\partial\mathbf E}{\partial t}
    \label{eq:ch08:PoyntingTheorem:Derivation1}
\end{equation}
Using product rule 6, we have
\[
    \nabla \cdot (\mathbf E \times \mathbf B)
    =
    \mathbf B \cdot \underbrace{(\nabla \times \mathbf E)}_{\color{red}(\dagger\dagger)}
    -
    \underbrace{\mathbf E \cdot (\nabla \times \mathbf B)}_{\color{blue}(\dagger)}
\]
and from Faraday's law,
\[
    {\color{red}(\dagger\dagger)\ \rightarrow\quad}
    \nabla \times \mathbf E
    =
    -\frac{\partial\mathbf B}{\partial t}
\]
Substituting them, the term $\color{blue}(\dagger)$ reads
\begin{equation}
    \mathbf E \cdot (\nabla \times \mathbf B)
    =
    -\mathbf B \cdot \frac{\partial\mathbf B}{\partial t}
    -\nabla \cdot (\mathbf E \times \mathbf B)
\end{equation}
Using the identity
$\displaystyle \frac{\partial}{\partial t} (\mathbf A \cdot \mathbf A) = 2\mathbf A \cdot \frac{\partial\mathbf A}{\partial t}$,
Eq.~(\ref{eq:ch08:PoyntingTheorem:Derivation1}) reads
\begin{equation}
    \begin{aligned}
        \mathbf E \cdot \mathbf J
        &=
        -
        \left[
            \epsilon_0 \mathbf E \cdot \frac{\partial\mathbf E}{\partial t}
            +
            \frac{1}{\mu_0} \mathbf B \cdot \frac{\partial\mathbf B}{\partial t}
        \right]
        -
        \frac{1}{\mu_0} \nabla \cdot (\mathbf E \times \mathbf B)
        \\
        &=
        -\frac{1}{2} \frac{\partial}{\partial t}
        \left( \epsilon_0 E^2 + \frac{1}{\mu_0} B^2 \right)
        -
        \frac{1}{\mu_0} \nabla \cdot (\mathbf E \times \mathbf B)
    \end{aligned}
\end{equation}
Putting this back into Eq.~(\ref{eq:ch08:MechanicalPower}), we arrive at
\begin{equation}
    \frac{dW}{dt}
    =
    -\int_{\mathcal V}
    \frac{\partial}{\partial t}
    \left[
        \frac{1}{2} \left( \epsilon_0 E^2 + \frac{1}{\mu_0} B^2 \right)
    \right]
    d\tau
    -
    \frac{1}{\mu_0}
    \int_{\mathcal V}
    \nabla \cdot (\mathbf E \times \mathbf B)
    d\tau
\end{equation}
Finally, making use of the divergence theorem in the second integral, it follows
\begin{equation}
    \boxed{%
        \underbrace{\frac{dW}{dt}}_{\color{blue}(\mathrm I)}
        =
        \underbrace{%
            -
            \frac{d}{dt}
            \int_{\mathcal V}
            \frac{1}{2} \left( \epsilon_0 E^2 + \frac{1}{\mu_0} B^2 \right)
            d\tau
        }_{\color{blue}(\mathrm{II})}
        \underbrace{%
            -
            \frac{1}{\mu_0} \oint_{\mathcal S} (\mathbf E \times \mathbf B) \cdot d\mathbf a
        }_{\color{blue}(\mathrm{III})}
    }
    \label{eq:ch08:PoyntingTheorem}
\end{equation}

\framed{%
    \textbf{Poynting theorem}:
    \[
        \begin{aligned}
        &\text{The work done on the charges by the electromagnetic force}\ \color{blue}(\mathrm I)
        \\
            =&\ %
            \text{The decrease in energy remaining in the field}\ \color{blue}(\mathrm{II})
            \\
             &+\ %
             \text{The energy that flowed out through the surface}\ \mathcal S\ \color{blue}(\mathrm{III})
        \end{aligned}
    \]
}

\subsubsection{Poynting Vector}
Poynting vector, defined as
\begin{equation}
    \boxed{%
        \mathbf S
        =
        \frac{1}{\mu_0} (\mathbf E \times \mathbf B)
    },
\end{equation}
is the energy per unit time per unit volume, transported by the fields.
(Its magnitude is called \textit{energy flux}.)\\

Using $u$ from Eq.~(\ref{eq:ch08:ElectromagneticEnergyDensity}) and $\mathbf S$, we can write Eq.~(\ref{eq:ch08:PoyntingTheorem}) more concisely:
\begin{equation}
    \frac{dW}{dt}
    =
    -\frac{d}{dt} \int_{\mathcal V} u d\tau
    -\oint_{\mathcal S} \mathbf S \cdot d\mathbf a
\end{equation}
If no work is done on the charge in volume $\mathcal V$ (for example, in empty space),
\begin{equation}
    \frac{dW}{dt} = 0
    \quad\rightarrow\quad
    \frac{d}{dt} \int_{\mathcal V} u d\tau
    =
    -\oint_{\mathcal S} \mathbf S \cdot d\mathbf a
\end{equation}
Using divergence theorem, it follows
\begin{equation}
    \boxed{%
        \frac{\partial u}{\partial t} = -\nabla \cdot \mathbf S
    }
\end{equation}

This states that if no energy is created/lost in a volume, the energy flowed out of the volume must be equal to the energy decrease in the volume.
This is ``continuity equation'' for energy (local conservation of electromagnetic energy).

\framed{%
    \example{8.1}
    Calculate power delivered through a current flow in the wire.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch08-example_1-01}
    \caption{Example 8.1. Power down the wire.}%
\end{figure}
We are going to use Poynting's theorem (Eq.~\ref{eq:ch08:PoyntingTheorem}).
That means, we need $\mathbf E$ and $\mathbf B$.

From Ex. 7.1, the electric field corresponding to a potential difference $V$ over the distance $L$ is
\[
    \mathbf E = \frac{V}{L} \uvec x
\]
From Ex. 5.7, the magnetic field \textit{at the surface} by the uniform current inside
\[
    \mathbf B = \frac{\mu_0 I}{2\pi a} \uvec\phi
\]
Thus, Poynting vector at the surface of the wire is
\[
    \mathbf S
    =
    \frac{1}{\mu_0} (\mathbf E \times \mathbf B)
    =
    -\uvec s \frac{VI}{2\pi a L}
\]
This is the energy flux passing through the surface of the wire.
It is interesting to note that energy does not flow along the wire, but flows through its surface.\\

Using Poynting's theorem, the power delivered is
\[
    \begin{aligned}
        \frac{dW}{dt}
        &=
        -\cancelto{\color{blue}0 \because \text{const. fields}}{\frac{d}{dt} \int_{\mathcal V} u d\tau}
        -\oint_{\mathcal S} \mathbf S \cdot d\mathbf a
        \\
        &=
        -\cancelto{\color{blue}0 \because \mathbf S \perp \uvec n}{\int_{\mathrm{sides}} \mathbf S \cdot d\mathbf a}
        -\int_{\rm surface} \mathbf S \cdot d\mathbf a
        \\
        &=
        -\left(-\frac{VI}{2\pi a L}\uvec s\right) \cdot (2\pi a L\uvec s)
        \\
        &=
        VI
    \end{aligned}
\]

\homework{8.1 (only the part related to Ex. 7.13)}

\framed{%
    \problem{8.1}
    Calculate the power (energy per unit time) transported down the cables of Ex. 7.13, assuming the two conductors are held at potential difference $V$, and carry current $I$.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch08-problem_1-01}
    \caption{Problem 8.1. Power transported through a coaxial cable.}%
\end{figure}
Following the same procedure as Ex. 8.1,
\begin{itemize}
    \item Electric field in $a < s < b$ (Ex. 7.2):
        \[
            \mathbf E = \frac{\lambda}{2\pi\epsilon_0} \frac{1}{s} \uvec s
        \]
        where $\lambda$ is charge per unit length.
        To determine $\lambda$, use the given potential difference $V$
        \[
            V = -\int_b^a \mathbf E \cdot d\mathbf l
            = -\frac{\lambda}{2\pi\epsilon_0} \int_b^a \frac{1}{s} ds
            = \frac{\lambda}{2\pi\epsilon_0} \log \frac{b}{a}
        \]
        Removing $\lambda$,
        \[
            \mathbf E = \frac{V}{\log (b/a)} \frac{1}{s} \uvec s
        \]
    \item Magnetic field in $a < s < b$ (Ex. 5.7):
        \[
            \mathbf B = \frac{\mu_0 I}{2\pi s} \uvec\phi
        \]
    \item Poynting vector:
        \[
            \mathbf S
            =
            V \log \frac{a}{b} \frac{1}{s} \cdot \frac{I}{2\pi s} (\uvec s \times \uvec \phi)
            =
            V \log \frac{a}{b} \frac{I}{2\pi} \frac{1}{s^2} \uvec z
        \]
    \item Power transported (noting $\partial u/\partial t = 0$):
        \[
            \begin{aligned}
                P
        &=
        \frac{dW}{dt}
        =
        -\oint_{\mathcal S} \mathbf S \cdot d\mathbf a
        \\
        &=
        V \log \frac{a}{b} \frac{I}{2\pi}
        \int_a^b \frac{1}{s^2} \cdot 2\pi s ds
        \\
        &=
        V I \log \frac{a}{b}
        \int_a^b \frac{1}{s} ds
        \\
        &=
        VI
            \end{aligned}
        \]
\end{itemize}
