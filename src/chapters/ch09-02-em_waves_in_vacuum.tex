\section{Electromagnetic Waves in Vacuum}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Wave Equation for $\mathbf E$ and $\mathbf B$}
\textbf Q. How did Maxwell predict the presence of electromagnetic waves?\\

Maxwell's equations in regions where $\rho = 0$ and $\mathbf J = 0$ (free space) read
\begin{equation}
    \left.
        \begin{array}{rlcrl}
            \text{(i)}   & \displaystyle \nabla \cdot \mathbf E = 0, &&
            \text{(iii)} & \displaystyle \nabla \times \mathbf E = -\frac{\partial\mathbf B}{\partial t}
            \\\\
            \text{(ii)}  & \displaystyle \nabla \cdot \mathbf B = 0, &&
            \text{(iv)}  & \displaystyle \nabla \times \mathbf B = \mu_0\epsilon_0\frac{\partial\mathbf E}{\partial t}
        \end{array}
    \right\}
\end{equation}
Apply curl to (iii):
\[
    \left\{
    \begin{aligned}
        \text{LHS:}
        &\quad
        \nabla \times (\nabla \times \mathbf E)
        =
        \nabla \underbrace{(\nabla \cdot \mathbf E)}_{\color{blue} = 0} - \nabla^2 \mathbf E
        =
        -\nabla^2 \mathbf E
        \\
        \text{RHS:}
        &\quad
        -\nabla \times \frac{\partial\mathbf B}{\partial t}
        =
        -\frac{\partial}{\partial t} \underbrace{(\nabla \times \mathbf B)}_{\color{blue}=\text{(iv)}}
        =
        -\mu_0\epsilon_0 \frac{\partial^2\mathbf E}{\partial t^2}
   \end{aligned}
   \right.
\]
Do the same for (iv), and we get
\begin{equation}
    \boxed{%
        \nabla^2\mathbf E = \mu_0\epsilon_0 \frac{\partial^2\mathbf E}{\partial t^2}
        ,\quad
        \nabla^2\mathbf B = \mu_0\epsilon_0 \frac{\partial^2\mathbf B}{\partial t^2}
    }
\end{equation}
These are vector equations, so each one has three equations:
\begin{equation}
    \nabla^2 E_i(x, y, z) = \mu_0\epsilon_0 \frac{\partial^2 E_i}{\partial t^2}
    ,\quad
    \nabla^2 B_i(x, y, z) = \mu_0\epsilon_0 \frac{\partial^2 B_i}{\partial t^2}
\end{equation}
where $i = x, y, z$.

Comparing with the \textbf{three-dimensional wave equation},
\[
    \nabla^2 f = \frac{1}{v^2} \frac{\partial^2 f}{\partial t^2}
\]
Maxwell's equations imply that empty space supports the propagation of electromagnetic waves, traveling at a speed
\begin{equation}
    v = \frac{1}{\sqrt{\epsilon_0\mu_0}} = 3.00 \times 10^8\,\text{m/s}
\end{equation}
precisely the speed of light, $c$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Monochromatic Plane Waves}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch09-plane_wave-01}
    \caption{Plane wave propagating in the $z$ direction.}%
\end{figure}
\begin{itemize}
    \item \textbf{Monochromatic} waves: Sinusoidal waves of a single frequency $\omega$ (or single wave number $k$).
    \item \textbf{Plane waves}: Waves have no dependence in directions perpendicular to the direction of propagation.
        That is, if the waves are traveling in the $z$ direction (as we have been assuming in Chapter 9.1), they have no $x$ or $y$ dependence.
\end{itemize}

\subsubsection{(a) Monochromatic plane waves of electromagnetic fields}
That is, we are interested in the fields of the form:
\begin{equation}
    \cxvec E(z, t) = \cxvec E_0 e^{i(kz - \omega t)}
    ,\quad
    \cxvec B(z, t) = \cxvec B_0 e^{i(kz - \omega t)}
    \label{eq:ch09:ElectromagneticWave:Z_Direction}
\end{equation}
where $\cxvec E_0$ and $\cxvec B_0$ are the corresponding complex amplitudes and $\omega = k c$ (in vacuum).

\subsubsection{(b) Transverse Property}
Maxwell's equations, (i) and (ii), impose extra constraints on $\cxvec E_0$ and $\cxvec B_0$:
\[
    \begin{aligned}
        &\nabla \cdot \cxvec E
        =
        e^{i(\ )} \underbrace{(\nabla \cdot \cxvec E_0)}_{\color{blue} = 0}
        +
        \cxvec E_0 \cdot \nabla e^{i(\ )}
        =
        (\cxvec E_0 \cdot \uvec z) \frac{\partial e^{i(\ )}}{\partial z}
        =
        0
        \\
        &\quad\rightarrow\ %
        \cxvec E_0 \cdot \uvec z = 0
    \end{aligned}
\]
or $(\tilde E_0)_z = 0$.
(Note that $\nabla e^{i(\ )} = \uvec z \partial e^{i(\ )}/\partial z$ because $e^{i(\ )}$ is a function only of $z$.)
Similarly, $(\tilde B_0)_z = 0$.
This is exactly the condition that transverse waves must have.

\framed{%
    The $\nabla \cdot \mathbf E = 0$ and $\nabla \cdot \mathbf B = 0$ conditions imply that electromagnetic waves (in free space) are transverse.
}

\subsubsection{(c) $\mathbf E$ and $\mathbf B$ Relationship}
Substituting Eq.~(\ref{eq:ch09:ElectromagneticWave:Z_Direction}) into Faraday's law,
the left-hand side
\[
    \begin{aligned}
        \nabla \times \cxvec E
        &=
        e^{i(\ )} \underbrace{(\nabla \times \cxvec E_0)}_{\color{blue} = 0\ \because\ \text{const.}}
        -
        \cxvec E_0 \times \underbrace{\nabla e^{i(\ )}}_{\color{blue}= ik \uvec z\, e^{i(\ )}}
        \\
        &=
        i (k \uvec z \times \cxvec E_0) e^{i(\ )}
    \end{aligned}
\]
and the right-hand side
\[
    -\frac{\partial\mathbf B}{\partial t}
    =
    -\frac{\partial}{\partial t} \left( \cxvec B_0 e^{i(\ )} \right)
    =
    \cxvec B_0 i \omega e^{i(\ )}
\]
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch09-E_B_relation-01}
    \caption{The relationship between $\cxvec E_0$ and $\cxvec B_0$.}%
    \label{fig:ch09:EBRelationShip}
\end{figure}
Thus, the electric and magnetic amplitudes are related by (Figure~\ref{fig:ch09:EBRelationShip})
\begin{equation}
    \cxvec B_0 = \frac{k}{\omega} \left( \uvec z \times \cxvec E_0 \right)
\end{equation}

\framed{%
    $\mathbf E(\mathbf r, t) = \Re(\cxvec E)$ and $\mathbf B(\mathbf r, t) = \Re(\cxvec B)$ are \textit{in phase} and mutually \textit{perpendicular}.
}

Let $B_0 = |\cxvec B_0|$ and $E_0 = |\cxvec E_0|$; then
\begin{equation}
    B_0 = \frac{k}{\omega} E_0 = \frac{1}{c} E_0
\end{equation}

\subsubsection{(d) Arbitrary Propagation Direction}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch09-wave_arbitrary_direction-01}
    \caption{Three-dimensional plane waves propagating in arbitrary direction.}%
    \label{fig:ch09:PlaneWaveArbitraryDirection}
\end{figure}
There is nothing special about the $z$ direction; we can easily generalize to monochromatic plane waves traveling in the arbitrary direction.
Using the position vector $\mathbf r$, the exponential term of waves propagating in the $z$ direction (Figure~\ref{fig:ch09:PlaneWaveArbitraryDirection}a) reads
\[
    e^{i(kz - \omega t)}
    =
    e^{i(k\uvec z \cdot \mathbf r  - \omega t)}
\]
By introducing the \textbf{propagation vector} (\textbf{wave vector}), $\mathbf k$, pointing in the direction of propagation, whose magnitude is the wave number $k$, we can write this as
\[
    e^{i(\mathbf k \cdot \mathbf r  - \omega t)}
\]
This is the coordinate-independent way of writing monochromatic plane waves propagating in an arbitrary direction.\\

So, the generalization of Eq.~(\ref{eq:ch09:ElectromagneticWave:Z_Direction}) reads
\begin{equation}
    \boxed{%
        \begin{aligned}
            \cxvec E(\mathbf r, t)
            &=
            \tilde E_0 e^{i(\mathbf k \cdot \mathbf r - \omega t)} \uvec n
            \\
            \cxvec B(\mathbf r, t)
            &=
            \frac{1}{c} \tilde E_0 e^{i(\mathbf k \cdot \mathbf r - \omega t)} (\uvec k \times \uvec n)
            =
            \frac{1}{c} \uvec k \times \cxvec E
        \end{aligned}
    }
\end{equation}
where $\uvec n$ is the polarization vector such that
\begin{equation}
    \uvec n \cdot \uvec k = 0
\end{equation}

The actual (real) electric and magnetic fields are
\[
    \left\{
        \begin{aligned}
        \mathbf E(\mathbf r, t)
        &=
        \Re \left( \cxvec E \right)
        =
        E_0 \cos\left( \mathbf k \cdot \mathbf r - \omega t + \delta \right) \uvec n
        \\
        \mathbf B(\mathbf r, t)
        &=
        \Re \left( \cxvec B \right)
        =
        \frac{1}{c} E_0 \cos\left( \mathbf k \cdot \mathbf r - \omega t + \delta \right) (\uvec k \times \uvec n)
        \end{aligned}
    \right.
\]

\homework{9.9}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Energy and Momentum in Electromagnetic Waves}
Recall from Chapter 8 \textbf{energy per unit volume} in electromagnetic field:
\begin{equation}
    u = \frac{1}{2} \left( \epsilon_0 E^2 + \frac{1}{\mu_0} B^2 \right)
\end{equation}
In the case of monochromatic plane wave,
\begin{equation}
    B^2 = \frac{1}{c^2} E^2 = \mu_0\epsilon_0 E^2
\end{equation}
Therefore,
\begin{equation}
    u = \frac{1}{2} \left( \epsilon_0 E^2 + \epsilon_0 E^2 \right)
    = \epsilon_0 E^2
    = \epsilon_0 E_0^2 \cos^2(kz - \omega t + \delta)
\end{equation}
Note that the electric and magnetic contributions are equal.
As the wave travels, it carries this energy along with it.

The \textbf{energy flux density} (energy per unit area, per unit time) transported by the fields is given by the Poynting vector:
\begin{equation}
    \mathbf S = \frac{1}{\mu_0} \left( \mathbf E \times \mathbf B \right)
\end{equation}
For monochromatic plane waves propagating in the $z$ direction,
\begin{equation}
    \mathbf S =
    c \epsilon_0 E_0^2 \cos^2(kz - \omega t + \delta) \uvec z
    = uc \uvec z
\end{equation}
Note that this has the same structure as $\mathbf J = \rho \mathbf v$, which is density multiplied by velocity vector.

\textbf{Momentum density} stored in the fields is then
\begin{equation}
    \mathbf g = \frac{1}{c^2} \mathbf S
    = \frac{1}{c} \epsilon_0 E_0^2 \cos^2(kz - \omega t + \delta) \uvec z
    = \frac{1}{c} u\uvec z
\end{equation}

In the case of \textit{light}, both the wavelength and period is very short ($\sim 5 \times 10^{-7}$ m and $\sim 10^{-15}$ s).
For most cases, the \textit{average} value is more meaningful.
Since the average of cosine-squared over a complete cycle is $\frac{1}{2}$, it follows
\begin{equation}
    \langle u \rangle = \frac{1}{2} \epsilon_0 E_0^2
\end{equation}
\begin{equation}
    \langle \mathbf S \rangle = c \langle u \rangle \uvec z = \frac{c}{2} \epsilon_0 E_0^2 \uvec z
\end{equation}
\begin{equation}
    \langle \mathbf g \rangle = \frac{1}{c} \langle u \rangle \uvec z = \frac{1}{2c} \epsilon_0 E_0^2 \uvec z
\end{equation}
where $\langle \cdots \rangle = \frac{1}{T}\int_0^T (\cdots) dt$.\\

\textbf{Intensity} is the average power per unit area transported by an electromagnetic wave
\begin{equation}
    I = \langle S \rangle = \frac{c}{2} \epsilon_0 E_0^2
\end{equation}

When light falls (at normal incidence) on a \textit{perfect absorber}, it delivers its momentum to the surface.
The relationship between momentum and pressure is
\[
    \begin{aligned}
        \text{force}
        &=
        \frac{d\mathbf p}{dt} \simeq \frac{\Delta\mathbf p}{\Delta t}
        \\
        \text{pressure}
        &=
        \text{force per unit area}
        \\
        &\simeq
        \frac{\Delta p}{\Delta t} \frac{1}{A}
    \end{aligned}
\]
The average momentum transferred by a monochromatic wave for the duration of $\Delta t$ is
\[
    \Delta \mathbf p
    =
    (\text{momentum density}) \cdot (\text{volume})
    =
    \langle \mathbf g \rangle A c \Delta t
\]
So, the \textbf{radiation pressure} (average force per unit area) is
\begin{equation}
    P = \frac{1}{A} \frac{\Delta p}{\Delta t} = \frac{1}{2} \epsilon_0 E_0^2 = \frac{I}{c}
\end{equation}
On a \textit{perfect reflector}, the change in momentum is doubled; and thus $P = 2I/c$.

\homework{9.10, 9.11, 9.12, 9.13}
