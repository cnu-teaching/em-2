\section{The Method of Images}
The image method exploits the uniqueness theorem in its fullest extent.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Classic Image Problem}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch03-classic_image_problem-01}
    \caption{Classic image problem.}%
    \label{fig:ch03:ClassicImageProblem}
\end{figure}
\framed{%
    \textbf Q. Suppose that a point charge $q$ is held a distance above an infinite grounded conducting plane (Figure~\ref{fig:ch03:ClassicImageProblem}a).
    What is the potential in the region above the plane?
}
This is how to approach the problem:
\begin{itemize}
    \item Mathematically, we need to solve Poisson's equation in the region $z > 0$ with a single point charge $q$ at $(0, 0, d)$, subject to the boundary conditions:
        \begin{enumerate}
            \item $V = 0$ when $z = 0$; and
            \item $V \rightarrow 0$ far from the charge.
        \end{enumerate}

    \item On the other hand, the first uniqueness theorem guarantees that there is only one function that meets these requirements.

    \item \textit{We can leverage this fact in our advantage to find the solution more easily.}
        \\
\end{itemize}

\textbf{Different problem:}\\
Consider a different situation (Figure~\ref{fig:ch03:ClassicImageProblem}b), where $+q$ is at $(0, 0, d)$ and $-q$ at $(0, 0, -d)$.
We can \textit{easily} write the potential at $\mathbf r$:
\begin{equation}
    V(\mathbf r)
    =
    \frac{1}{4\pi\epsilon_0}
    \left[
        \frac{q}{\sqrt{x^2 + y^2 + {(z - d)}^2}}
        -
        \frac{q}{\sqrt{x^2 + y^2 + {(z + d)}^2}}
    \right]
    \label{eq:ch03:PotentialOfClassicImageProblem}
\end{equation}
We specifically chose this configuration because
\begin{enumerate}
    \item $V = 0$ when $z = 0$;
    \item $V \rightarrow 0$ far from the charge; and
    \item $q$ is the only charge in $z > 0$.
\end{enumerate}

These are precisely the condition of the original problem.
By the \textit{uniqueness theorem}, Eq.\ (\ref{eq:ch03:PotentialOfClassicImageProblem}) must be the potential we are looking for.
The charge $-q$ we introduced in the analogous problem is called the \textbf{image charge}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Induced Surface Charge}
\framed{%
    \textbf Q. What is the surface charge induced on the conductor?
}

From the boundary condition,
\[
    \left. \frac{\partial V_{\rm above}}{\partial z} \right|_{z = 0^+}
    -
    \cancelto{\color{blue}0 \because V_{\rm below} = \mathrm{const}}{%
        \left. \frac{\partial V_{\rm below}}{\partial z} \right|_{z = 0^-}}
    =
    -\frac{\sigma}{\epsilon_0}
\]
where $\uvec n = \uvec z$.
Therefore,
\[
    \sigma = -\epsilon_0 \left. \frac{\partial V}{\partial z} \right|_{z = 0^+}
\]
Differentiating Eq.\ (\ref{eq:ch03:PotentialOfClassicImageProblem}) with respect to $z$,
\[
    \frac{\partial V}{\partial z}
    =
    \frac{1}{4\pi\epsilon_0}
    \left[
        \frac{-q (z - d)}{(x^2 + y^2 + (z - d)^2)^{3/2}}
        +
        \frac{ q (z + d)}{(x^2 + y^2 + (z + d)^2)^{3/2}}
    \right]
\]
Substituting it into $\sigma$, we get
\begin{equation}
    \sigma(x, y)
    =
    \frac{-qd}{2\pi (x^2 + y^2 + d^2)^{3/2}}
\end{equation}
As expected, the induced charge is \textit{negative} and greatest at $x = y = 0$.

The total induced charge is a matter of surface integration:
\[
    Q = \int \sigma da
\]
Using polar coordinates, it follows
\begin{equation}
    Q
    =
    \int_0^{2\pi} \int_0^\infty
    \frac{-qd}{2\pi (r^2 + d^2)^{3/2}}
    r dr d\phi
    =
    \left. \frac{qd}{\sqrt{r^2 + d^2}} \right|_0^\infty
    =
    -q
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Force and Energy}
\framed{%
    \textbf Q. What is force on $q$?
}

The charge $q$ should be attracted toward plane, because of the negative induced charge.
We can use the same potential and field in the vicinity of $q$ to calculate the force.

The field on $+q$ at $(0, 0, d)$ by $-q$ at $(0, 0, -d)$ is
\[
    \mathbf E
    =
    -\frac{1}{4\pi\epsilon_0} \frac{q}{(2d)^2} \uvec z
\]
and thus the force
\[
    \mathbf F
    =
    -\frac{1}{4\pi\epsilon_0} \frac{q^2}{(2d)^2} \uvec z
\]

\framed{%
    \textbf{Note}:
Using
\[
    \int_0^\infty \frac{r dr}{(r^2 + d^2)^2} = \frac{1}{2 d^2}
\]
we find that the potential at $(0, 0, d)$ by the surface charge $\sigma(x, y)$ is identical to the potential by the image charge:
\[
    \begin{aligned}
        V
        &=
        \frac{1}{4\pi\epsilon_0}
        \int \frac{\sigma}{\eta} da
        \\
        &=
        \frac{1}{4\pi\epsilon_0}
        \int_0^{2\pi} \int_0^\infty
        \left( \frac{1}{\sqrt{r^2 + d^2}} \right)
        \left( \frac{-qd}{2\pi (r^2 + d^2)^{3/2}} \right)
        r dr d\phi
        \\
        &=
        -\frac{q}{4\pi\epsilon_0} \frac{1}{(2d)}
    \end{aligned}
\]
}

\framed{%
    \textbf Q. What is the energy of this configuration?
}

For energy, we must be a bit careful.
The \textit{energy} stored in this configuration is equal to \textit{work} needed to bring $+q$ to $(0, 0, d)$ against the electric attraction:
\begin{equation}
    \begin{aligned}
        W
        &=
        \int_\infty^d (-q\mathbf E) \cdot d\mathbf l
        =
        \frac{1}{4\pi\epsilon_0} \int_\infty^d \frac{q^2}{4 z^2} dz
        \\
        &=
        \frac{1}{4\pi\epsilon_0} \left. \left( -\frac{q^2}{4z} \right) \right|_\infty^d
        \\
        &=
        -\frac{1}{4\pi\epsilon_0} \frac{q^2}{4d}
    \end{aligned}
\end{equation}

This is different from the energy of the two-charge configuration:
\[
    W
    =
    \frac{q^2}{4\pi\epsilon_0}
    \underbrace{\int_\infty^d \frac{1}{(z + d)^2} dz}_{\color{blue}= -1/(2d)}
    =
    -\frac{1}{4\pi\epsilon_0} \frac{q^2}{2d}
\]
The reason is that as I move $q$, I work only on $q$; \textbf{no work is needed to move the induced charge} over the conductor since the conductor is equipotential.

\framed{%
    \example{3.2}%
    A point charge $q$ is situated at a distance $a$ from the center of a grounded conducting sphere of radius $R$ (Figure~\ref{fig:ch03:Example2} (left)).
    Find the potential outside the sphere.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch03-example_2-01}
    \caption{Example 3.2 Configuration.}%
    \label{fig:ch03:Example2}
\end{figure}
\textbf Q. How should we place an image charge $q'$?

\textbf A. By symmetry, $q'$ should be on the line connecting the center of the sphere and $q$, and located inside the sphere (see Figure~\ref{fig:ch03:ClassicImageProblem} (right)).\\

The question is, what must $q'$ and $b$ be so that the potential at radius $R$ becomes zero in the analogous problem?
\\

Let's write down the potential of this configuration:
\[
    V(r, \theta)
    =
    \frac{1}{4\pi\epsilon_0} \left( \frac{q}{\eta} + \frac{q'}{\eta'} \right)
    =
    \frac{1}{4\pi\epsilon_0}
    \left[
        \frac{q}{\sqrt{r^2 + a^2 - 2ra \cos\theta}}
        +
        \frac{q'}{\sqrt{r^2 + b^2 - 2rb \cos\theta}}
    \right]
\]
Since the conductor is grounded,
\begin{enumerate}
    \item [(1)] $V = 0$ when $r = R$ and $\theta = 0$; thus
        \[
            \frac{q}{\sqrt{r^2 + a^2 - 2ra \cos\theta}}
            +
            \frac{q'}{\sqrt{r^2 + b^2 - 2rb \cos\theta}}
            =
            0
            \quad \rightarrow \quad
            \frac{q}{a - R}
            +
            \frac{q'}{R - b}
            =
            0
        \]
    \item [(2)] $V = 0$ when $r = R$ and $\theta = \pi$; thus
        \[
            \frac{q}{\sqrt{r^2 + a^2 + 2ra \cos\theta}}
            +
            \frac{q'}{\sqrt{r^2 + b^2 + 2rb \cos\theta}}
            =
            0
            \quad \rightarrow \quad
            \frac{q}{a + R}
            +
            \frac{q'}{R + b}
            =
            0
        \]
\end{enumerate}
Solving (1) and (2) together, we find
\begin{equation}
    q' = -\frac{R}{a}q
    \quad \text{and} \quad
    b = \frac{R^2}{a}
\end{equation}
So, the potential outside in the real problem must be identical to
\begin{equation}
    V(r, \theta)
    =
    \frac{1}{4\pi\epsilon_0} \left[
        \frac{q}{\sqrt{r^2 + a^2 - 2ra\cos\theta}}
        -
        \frac{q}{\sqrt{R^2 + (ra/R)^2 - 2ra\cos\theta}}
    \right]
\end{equation}

By the same reasoning, the force of attraction is
\begin{equation}
    F
    =
    \frac{1}{4\pi\epsilon_0} \frac{qq'}{(a - b)^2}
    =
    -\frac{1}{4\pi\epsilon_0} \frac{q^2 R a}{(a^2 - R^2)^2}
\end{equation}

\framed{%
    \textbi{Side Note}:
    There is an easier way to find $q'$.
    Using the result of Prob. 3.1,
    \[
        V_{\rm avg}
        =
        V(0) + \frac{Q_{\rm enc}}{4\pi\epsilon_0 R}
    \]
    Since the average potential on the surface of the conductor is zero, it follows
    \[
        0 = \frac{q}{4\pi\epsilon_0 a} + \frac{q'}{4\pi\epsilon_0 R}
    \]
    and thus
    \[
        q' = -q \frac{R}{a}
    \]
    Then, we put this result into either (1) or (2) to find $b$.
}

\homework{3.7, 3.8, 3.9}
