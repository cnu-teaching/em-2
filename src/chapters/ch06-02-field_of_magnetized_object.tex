\section{The Field of a Magnetized Object}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Bound Current}
\framed{%
    \textbf Q. Suppose we have a piece of magnetized material of $\mathbf M$.
    What field does this material produce?
}

Using the vector potential of a single dipole $\mathbf m$
\begin{equation}
    \mathbf A(\mathbf r)
    =
    \frac{\mu_0}{4\pi} \frac{\mathbf m \times \uspvec}{\eta^2}
\end{equation}
it's a matter of volume integral ($\mathbf m \rightarrow \mathbf M d\tau'$):
\begin{equation}
    \mathbf A(\mathbf r) = \frac{\mu_0}{4\pi} \int \frac{\mathbf M(\mathbf r') \times \uspvec}{\eta^2} d\tau'
    \label{eq:ch06:VectorPotential:Magnetization}
\end{equation}

As for the electric polarization, we will manipulate it to gain any physical insights.
Exploiting the identity
\[
    \nabla' \frac{1}{\eta} = \frac{\uspvec}{\eta^2}
\]
the integral can be cast into a more illuminating form
\[
    \mathbf A(\mathbf r)
    =
    \frac{\mu_0}{4\pi} \int \mathbf M(\mathbf r') \times \nabla' \left( \frac{1}{\eta} \right) d\tau'
\]
Integrating by parts,
\[
    \mathbf A(\mathbf r)
    =
    \frac{\mu_0}{4\pi}
    \left[
        \int \frac{1}{\eta} \left( \nabla' \times \mathbf M(\mathbf r') \right) d\tau'
        -
        \int \nabla' \times \left( \frac{\mathbf M(\mathbf r')}{\eta} \right) d\tau'
    \right]
\]
Problem 1.61(b) invites us to express the latter as a surface integral
\begin{equation}
    \mathbf A(\mathbf r)
    =
    \frac{\mu_0}{4\pi}
    \left[
        \int \frac{1}{\eta} (\underbrace{\nabla' \times \mathbf M(\mathbf r')}_{\color{blue} \mathbf J_b}) d\tau'
        +
        \oint \frac{1}{\eta} \underbrace{\mathbf M(\mathbf r') \times \uvec n}_{\color{blue} \mathbf K_b} da'
    \right]
\end{equation}
where $\uvec n$ is the usual outward normal unit vector.
The first term looks just like the potential of a volume current
\begin{equation}
    \boxed{%
        \mathbf J_b = \nabla \times \mathbf M
    }
\end{equation}
and the second looks like the potential of a surface current
\begin{equation}
    \boxed{%
        \mathbf K_b = \mathbf M \times \uvec n
    }
\end{equation}

With these definitions,
\begin{equation}
    \mathbf A(\mathbf r)
    =
    \frac{\mu_0}{4\pi} \int_{\mathcal V} \frac{\mathbf J_b(\mathbf r')}{\eta} d\tau'
    +
    \frac{\mu_0}{4\pi} \oint_{\mathcal S} \frac{\mathbf K_b(\mathbf r')}{\eta} da'
\end{equation}
The currents $\mathbf J_b$ and $\mathbf K_b$ are the \textbf{bound current}.

\framed{%
    \example{6.1}
    Find the magnetic field of a uniformly magnetized sphere.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch06-example_1-01}
    \caption{Example 6.1 Magnetic field of uniformly magnetized sphere.}%
\end{figure}
We can choose the $z$ axis along the direction of $\mathbf M$.
Then, we have
\[
    \mathbf J_b = \nabla \times \mathbf M = 0
    ,\qquad
    \mathbf K_b = \mathbf M \times \uvec r = M \sin\theta \uvec\phi
\]
In Ex.~5.11, we found that a rotating spherical shell of a uniform charge $\sigma$ generates a magnetic field inside the sphere
\[
    \mathbf B = \frac{2}{3} \mu_0 \sigma R \boldsymbol\omega
\]
Since the surface current density is
\[
    \mathbf K = \sigma\omega R \sin\theta \uvec\phi
    = \underbrace{\sigma R \boldsymbol\omega}_{\color{blue}\mathbf M} \times r
\]
it follows
\[
    \mathbf B = \frac{2}{3} \mu_0 \mathbf M
\]
which states that the internal field is uniform.\\

The field outside is the same as that of a perfect magnetic dipole given by
\[
    \mathbf m = \frac{4}{3} \pi R^3 \mathbf M
\]

\homework{6.7, 6.8}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Physical Interpretation of Bound Currents}
Let's see how the bound currents $\mathbf J_b$ and $\mathbf K_b$ can be understood physically.

\subsubsection{Uniform Magnetization}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch06-bound_current_of_uniform_magnetization-01}
    \caption{Bound current of a thin slab of magnetized medium.}%
    \label{fig:ch06:SlabBoundCurrent}
\end{figure}
\begin{itemize}
    \item Figure~\ref{fig:ch06:SlabBoundCurrent} depicts a thin slab of uniformly magnetized material, with the dipoles represented by tiny current rings.

    \item The internal currents cancel, so the whole thing becomes equivalent to a single ribbon of current $I$ flowing around the boundary.

    \item What is this current in terms of $\mathbf M$?
\end{itemize}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch06-representation_of_tiny_magnetic_moment-01}
    \caption{Representation of a tiny magnetic moment.}%
    \label{fig:ch06:TinyMagneticMoment}
\end{figure}
Each tiny current loop has a magnetic moment (Figure~\ref{fig:ch06:TinyMagneticMoment})
\[
    m = M a t
\]
while the dipole moment in terms of current is
\[
    m = I a
\]
Therefore, the current $I$ in terms of $M$ is
\[
    I = M t
\]
So the surface current density is
\[
    K_b = \frac{I}{t} = M
\]
Using the outward unit vector $\uvec n$
\[
    \mathbf K_b = \mathbf M \times \uvec n
\]
as expected.

\subsubsection{Nonuniform Magnetization}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch06-bound_current_of_nonuniform_magnetization-01}
    \caption{Bound current of nonuniform magnetization.}%
    \label{fig:ch06:BoundCurrentNonuniformMagnetization}
\end{figure}
When the magnetization is nonuniform, the internal currents no longer cancel.
Figure~\ref{fig:ch06:BoundCurrentNonuniformMagnetization} shows two adjacent chunks of magnetized material.
For the configuration of Figure~\ref{fig:ch06:BoundCurrentNonuniformMagnetization}a, the net current in the $x$ direction on the surface where they join is
\[
    I_x = \left[ M_z(y + dy) - M_z(y) \right] dz
    = \frac{\partial M_z}{\partial y} dy dz
\]
The corresponding volume current density is
\[
    (J_b)_x = \frac{I_x}{a_\perp} = \frac{\partial M_z}{\partial y}
\]
By the same token, a nonuniform magnetization in the $y$ direction (Figure~\ref{fig:ch06:BoundCurrentNonuniformMagnetization}b) would contribute an amount
\[
    (J_b)_x = -\frac{\partial M_y}{\partial z}
\]
Combining them together,
\[
    (J_b)_x
    =
    \frac{\partial M_z}{\partial y}
    -
    \frac{\partial M_y}{\partial z}
    =
    (\nabla \times \mathbf M)_x
\]
The generalization is straightforward to have
\[
    \mathbf J_b = \nabla \times \mathbf M
\]

Like any other \textit{steady} current, the bound current satisfies
\[
    \nabla \cdot \mathbf J_b = 0
\]
which is directly from the vector identity.
