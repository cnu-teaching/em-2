\section{Momentum}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Newton's Third Law in Electrodynamics}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch08-fields_of_moving_charge-01}
    \caption{Electric and magnetic fields of a moving charge.}%
    \label{fig:ch08:FieldsOfMovingCharge}
\end{figure}
\begin{itemize}
    \item Newton's third law: All forces between two objects exist in equal magnitude and opposite direction.
    \item A moving charge creates $\mathbf E$ radially outward from the instantaneous position of the charge (Figure~\ref{fig:ch08:FieldsOfMovingCharge}a).
    \item A moving charge creates $\mathbf B$ that circles around the velocity vector (Figure~\ref{fig:ch08:FieldsOfMovingCharge}b).
    \item Interaction of two moving charges suggests that the fields should also carry some momentum to ensure momentum conservation.
\end{itemize}

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch08-interaction_of_two_moving_charges-01}
    \caption{Interaction of two moving charges.}%
    \label{fig:ch08:InteractionOfTwoMovingCharges}
\end{figure}
Suppose that two identical charges are traveling at the same speed, but their movement is perpendicular to each other.

In situations like Figure~\ref{fig:ch08:InteractionOfTwoMovingCharges}, the net electromagnetic force (i.e., Lorentz force) of $q_1$ on $q_2$ (denoted as $\mathbf F_{12}$) is equal but \textit{not} opposite to the force of $q_2$ on $q_1$ (denoted as $\mathbf F_{21}$):
\[
    \mathbf F_{12} + \mathbf F_{21} \ne 0
\]
Is this in violation of Newton's third law?
(\textit{cf}, the third law holds in electrostatics and magnetostatics.)\\

The remedy is that \textit{the fields themselves carry momentum}.
This is not surprising, considering that they also carry energy.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Maxwell's Stress Tensor}
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch08-lorentz_force_on_volume_charge-01}
    \caption{Lorentz force on charges in volume $\mathcal V$.}%
\end{figure}
From the Lorentz force on $dq$
\[
    d\mathbf F
    =
    (\mathbf E + \mathbf v \times \mathbf B) dq,
\]
the total electromagnetic force on charges on volume $\mathcal V$ is
\begin{equation}
    \mathbf F
    =
    \int_{\mathcal V} (\mathbf E + \mathbf v \times \mathbf B) \rho d\tau
    =
    \int_{\mathcal V} \underbrace{(\rho\mathbf E + \mathbf J \times \mathbf B)}_{\color{blue}(1)} d\tau
\end{equation}
The term (1) is the \textit{force per unit volume}
\begin{equation}
    \mathbf f = \rho\mathbf E + \mathbf J \times \mathbf B
\end{equation}
Next, using Gauss's law
\[
    \rho = \epsilon_0 \nabla \cdot \mathbf E
\]
and Amp\`ere-Maxwell law
\[
    \mathbf J = \frac{1}{\mu_0} \nabla \times \mathbf B - \epsilon_0 \frac{\partial\mathbf E}{\partial t}
\]
eliminate $\rho$ and $\mathbf J$ from $\mathbf f$:
\[
    \mathbf f
    =
    \epsilon_0 (\nabla \cdot \mathbf E) \mathbf E
    +
    \frac{1}{\mu_0} (\nabla \times \mathbf B) \times \mathbf B
    -
    \epsilon_0 \underbrace{\left( \frac{\partial\mathbf E}{\partial t} \times \mathbf B \right)}_{\color{blue}(2)}
\]
We are going to transform the term (2).
Substituting Faraday's law
\[
    \frac{\partial\mathbf B}{\partial t} = -\nabla \times \mathbf E
\]
into the identity
\[
    \frac{\partial}{\partial t} (\mathbf E \times \mathbf B)
    =
    \frac{\partial\mathbf E}{\partial t} \times \mathbf B
    +
    \mathbf E \times \frac{\partial\mathbf B}{\partial t}
\]
it follows
\[
    \begin{aligned}
        &\frac{\partial}{\partial t} (\mathbf E \times \mathbf B)
        =
        \frac{\partial\mathbf E}{\partial t} \times \mathbf B
        -
        \mathbf E \times (\nabla \times \mathbf E)
        \\
        &\quad\rightarrow
        \frac{\partial\mathbf E}{\partial t} \times \mathbf B
        =
        \frac{\partial}{\partial t} (\mathbf E \times \mathbf B)
        +
        \mathbf E \times (\nabla \times \mathbf E)
    \end{aligned}
\]
Putting it into (2),
\[
    \begin{aligned}
        \mathbf f
        &=
        \epsilon_0 (\nabla \cdot \mathbf E)\mathbf E
        -
        \epsilon_0 \mathbf E \times (\nabla \times \mathbf E)
        \\
        &\ {\color{blue}+ \frac{1}{\mu_0} (\nabla \cdot \mathbf B)\mathbf B}
        -
        \frac{1}{\mu_0} \mathbf B \times (\nabla \times \mathbf B)
        -
        \epsilon_0 \frac{\partial}{\partial t} (\mathbf E \times \mathbf B)
    \end{aligned}
\]
or
\begin{equation}
    \begin{aligned}
        \mathbf f
        &=
        \epsilon_0
        \left[ (\nabla \cdot \mathbf E)\mathbf E - \mathbf E \times (\nabla \times \mathbf E) \right]
        \\
        &+
        \frac{1}{\mu_0}
        \left[ {\color{blue} (\nabla \cdot \mathbf B)\mathbf B} - \mathbf B \times (\nabla \times \mathbf B) \right]
        -
        \epsilon_0 \frac{\partial}{\partial t} (\mathbf E \times \mathbf B)
    \end{aligned}
\end{equation}
where, since $\nabla \cdot \mathbf B = 0$, we inserted the term with blue color for symmetry.

Meanwhile, making use of product rule 4:
\[
    \nabla (\mathbf A \cdot \mathbf B)
    =
    \mathbf A \times (\nabla \times \mathbf B)
    +
    \mathbf B \times (\nabla \times \mathbf A)
    +
    (\mathbf A \cdot \nabla )\mathbf B
    +
    (\mathbf B \cdot \nabla )\mathbf A
\]
it follows
\[
    \begin{aligned}
        &\nabla (\mathbf E \cdot \mathbf E)
        = \nabla E^2 = 2 \left[ \mathbf E \times (\nabla \times \mathbf E) + (\mathbf E \cdot \nabla)\mathbf E \right]
        \\
        &\ \rightarrow\ %
        \mathbf E \times (\nabla \times \mathbf E)
        =
        \frac{1}{2} \nabla E^2 - (\mathbf E \cdot \nabla)\mathbf E
    \end{aligned}
\]
Likewise,
\[
    \mathbf B \times (\nabla \times \mathbf B)
    =
    \frac{1}{2} \nabla B^2 - (\mathbf B \cdot \nabla)\mathbf B
\]
Substituting them, $\mathbf f$ becomes
\begin{equation}
    \begin{aligned}
        \mathbf f
        =&\,
        \underbrace{
            \epsilon_0 \left[
                (\nabla \cdot \mathbf E)\mathbf E
                +
                (\mathbf E \cdot \nabla)\mathbf E
                -
                \frac{1}{2} \nabla E^2
            \right]
            +
            \frac{1}{\mu_0} \left[
                (\nabla \cdot \mathbf B)\mathbf B
                +
                (\mathbf B \cdot \nabla)\mathbf B
                -
                \frac{1}{2} \nabla B^2
            \right]
        }_{\color{blue}\nabla \cdot \tensor T}
        \\
        &-
        \underbrace{\epsilon_0 \frac{\partial}{\partial t} (\mathbf E \times \mathbf B)}_{\color{blue} \epsilon_0\mu_0 \mathbf S}
    \end{aligned}
    \label{eq:ch08:LorentzForceOnChargeInUnitVolume}
\end{equation}
This is the force on the charge in a unit volume.\\

We can simplify this expression by introducing the \textbf{Maxwell stress tensor}
\begin{equation}
    T_{ij}
    =
    \epsilon_0 \left( E_i E_j - \frac{1}{2}\delta_{ij}E^2 \right)
    +
    \frac{1}{\mu_0} \left( B_i B_j - \frac{1}{2}\delta_{ij}B^2 \right)
    \label{eq:ch08:MaxwellStressTensor}
\end{equation}
where $i, j = x, y, z$, $E^2 = \sum_{i = 1}^3 E_i^2$ and $B^2 = \sum_{i = 1}^3 B_i^2$, and $\delta_{ij}$ is the Kronecker delta.
To write a few components,
\[
    \begin{aligned}
        T_{xx}
        &=
        \epsilon_0 \left( E_x^2 - \frac{1}{2} E^2 \right)
        +
        \frac{1}{\mu_0} \left( B_x^2 - \frac{1}{2} B^2 \right)
        \\
        &=
        \frac{\epsilon_0}{2} \left( E_x^2 - E_y^2 - E_z^2 \right)
        +
        \frac{1}{2\mu_0} \left( B_x^2 - B_y^2 - B_z^2 \right)
        \\
        T_{xy}
        &=
        \epsilon_0 \left( E_x E_y - 0 \right)
        +
        \frac{1}{\mu_0} \left( B_x B_y - 0 \right)
        \\
        &\qquad\qquad \vdots
    \end{aligned}
\]

In terms of the Maxwell stress tensor, Eq.~(\ref{eq:ch08:LorentzForceOnChargeInUnitVolume}) reads
\begin{equation}
    \mathbf f = \nabla \cdot \tensor T - \epsilon_0 \mu_0 \frac{\partial \mathbf S}{\partial t}
    \label{eq:ch08:LorentzForce:MaxwellStressTensor}
\end{equation}

Integrating Eq.~(\ref{eq:ch08:LorentzForce:MaxwellStressTensor}), the total electromagnetic force on charge in a volume $\mathcal V$ is
\begin{equation}
    \begin{aligned}
        \mathbf F = \int_{\mathcal V} \mathbf f d\tau
        &=
        \int_{\mathcal V} \nabla \cdot \tensor T d\tau
        -
        \epsilon_0 \mu_0 \int_{\mathcal V} \frac{\partial \mathbf S}{\partial t} d\tau
        \\
        &=
        \oint_{\mathcal S} \tensor T \cdot d\mathbf a
        -
        \epsilon_0 \mu_0 \frac{d}{dt} \int_{\mathcal V} \mathbf S d\tau
    \end{aligned}
\end{equation}
where we used the divergence theorem in the first term.
\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch08-static_electromagnetic_force-01}
    \caption{Electromagnetic force acting on charge.}%
\end{figure}

\separator%

\textbf{Proof of Eq.~(\ref{eq:ch08:LorentzForce:MaxwellStressTensor}):}
Considering
\[
    V_i = \mathbf V \cdot \uvec e_i
    ,\qquad
    T_{ij} = \uvec e_i \cdot \tensor T \cdot \uvec e_j
\]
we can deduce
\[
    \tensor T
    =
    \epsilon_0 \left( \mathbf E\mathbf E - \frac{1}{2} E^2 (\uvec x\uvec x + \uvec y\uvec y + \uvec z\uvec z) \right)
    +
    \frac{1}{\mu_0} \left( \mathbf B\mathbf B - \frac{1}{2} B^2 (\uvec x\uvec x + \uvec y\uvec y + \uvec z\uvec z) \right)
\]
Since we can write
\[
    \mathbf E\mathbf E
    =
    (\mathbf E E_x) \uvec x
    +
    (\mathbf E E_y) \uvec y
    +
    (\mathbf E E_z) \uvec z
\]
it follows
\[
    \nabla \cdot (\mathbf E\mathbf E)
    =
    \left( \nabla \cdot (\mathbf E E_x) \right) \uvec x
    +
    \left( \nabla \cdot (\mathbf E E_y) \right) \uvec y
    +
    \left( \nabla \cdot (\mathbf E E_z) \right) \uvec z
\]
Its $x$ component is
\[
    \left( \nabla \cdot (\mathbf E\mathbf E) \right)_x
    =
    \nabla \cdot (\mathbf E E_x)
    =
    (\mathbf E \cdot \nabla) E_x
    +
    E_x (\nabla \cdot \mathbf E)
\]
By the same reasoning, we find
\[
    \left( \nabla \cdot (E^2 \uvec x\uvec x) \right)_x
    =
    \nabla \cdot (E^2 \uvec x)
    =
    (\uvec x \cdot \nabla) E^2
    +
    E^2 \underbrace{(\nabla \cdot \uvec x)}_{\color{blue} = 0}
    =
    \frac{\partial}{\partial x} E^2
\]
Putting this all together, it follows
\[
    \begin{aligned}
        \left( \nabla \cdot \tensor T \right)_x
        &=
        \epsilon_0 \left[
            (\nabla \cdot \mathbf E) E_x
            +
            (\mathbf E \cdot \nabla) E_x
            -
            \frac{1}{2} \frac{\partial}{\partial x} E^2
        \right]
        \\
        &+
        \frac{1}{\mu_0} \left[
            (\nabla \cdot \mathbf B) B_x
            +
            (\mathbf B \cdot \nabla) B_x
            -
            \frac{1}{2} \frac{\partial}{\partial x} B^2
        \right]
    \end{aligned}
\]

\separator%

In the case of electrostatic ($\partial\mathbf E/\partial t = 0$) and magnetostatic ($\partial\mathbf B/\partial t = 0$) fields,
\begin{equation}
    \mathbf F = \oint_{\mathcal S} \tensor T \cdot d\mathbf a
    \qquad \text{(static)}
    \label{eq:ch08:LorentzForce:Static}
\end{equation}
This is the electromagnetic force, in terms of the Maxwell stress tensor, acting on charges in $\mathcal V$.

\framed{%
    \example{8.2}%
    Determine the net force on the ``northern'' hemisphere of uniformly charged solid sphere of radius $R$ and charge $Q$.
    (This is the same as Prob. 2.47, but this time use the Maxwell stress tensor.)
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch08-example_2-01}
    \caption{Example 8.2 Force on a hemispheric charge.}%
\end{figure}
Since there is no current, $\mathbf B = 0$.
The electric field is given by
\begin{equation}
    \mathbf E
    =
    \left\{
        \begin{array}{ccl}
            \displaystyle \frac{1}{4\pi\epsilon_0} \frac{Q}{r^2} \uvec r & & r > R \\
            \\
            \displaystyle \frac{1}{4\pi\epsilon_0} \frac{Q}{R^3} \mathbf r & & r < R \\
        \end{array}
    \right.
\end{equation}

From Eq.~(\ref{eq:ch08:LorentzForce:Static}), the net force in the northern hemisphere is
\[
    \mathbf F
    =
    \oint_{\mathcal S} \tensor T \cdot d\mathbf a
    =
    \int_{\rm dome} \tensor T \cdot d\mathbf a
    +
    \int_{\rm disk} \tensor T \cdot d\mathbf a
\]
where $\mathcal S$ is the surface enclosing the northern hemisphere.

By symmetry, we know that $\mathbf F = F \uvec z$ (i.e., the two spheres should repel each other).
So, focusing only on the $z$ component,
\[
    F
    =
    \uvec z \cdot \mathbf F
    =
    \underbrace{\int_{\rm dome} \uvec z \cdot \tensor T \cdot d\mathbf a}_{\color{blue}F_{\rm dome}}
    +
    \underbrace{\int_{\rm disk} \uvec z \cdot \tensor T \cdot d\mathbf a}_{\color{blue}F_{\rm disk}}
\]
\begin{enumerate}
    \item \textbf{Disk}: The surface normal at the $xy$ plane (i.e., $z = 0$) is $\uvec n = -\uvec z$.
        Then,
        \[
            \begin{aligned}
                \uvec z \cdot \tensor T \cdot \uvec n
                &=
                \epsilon_0 \left[
                    (\uvec z \cdot \mathbf E)(\mathbf E \cdot (-\uvec z))
                    -
                    \frac{1}{2} E^2
                    (\uvec z \cdot \uvec z)(\uvec z \cdot (-\uvec z))
                \right]
                \\
                &=
                \frac{\epsilon_0}{2} (E_x^2 + E_y^2 - E_z^2)
            \end{aligned}
        \]
        However, $E_z = 0$ when $z = 0$, so
        \[
            \left. \uvec z \cdot \tensor T \cdot \uvec n \right|_{z = 0}
            =
            \frac{\epsilon_0}{2}  (E_x^2 + E_y^2)
            =
            \frac{\epsilon_0}{2} \left( \frac{Q}{4\pi\epsilon_0 R^3} \right)^2
        \]
        Using $da = r dr d\phi$, we find
        \[
            F_{\rm disk}
            =
            \frac{\epsilon_0}{2} \left( \frac{Q}{4\pi\epsilon_0 R^3} \right)^2
            \cdot 2\pi \int_0^R r^3 dr
            =
            \frac{1}{4\pi\epsilon_0} \frac{Q^2}{16 R^2}
        \]

    \item \textbf{Dome}: The surface normal is $\uvec n = \uvec r$. Then
        \[
            \begin{aligned}
                \uvec z \cdot \tensor T \cdot \uvec n
                &=
                \epsilon_0 \left[
                    (\uvec z \cdot \mathbf E)(\mathbf E \cdot \uvec r)
                    -
                    \frac{1}{2} E^2
                    (\uvec z \cdot \uvec z)(\uvec z \cdot \uvec r)
                \right]
                \\
                &=
                \epsilon_0 E^2 \left[
                    (\uvec z \cdot \uvec r)\underbrace{(\uvec r \cdot \uvec r)}_{\color{blue}=1}
                    -
                    \frac{1}{2}
                    \underbrace{(\uvec z \cdot \uvec z)}_{\color{blue}=1}(\uvec z \cdot \uvec r)
                \right]
                \\
                &=
                \frac{\epsilon_0}{2} E^2 (\uvec z \cdot \uvec r)
            \end{aligned}
        \]
        $E^2$ is evaluated at $r = R$
        \[
            E^2 = \left( \frac{1}{4\pi\epsilon_0} \frac{Q}{R^2} \right)^2
        \]
        and in spherical coordinates $\uvec z \cdot \uvec r = \cos\theta$, so
        \[
            \left. \uvec z \cdot \tensor T \cdot \uvec n \right|_{r = R}
            =
            \frac{\epsilon_0}{2} \left( \frac{1}{4\pi\epsilon_0} \frac{Q}{R^2} \right)^2 \cos\theta
        \]
        Using $da = R^2 \sin\theta d\theta d\phi$, we find
        \[
            F_{\rm dome}
            =
            \frac{\epsilon_0}{2} \left( \frac{1}{4\pi\epsilon_0} \frac{Q}{R^2} \right)^2
            \cdot 2\pi R^2 \underbrace{\int_0^{\pi/2} \cos\theta \sin\theta d\theta}_{\color{blue}=1/2}
            =
            \frac{1}{4\pi\epsilon_0} \frac{Q^2}{8 R^2}
        \]
\end{enumerate}
Putting them together, we get
\[
    F = \left( F_{\rm disk} + F_{\rm dome} \right)
    = \frac{1}{4\pi\epsilon_0} \frac{3 Q^2}{16 R^2}
\]

Incidently, since there is no charge outside the dome, one can extend the volume to infinity for $z > 0$.
Then, $F_{\rm dome} \rightarrow 0$, because $\mathbf E \rightarrow 0$ as $r \rightarrow \infty$.
So,
\[
    F = F_{\rm disk} + F_{\text{outer plane}}
\]
Contribution from the $xy$ plane outside the sphere is
\[
    F_{\text{outer plane}}
    =
    \frac{\epsilon_0}{2} \left( \frac{Q}{4\pi\epsilon_0} \right)^2
    \cdot 2\pi \int_0^R \frac{1}{r^3} dr
    =
    \frac{\epsilon_0}{2} \left( \frac{Q}{4\pi\epsilon_0} \right)^2
    \cdot \frac{\pi}{R^2}
    \,\color{blue}{=F_{\rm dome}}
\]

\homework{8.3, 8.4}

\framed{%
    \problem{8.3}%
    Calculate the force of \textit{magnetic} attraction between the northern and southern hemispheres of a uniformly charged spinning spherical shell, with radius $R$, angular velocity $\omega$, and surface charge density $\sigma$.
    (Same as Prob. 5.44, but using the Maxwell stress tensor.)
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch08-problem_3-01}
    \caption{Problem 8.3 Force due to spinning shell charge.}%
\end{figure}
As in Ex. 8.2,
\[
    \mathbf F = F \uvec z
    =
    \oint_{\mathcal S} \tensor T \cdot d\mathbf a
    =
    \int_{\rm disk} \left( \tensor T_E + \tensor T_B \right) \cdot d\mathbf a
    +
    \int_{\rm outer} \left( \tensor T_E + \tensor T_B \right) \cdot d\mathbf a
\]
where $\tensor T_E$ and $\tensor T_B$ are respectively the electric and magnetic contributions to $\tensor T$.

\subsubsection{Force Due To Electric Field}
The electric field of a uniform shell charge is
\[
    \mathbf E
    =
    \left\{
        \begin{array}{ccl}
            0 & & r < R \\
            \\
            \displaystyle \frac{1}{4\pi\epsilon_0} \frac{Q}{r^2} \uvec r & & r > R \qquad (Q = 4\pi R^2 \sigma)
        \end{array}
    \right.
\]
Using the result of Ex. 8.2,
\[
    F_E = F_{E,\rm outer}
    = \frac{1}{4\pi\epsilon_0} \frac{Q^2}{8 R^2}
    = \frac{1}{4\pi\epsilon_0} \frac{(4\pi)^2 R^4 \sigma^2}{8 R^2}
    = \frac{4\pi}{\epsilon_0} \frac{\sigma^2 R^2}{8}
\]

\subsubsection{Force Due To Magnetic Field}
From Ex. 5.11, the vector potential is
\[
    \mathbf A(r, \theta, \phi)
    =
    \left\{
        \begin{array}{ccl}
            \displaystyle \frac{\mu_0 R \omega \sigma}{3} r \sin\theta \uvec \phi & & r < R \\
            \\
            \displaystyle \frac{\mu_0 R^4 \omega \sigma}{3} \frac{\sin\theta}{r^2} \uvec \phi & & r > R \\
        \end{array}
    \right.
\]
which is given in spherical coordinates.
Meanwhile, the magnetic field in terms of the vector potential is
\[
    B = \nabla \times \mathbf A
    = \nabla \times \left( A_\phi \uvec \phi \right)
    =
    -\frac{\partial A_\phi}{\partial z} \uvec s
    +\frac{1}{s} \frac{\partial \left( s A_\phi \right)}{\partial s} \uvec z
\]
where cylindrical coordinates are assumed for the expansion.
Note that $\partial A_\phi/\partial z = 0$ when $z = 0$.
Thus, at the $xy$ plane
\[
    \mathbf B = \frac{1}{s} \frac{\partial}{\partial s} \left( s A_\phi \right) \uvec z
\]
Substituting $A_\phi$ we found earlier with replacements, $r \rightarrow s$ and $\theta \rightarrow \pi/2$, the vector potential at $z = 0$ reads
\[
    \mathbf A(s, \phi)
    =
    \left\{
        \begin{array}{ccl}
            \displaystyle \frac{\mu_0 R \omega \sigma}{3} s \uvec \phi & & s < R \\
            \\
            \displaystyle \frac{\mu_0 R^4 \omega \sigma}{3} \frac{1}{s^2} \uvec \phi & & s > R \\
        \end{array}
    \right.
\]
Therefore, the magnetic field at $z = 0$ is
\[
    \mathbf B(s, \phi)
    =
    \left\{
        \begin{array}{ccl}
            \displaystyle \frac{2 \mu_0 R \omega \sigma}{3} \uvec z & & s < R \\
            \\
            \displaystyle -\frac{\mu_0 R \omega \sigma}{3} \frac{R^3}{s^3} \uvec z & & s > R \\
        \end{array}
    \right.
\]

\begin{enumerate}
    \item \textbf{Disk}:
        Using the result of Ex. 8.2, we have
        \[
            \left. \uvec z \cdot \tensor T_B \cdot \uvec n \right|_{z = 0}
            =
            \frac{1}{2\mu_0} (-B_z^2(s, \phi))
            =
            -\frac{1}{2\mu_0} \left( \frac{2 \mu_0 R \omega \sigma}{3} \right)^2
        \]
        and thus
        \[
            F_{B,\rm disk}
            =
            -\frac{1}{2\mu_0} \left( \frac{2 \mu_0 R \omega \sigma}{3} \right)^2 \cdot \pi R^2
            =
            -2 \pi R^2 \mu_0 \left( \frac{R \omega \sigma}{3} \right)^2
        \]

    \item \textbf{Outer Plane}:
        Similarly,
        \[
            \left. \uvec z \cdot \tensor T_B \cdot \uvec n \right|_{z = 0}
            =
            -\frac{1}{2\mu_0} \left( \frac{\mu_0 R \omega \sigma}{3} \right)^2 \frac{R^6}{s^6}
        \]
        Using $da = s ds d\phi$,
        \[
            \begin{aligned}
                F_{B,\rm outer}
                &=
                -\frac{1}{2\mu_0} \left( \frac{\mu_0 R \omega \sigma}{3} \right)^2
                \cdot 2\pi \int_R^\infty \frac{R^6}{s^6} sds
                \\
                &=
                -\frac{\pi R^2}{4\mu_0} \left( \frac{\mu_0 R \omega \sigma}{3} \right)^2
                \\
                &=
                -\frac{1}{4} \pi R^2 \mu_0 \left( \frac{R \omega \sigma}{3} \right)^2
            \end{aligned}
        \]
\end{enumerate}

Putting all this together,
\[
    F = F_E + F_B
    = \frac{4\pi}{\epsilon_0} \frac{\sigma^2 R^2}{8} - \frac{9}{4} \pi R^2 \mu_0 \left( \frac{R \omega \sigma}{3} \right)^2
\]
The force of magnetic attraction is
\[
    F_B = - \frac{9}{4} \pi R^2 \mu_0 \left( \frac{R \omega \sigma}{3} \right)^2
\]

\framed{%
    \problem{8.4a}%
    Consider two equal point charges $q$, separated by a distance $2a$.
    Construct the plane equidistant from the two charges.
    By integrating Maxwell's stress tensor over this plane, determine the force of one charge on the other.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch08-problem_4-01}
    \caption{Problem 8.4a Force between two point charges.}%
\end{figure}
We already know the answer: The force on the right charge is
\[
    \mathbf F = \frac{q^2}{4\pi\epsilon_0} \frac{1}{4 a^2} \uvec z
\]
Let's solve it using the Maxwell stress tensor.\\

We can choose a surface of infinite hemisphere as in Ex. 8.2, where the contribution from the dome vanishes because $E \rightarrow 0$ as $r \rightarrow 0$.
Furthermore, by symmetry, only the $z$ component of the force should survive, so
\[
    \mathbf F
    =
    F \uvec z
    =
    \int_{\rm plane} \tensor T \cdot d \mathbf a
\]
At $z = 0$, the total electric field has only one component (in cylindrical coordinates):
\[
    \mathbf E(s, \phi) = E_s(s) \uvec s
\]
where
\[
    E_s(s) = \frac{2q}{4\pi\epsilon_0} \frac{s}{(s^2 + a^2)^{3/2}} \uvec s
\]
Using this information and the result of Ex. 8.2, we find
\[
    \begin{aligned}
        \left. \uvec z \cdot \tensor T \cdot \uvec n \right|_{z = 0}
        &=
        \epsilon_0 \underbrace{(\uvec z \cdot \mathbf E)(\mathbf E \cdot (-\uvec z))}_{\color{blue} = 0}
        -
        \frac{\epsilon_0}{2} E^2
        \underbrace{(\uvec z \cdot \uvec z)(\uvec z \cdot (-\uvec z))}_{\color{blue} = -1}
        \\
        &=
        \frac{\epsilon_0}{2} E_s^2
        =
        \frac{\epsilon_0}{2} \left( \frac{2q}{4\pi\epsilon_0} \right)^2 \frac{s^2}{(s^2 + a^2)^3}
    \end{aligned}
\]
Putting this into the integral and using $da = s ds d\phi$, we find
\[
    F
    =
    \frac{\epsilon_0}{2} \left( \frac{2q}{4\pi\epsilon_0} \right)^2
    \cdot 2\pi \underbrace{\int_0^\infty \frac{s^2}{(s^2 + a^2)^3} sds}_{\color{blue} = 1/(4a^2)}
    =
    \frac{q^2}{4\pi\epsilon_0} \frac{1}{4 a^2}
\]
as expected.

\framed{%
    \problem{8.4b}%
    Do the same as Prob. 8.4a, but for charges that are opposite in sign.
}
\solution%

\begin{figure}[ht]
    \centering
    \includegraphics{assets/ch08-problem_4-02}
    \caption{Problem 8.4b Force between two point charges.}%
\end{figure}
Suppose that $-q$ is placed below the $xy$ plane.
Of course, the Coulomb force on $+q$ is
\[
    \mathbf F = -\frac{q^2}{4\pi\epsilon_0} \frac{1}{4 a^2} \uvec z
\]

In this case, at $z = 0$ only the $z$ component of the total electric field survives:
\[
    E_z(s) = -\frac{2q}{4\pi\epsilon_0} \frac{a}{(s^2 + a^2)^{3/2}}
\]
Similar to Prob. 8.4a,
\[
    \begin{aligned}
        \left. \uvec z \cdot \tensor T \cdot \uvec n \right|_{z = 0}
        &=
        \epsilon_0 E_z^2 \underbrace{(\uvec z \cdot \uvec z)(\uvec z \cdot (-\uvec z))}_{\color{blue} = -1}
        -
        \frac{\epsilon_0}{2} E_z^2
        \underbrace{(\uvec z \cdot \uvec z)(\uvec z \cdot (-\uvec z))}_{\color{blue} = -1}
        \\
        &=
        -\frac{\epsilon_0}{2} E_z^2
        =
        -\frac{\epsilon_0}{2} \left( \frac{2q a}{4\pi\epsilon_0} \right)^2 \frac{1}{(s^2 + a^2)^3}
    \end{aligned}
\]
Using $da = s ds d\phi$, we find
\[
    F
    =
    -\frac{\epsilon_0}{2} \left( \frac{2q a}{4\pi\epsilon_0} \right)^2
    \cdot 2\pi \underbrace{\int_0^\infty \frac{1}{(s^2 + a^2)^3} sds}_{\color{blue} = 1/(4a^4)}
    =
    -\frac{q^2}{4\pi\epsilon_0} \frac{1}{4 a^2}
\]
as expected.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Conservation of Momentum}
Newton's second law tells us that the force on an object is equal to the rate of change of its momentum:
\[
    \mathbf F = \frac{d\mathbf p_{\rm mech}}{dt}
\]
Equating it to Eq.~(\ref{eq:ch08:LorentzForce:MaxwellStressTensor}),
\begin{equation}
    \frac{d\mathbf p_{\rm mech}}{dt}
    =
    -\underbrace{\epsilon_0\mu_0 \frac{d}{dt} \int_{\mathcal V} \mathbf S d\tau}_{\color{blue}(1)}
    +\underbrace{\oint_{\mathcal S} \tensor T \cdot d\mathbf a}_{\color{blue}(2)}
    \label{eq:ch08:Newton2ndLaw:MaxwellStressTensor}
\end{equation}
where $\mathbf p_{\rm mech}$ is the (mechanical) momentum of the particles in volume $\mathcal V$.
This is reminiscence of Poynting's theorem Eq.~(\ref{eq:ch08:PoyntingTheorem}); in fact, both have the same structure.

The terms on the right are identified as
\begin{enumerate}
    \item [(1)] Momentum stored in the field
        \begin{equation}
            \mathbf p = \mu_0\epsilon_0 \int_{\mathcal V} \mathbf S d\tau
            \label{eq:ch08:FieldMomentum}
        \end{equation}
    \item [(2)] Momentum per unit time flowing {\color{red}\textit{in}} through the surface
        \[
            \oint_{\mathbf S} \tensor T \cdot d\mathbf a
        \]
\end{enumerate}

Eq.~(\ref{eq:ch08:Newton2ndLaw:MaxwellStressTensor}) is the statement of \textit{conservation of momentum} in electrodynamics: If the mechanical momentum increases ($d\mathbf p_{\rm mech}/dt > 0$),
\begin{enumerate}
    \item either the field momentum decreases ($d\mathbf p/dt < 0$),
    \item or else the fields are carrying momentum {\color{red}\textit{into}} the volume through the surface ($\oint \tensor T \cdot d\mathbf a > 0$).
\end{enumerate}

\separator%
\begin{itemize}
    \item From Eq.~(\ref{eq:ch08:FieldMomentum}), we can identify the \textbf{momentum density} in the fields:
        \begin{equation}
            \boxed{%
                \mathbf g = \mu_0\epsilon_0 \mathbf S = \epsilon_0 (\mathbf E \times \mathbf B)
            }
            \label{eq:ch08:FieldMomentumDensity}
        \end{equation}

        {%
            \color{gray}
            Note that $|\mathbf g|/|\mathbf S| = 1/c^2$.
            Therefore, momentum imparted by the electromagnetic fields is very small compared with energy carried by them.
        }

    \item On the other hand, the \textbf{momentum flux} transported by the fields (that is, {\color{red}\textit{outward}} of the volume) is $-\tensor T$.
        (Specifically, $-\tensor T \cdot d\mathbf a$ is the electromagnetic momentum per unit time passing through the area $d\mathbf a$.)

    \item If the mechanical momentum in $\mathcal V$ is not changing (e.g., in empty space where there is no charge), then
        \[
            \int \frac{\partial\mathbf g}{\partial t} d\tau
            =
            \oint \tensor T \cdot d\mathbf a
            =
            \int (\nabla \cdot \tensor T) d\tau
        \]
        and hence,
        \begin{equation}
            \frac{\partial\mathbf g}{\partial t} = \nabla \cdot \tensor T
        \end{equation}
        This is the continuity equation for electromagnetic momentum (local conservation of momentum).
\end{itemize}

\separator%

In general, the field momentum by itself, and the mechanical momentum by itself, are \textit{not} conserved
$\rightarrow$ Charge and fields exchange momentum, and only the total is conserved.

\subsubsection{Role of Poynting Vector}
\begin{itemize}
    \item $\mathbf S$: Energy per unit area, per unit time, transported by the electromagnetic fields (\textit{energy flux}).
    \item $\mu_0 \epsilon_0 \mathbf S$: Momentum per unit volume stored in those fields (\textit{momentum density}).
\end{itemize}

\subsubsection{Role of Maxwell Stress Tensor}
\begin{itemize}
    \item $\tensor T$: Electromagnetic stress (force per unit area) acting on a surface (general form of \textit{pressure})
    \item $-\tensor T$: Flow of momentum carried by the fields (\textit{momentum flux})
\end{itemize}

\homework{8.5, 8.7}
